<!DOCTYPE html>
<html lang="en">
<title>DeLINEATE Toolbox</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../w3_mod.css">
<link rel="stylesheet" href="../../w3-theme-black.css">
<style>
.w3-sidebar {
  z-index: 3;
  width: 250px;
  top: 43px;
  bottom: 0;
  height: inherit;
}
</style>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
    <a href="../../index.html" class="w3-bar-item w3-button w3-theme-l1">&nbsp;About the DeLINEATE Toolbox&nbsp;</a>
    <a href="../../download.html" class="w3-bar-item w3-button w3-hide-small w3-hover-white">&nbsp;Download&nbsp;</a>
    <a href="../../documentation.html" class="w3-bar-item w3-button w3-hide-small w3-hover-white">&nbsp;Documentation&nbsp;</a>
    <a href="../../contact_contrib.html" class="w3-bar-item w3-button w3-hide-small w3-hover-white">&nbsp;Contact/Contribute&nbsp;</a>
  </div>
</div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" >


  <!-- Just some padding to shift everything down a bit -->
  <div class="w3-row w3-padding-32">
    <p>&nbsp;</p>
  </div>


  <!-- Spacer div to the left, then a paragraph of info -->
  <div class="w3-row w3-padding-16">
    <div class="w3-nearsixth w3-container">
      <p>&nbsp;</p>
    </div>
  
    <div class="w3-twothird w3-container">
    
    
    
    <p><strong><em>PREFACE TO ALL DOCUMENTATION</em></strong>: We have tried to be as comprehensive, helpful, and accurate as we can in all of these documents, but providing good documentation is always an uphill climb. Our code, and the underlying backend code we rely on, is always changing, which means things can easily go out of date; and doing these kinds of analyses is intrinsically a complicated process, which makes it hard to write documentation that works well for people at all different levels of technical proficiency and familiarity with the underlying concepts and technologies. We really don't want the learning curve to be a barrier to people using this toolbox, so we <em>highly</em> recommend -- especially while the number of users is relatively small and manageable -- getting in touch with the developers if you're confused, don't know where to start, etc., etc. And, of course, if you find any errors, omissions, or inconsistencies! Seriously, don't be a stranger... we are happy to add features, flesh out documentation, walk through setup, and so on, to help this project serve as many users as possible, but that requires hearing from you to help let us know what areas need the most attention. Please see our website (<code>http://delineate.it/</code>) and click on the contact page to get a link to our Bitbucket repo and an email address for the project devs.</p>

<hr />

<h1>DTGui (class)</h1>

<p>In this version of the toolbox, DTGui is still in a pretty early form, so it may not get comprehensive documentation until it is a little more mature. Also, since it is a GUI, no one should <em>really</em> be using it programmatically. So, we will eschew the usual documentation about methods and such, and focus on the primary elements of the interface. Overall, a video tutorial may be a more helpful way of learning <code>DTGui</code> than reading about it, anyway, so please check the website... if one hasn't been posted there yet by the time you read this, it should be soon.</p>

<p>The rest of this documentation pre-supposes that you have some general idea of the overall toolbox architecture, so if concepts like jobs and JSON job files, or the main object types that comprise an analysis (<code>DTAnalysis</code>,<code>DTData</code>,<code>DTModel</code>,<code>DTOutput</code>) are unfamiliar to you, you will probably want to check out the other docs/tutorials first.</p>

<p>Note that <code>DTGui</code> only provides some of the most common configurations/options that we think users will want. However, due to the flexible architecture of the toolbox, using a JSON job file (or writing your own code) gives much broader access to the many additional options that are theoretically possible to select in the Keras or PyMVPA backends, which are simply too numerous to squeeze into any reasonable GUI. So, if <code>DTGui</code> does not provide some advanced/obscure option that you want to use, you may still need to edit JSON job files (or write your own code).</p>

<p><br><br></p>

<h2>Main interface elements</h2>

<p><strong>"Select backend" button</strong>: This should be the first thing that your eye falls on, at the top center of the main <code>DTGui</code> window. This choice will affect the other interface elements, so probably best to make this one first. Current options are <code>PyMVPA</code> or <code>Keras</code>.</p>

<p><strong>"Data" section</strong>: This section is centered near the top of the window, directly under the "Select backend" button described above. This is where you can select all the options that make up a <code>DTData</code> object or, equivalently, all the options that go in the <code>data</code> section of a JSON job file. The main things to provide here are a loader function name, and the parameters to pass into the loader function (assumed to be strings; one parameter per line). Typically the first parameter would be a filename, although that's really up to your loader function. See the <code>DTData</code> documentation for more explanation of these concepts.</p>

<p><strong>"Analysis" section</strong>: This section is on the left side of the window, under the "Data" section. This is where you can select all the options that make up a <code>DTAnalysis</code> object or, equivalently, all the options that go in the <code>analysis</code> section of a JSON job file. Options provided here include things like how many iterations to run, what kind of cross-validation to use, how to scale the data, and more. See the <code>DTAnalysis</code> documentation for more explanation of these concepts.</p>

<p><strong>"Model" section</strong>: This section is in the center of the window, under the "Data" section. This is where you can select all the options that make up a <code>DTModel</code> object or, equivalently, all the options that go in the <code>model</code> section of a JSON job file. Options provided here will determine what kind of classifier is used and the various hyperparameters of that classifier. Note that the interface options available in this section are <em>heavily</em> dependent on whether you're using the Keras or PyMVPA backend, so again, make sure you've selected your backend first at the top of the window before messing with anything in this section. See the <code>DTModel</code> documentation for more explanation of these concepts.</p>

<p><strong>"Output" section</strong>: This section is on the right side of the window, under the "Data" section. This is where you can select all the options that make up a <code>DTOutput</code> object or, equivalently, all the options that go in the <code>output</code> section of a JSON job file. Options provided here include things where to save output files, what to call them, and what kinds of output to generate. See the <code>DTOutput</code> documentation for more explanation of these concepts.</p>

<p><strong>"Load settings from JSON file" button</strong>: This button allows you to load in a previously configured job from a JSON file, so that you can modify it or just inspect its contents. Clicking this will open a standard "Open file" dialog box.</p>

<p><strong>"Save settings to JSON file" button</strong>: This button lets you save the current configuration to a JSON job file that can then be run from the command line via the <code>delineate.py</code> command (currently, <code>DTGui</code> does not support running a job directly from the GUI). Clicking this will open a standard "Save file" dialog box.</p>

<p><strong>"Close GUI" button</strong>: Those among you who did well in school might have already figured this one out, but this button closes the GUI window. But if you want your configuration saved, make sure you click the "Save settings" button first!</p>
</div></div>

  <div class="w3-row w3-padding-16">
    <p>&nbsp;</p>
  </div>

  <div class="w3-row w3-padding-16">
    <p>&nbsp;</p>
  </div>

  <div class="w3-row w3-padding-16">
    <p>&nbsp;</p>
  </div>


  <footer id="myFooter" class="w3-bottom">
    <div class="w3-container w3-theme-l2 w3-padding-16">
      <h4>DeLINEATE Toolbox</h4>
    </div>

    <div class="w3-container w3-theme-l1">
      <p>http://delineate.it &nbsp; &nbsp; &#8226; &nbsp; &nbsp; Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
    </div>
  </footer>

<!-- END MAIN -->
</div>

</body>
</html>


