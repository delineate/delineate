import numpy as np
import itertools, random

# general design notes:
# - these slicers should basically do what the default slice_train_val_test() does in DTData, so maybe see that 
#   for inspiration if needed
# - namely, take a DTData dataset and some parameters, and return training, validation, and/or test data subsets,
#   along with (zero-based) labels for all of those subsets, the indices of the test samples (relative to the 
#   data in the DTData dataset... but only needed if you want to print out 'tags' in DTOutput, so can be basically 
#   ignored if you want to do something weird and test_indices doesn't fit into your plan), and an inverse label 
#   dictionary for translating zero-based labels back to native label representations for the dataset
# - note we should not have to import DTData, because these slicers should be imported BY DTData








def ptc_slicer_universal_helper( in_data, in_labels, n_diff_percombo ):
    # not a slicer itself; a helper function for ptc_slicer_universal(), below
    # regular data/labels (will be zero-based) in... same/diff data/labels out
    # n.b. 0=diff, 1=same for output labels
    
    n_classes = np.amax( in_labels ) + 1 #should be able to safely assume zero-based
    class_combos = list(itertools.combinations( range(n_classes), 2 ))  #ends up a list of tuples
    n_class_combos = len(class_combos)
    total_n_sames_diffs = n_class_combos * n_diff_percombo
    n_same_perclass = int(total_n_sames_diffs / n_classes) #caveat emptor; will truncate if it doesn't divide nicely
    
    # get trial/sample counts per class and indices
    inds_by_class = []
    n_trials_per_class = []
    for cur_class in range(n_classes):
        these_inds = np.asarray(np.where(in_labels == cur_class)).flatten()
        inds_by_class.append(these_inds)
        n_trials_per_class.append( len(these_inds) )
    
    #do diffs
    all_diff_inds = None
    for cur_combo in class_combos: #cur_combo should be a two-item tuple of zero-based class labels
        inds1 = inds_by_class[ cur_combo[0] ]
        inds2 = inds_by_class[ cur_combo[1] ]
        ind_inds = np.empty( (n_diff_percombo*2, 2), int )
        ind_inds[:,0] = np.random.randint( 0, len(inds1), n_diff_percombo*2 )
        ind_inds[:,1] = np.random.randint( 0, len(inds2), n_diff_percombo*2 )
        ind_inds = np.unique(ind_inds, axis=0)
        if len(ind_inds) < n_diff_percombo:
            DTError.err("ptc_slicer_helper: insufficient number of unique pairings found (diffs)")
        np.random.shuffle(ind_inds) #should do by rows, by default
        ind_inds = ind_inds[0:n_diff_percombo,:]
        these_diff_inds = np.empty( (n_diff_percombo, 2), int )
        these_diff_inds[:,0] = inds1[ ind_inds[:,0] ]
        these_diff_inds[:,1] = inds2[ ind_inds[:,1] ]
        if all_diff_inds is None: #there's probably a better way to do this, but ¯\_(ツ)_/¯
            all_diff_inds = these_diff_inds
        else:
            all_diff_inds = np.concatenate( (all_diff_inds, these_diff_inds) )
    
    #do sames
    all_same_inds = None
    for cur_class in range(0,n_classes):
        inds1 = inds_by_class[ cur_class ]
        ind_inds = np.empty( (n_same_perclass*2, 2), int )
        ind_inds = np.random.randint( 0, len(inds1), (n_same_perclass*2, 2) )
        ind_inds = ind_inds[ (ind_inds[:,0] != ind_inds[:,1]),: ] #remove where row is two of the same value
        ind_inds = np.sort(ind_inds) #sort rows so we catch pair flips (e.g. [1,2] and [2,1]) as dupes on the next line
        ind_inds = np.unique(ind_inds, axis=0)
        if len(ind_inds) < n_same_perclass:
            DTError.err("ptc_slicer_helper: insufficient number of unique pairings found (sames)")
        np.random.shuffle(ind_inds) #should do by rows, by default
        ind_inds = ind_inds[0:n_same_perclass,:]
        these_same_inds = np.empty( (n_same_perclass, 2), int )
        these_same_inds[:,0] = inds1[ ind_inds[:,0] ]
        these_same_inds[:,1] = inds1[ ind_inds[:,1] ]
        if all_same_inds is None: #there's probably a better way to do this, but ¯\_(ツ)_/¯
            all_same_inds = these_same_inds
        else:
            all_same_inds = np.concatenate( (all_same_inds, these_same_inds) )
    
    # combine diffs/sames, randomize which trial goes on "top" vs "bottom"
    all_diff_same_inds = np.concatenate( (all_diff_inds, all_same_inds) )
    for i in range( 0, len(all_diff_same_inds) ):
        if random.choice( (True,False) ):
            tmp = all_diff_same_inds[i,0]
            all_diff_same_inds[i,0] = all_diff_same_inds[i,1]
            all_diff_same_inds[i,1] = tmp
    
    #assemble out_data and out_labels
    out_data = np.empty( (total_n_sames_diffs*2, 2)+in_data.shape[1:], in_data.dtype )
    out_labels = np.zeros(total_n_sames_diffs*2, int)
    out_labels[total_n_sames_diffs:] = 1
    #finish assembling out_data
    out_data[:,0,:] = in_data[ all_diff_same_inds[:,0], : ]
    out_data[:,1,:] = in_data[ all_diff_same_inds[:,1], : ]
    
    return out_data, out_labels


def ptc_slicer_universal(dataset,train_val_test,classify_over,other_params):
# - not meant to be a "universal" PTC implementation per se, really just a PTC implementation for what we have been 
#   calling "universal" deep learning analyses (i.e. anything where the training/validation/test data subsets are 
#   just drawn randomly from a single "universal" pool of data
# - for more info on PTC see Williams et al, 2020: https://doi.org/10.3389/fnins.2020.00417
# - in this variation, mostly meant for our internal use but also potentially useful to others (either by itself or 
#   as a template for other PTC analyses or custom slicers in general), we first divide up the raw dataset as usual 
#   with the conventional DTData.slice_train_val_test() method
# - this also uses the train_val_test and classify_over parameters as they would normally be used in a conventional 
#   (non-PTC) deep learning analysis (side note: this slicer is also only meant for Keras-based deep learning 
#   analyses... won't really work with PyMVPA
# - for this particular style of analysis, we'll just require one more thing in other_params (which would have been 
#   named slicer_params in a job file)... other_params will need to be a list of three integers, which breaks down 
#   into a single integer for each of train/val/test (respectively) indicating how many "diff" pairs to generate for 
#   EACH COMBINATION OF CLASSES, which in turn will be used to figure out how many "same" pairs to make
#   - so if this value is 100 and you have 3 classes, that's 3 class combos (AB, BC, AC) x 100 = 300 "diff" pairs, 
#     so to get 300 "same" pairs that's 100 each of AA, BB, and CC. But if it's 100 and you have only two classes, 
#     that's only 100 "diff" pairs (AB), and thus 50 each of AA and BB. For now, no guarantees on what happens if you 
#     pick a weird number that doesn't divide out evenly for your situation... that's up to you.
#   - also, pairings will be drawn randomly without replacement (of pairs... individual trials/samples can be reused), 
#     so it is also up to you to make sure that however many same/diff pairs you ask for, you actually have enough 
#     data to generate them
#   - for now, we're not bothering with making iterators and whatnot, so try to ensure that whatever you ask for will 
#     fit into your computer memory... with PTC it is very easy to generate huge datasets if you're not careful. We 
#     may look into making an iterator-based version of this down the line, but it might depend on how much hassle that 
#     entails...
# - note that the labels that are spit out in DTOutput (if you choose to output labels) will be "same" and "diff" but 
#   will not have any info on the original categories... that's a bit beyond our functionality at this time. This also 
#   means that test_indices returned will be empty/meaningless so don't try to DTOutput any tags using this slicer. 
#   Other output types should work normally... just remember that trials/samples will refer to PAIRS of trials/samples 
#   from the original dataset and regardless of the initial # of classes, chance accuracy will be 50% (same/diff)

    train_proportion = train_val_test[0]
    val_proportion   = train_val_test[1]
    test_proportion  = train_val_test[2]
    
    [tmp_traindat, tmp_valdat, tmp_testdat, tmp_zb_trainlab, tmp_zb_vallab, tmp_zb_testlab, test_inds_ignore, inv_ld_ignore] = dataset.slice_train_val_test(train_proportion, val_proportion, test_proportion, classify_over )
    
    n_diff_train = other_params[0]
    n_diff_val   = other_params[1]
    n_diff_test  = other_params[2]
    
    [training_data,   zerobase_training_labels]   = ptc_slicer_universal_helper( tmp_traindat, tmp_zb_trainlab, n_diff_train )
    [validation_data, zerobase_validation_labels] = ptc_slicer_universal_helper( tmp_valdat,   tmp_zb_vallab,   n_diff_val   )
    [test_data,       zerobase_test_labels]       = ptc_slicer_universal_helper( tmp_testdat,  tmp_zb_testlab,  n_diff_test  )
    
    inverse_label_dict = {0:'diff', 1:'same'} #should always be the same with PTC
    test_indices = None
    return training_data, validation_data, test_data, zerobase_training_labels, zerobase_validation_labels, zerobase_test_labels, test_indices, inverse_label_dict












