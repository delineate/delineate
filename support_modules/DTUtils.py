import numpy as np


def listify( item_or_list ):
    # turns input into list if necessary
    
    if isinstance( item_or_list, list ):
        return item_or_list
    
    if isinstance( item_or_list, np.ndarray ):
        return item_or_list
    
    return [item_or_list]


def dt_input( msg ):
    #2/3 compatibility: use input if it exists, else raw_input
    try:
        return_val = raw_input( msg )
    except NameError:
        return_val = input( msg )
    
    return return_val
