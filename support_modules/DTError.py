import sys, traceback


def err( messages ):
    global dt_debug_mode
    if 'dt_debug_mode' not in globals():
        dt_debug_mode = True # TK: change to false when we go into public production? Or leave on forever?
                             #     or, another option -- leave like this until we implement some kind of global defaults file?
    
    warn( messages, trace=False )
    
    if dt_debug_mode:
        stacktrace()
    
    sys.exit()


def warn( messages, trace=False ):
    if not isinstance( messages, list ):
        messages = [messages]
    
    print('')
    for line in messages:
        print(line)
    print('')
    
    if trace == 'defer_to_global':
        global dt_debug_mode
        if 'dt_debug_mode' not in globals():
            dt_debug_mode = False # different default than err(), at least for now... TK, change how globals work? see above
        trace = dt_debug_mode
    
    if trace == True:
        stacktrace()


def stacktrace():
    stack = traceback.extract_stack()
    stack = traceback.format_list(stack)
    stack.pop()
    stack.pop()
    for line in stack:
        print(line)

    exc_type, exc_value, exc_traceback = sys.exc_info()
    stack = traceback.format_exc()
    stack = stack.replace('Traceback (most recent call last):','')
    print(stack)
