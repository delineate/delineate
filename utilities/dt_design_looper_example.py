import copy,os,sys
sys.path.append(os.path.join(os.path.dirname(__file__),'../'))
# The line above appends the path one level up from where this file is located to Python's search path.
#   Assumption is that users will leave this file in the toolbox's "utilities" subfolder.
#   (Basically, all the lines in this example script with os.path.join in them are a convenience so that 
#   you can run the script unaltered without actually needing to start out your Python in the folder in 
#   which this file is located.)
#   If you are adapting this file to your own purposes, you may not need the line above if you have 
#   already ensured that the DeLINEATE toolbox is installed on your Python search path somewhere.
import DTJob

model_compile_options = {}
model_compile_options['loss'] = 'categorical_crossentropy'
model_compile_options['metrics'] = ['accuracy']
model_compile_options['optimizer'] = 'rmsprop'

model_layer_0 = {}
model_layer_0['input_shape'] = [31,37]
model_layer_0['layer_type'] = 'Reshape'
model_layer_0['target_shape'] = [1,31,37]

model_layer_1 = {}
model_layer_1['layer_type'] = 'Conv2D'
model_layer_1['padding'] = 'same'
model_layer_1['kernel_size'] = [3,6]
model_layer_1['kernel_regularizer'] = 'keras.regularizers.l2(0.01)'
model_layer_1['filters'] = 12

model_layer_2 = {}
model_layer_2['layer_type'] = 'LeakyReLU'
model_layer_2['alpha'] = 0.1

model_layer_3 = {}
model_layer_3['layer_type'] = 'Flatten'

model_layer_4 = {}
model_layer_4['layer_type'] = 'Dense'
model_layer_4['units'] = 16

model_layer_5 = {}
model_layer_5['layer_type'] = 'LeakyReLU'
model_layer_5['alpha'] = 0.1

model_layer_6 = {}
model_layer_6['layer_type'] = 'Dense'
model_layer_6['activation'] = 'softmax'
model_layer_6['units'] = 3

model_layers = [model_layer_0,model_layer_1,model_layer_2,model_layer_3,model_layer_4,model_layer_5,model_layer_6]

model = {}
model['compile_options'] = model_compile_options
model['layers'] = model_layers

data = {}
data['loader_params'] = [os.path.join(os.path.dirname(__file__),'../sample_data/sample_fsw_dataset_ref_study.npy'), os.path.join(os.path.dirname(__file__),'../sample_data/sample_fsw_dataset_ref_study_labels.npy')]
# Similar to other os.path.join stuff up at the top, the line above assumes you are running this file unaltered and that it resides in the usual place within the toolbox folder hierarchy.
#   If you are adapting this file to your own purposes, you may not need all the os.path.join nonsense... you could just specify regular path strings, either in absolute terms (e.g. 
#   '/home/dt_user/my_data/some_data_file.npy') or relative to whatever directory you run the script from (e.g. 'some_subfolder/some_data_file.npy')
data['loader'] = 'fsw_sample_nonflat_loader'

analysis_backend_options = {}
analysis_backend_options['patience'] = 100
analysis_backend_options['epochs'] = 10000
analysis_backend_options['batch_size'] = 500

analysis = {}
analysis['nits'] = 5
analysis['xval_type'] = 'single'
analysis['train_val_test'] = [70, 15, 15]
analysis['scaling_method'] = 'standardize'
analysis['classify_over'] = 'class'
analysis['backend_options'] = analysis_backend_options

output = {}
output['output_location'] = os.path.join(os.path.dirname(__file__),'../sample_output')
# Again, line above is similar to previous os.path.join usages... you may not need the os.path.join stuff if you just specify a directory path and don't need it to be located anywhere in particular relative 
#   to the script file that you're running
output['output_filename_stem'] = 'simple_cnn_looper'
output['output_file_types'] = ['acc_summary','job_config']

template_job_struct = {};
template_job_struct['model'] = model
template_job_struct['data'] = data
template_job_struct['analysis'] = analysis
template_job_struct['output'] = output

for kernel_size in [[2,4],[3,6],[4,8]]: #loop through # of rows in same
    for filters in [8,12]: #loop through # of filters in same
        for dense_units in [8,32]: #loop through size of dense layer
            for batch_size in [50,200,500]: #loop through batch sizes
                
                temporary_job_struct = copy.deepcopy(template_job_struct)
                temporary_job_struct['model']['layers'][1]['kernel_size']              = kernel_size
                temporary_job_struct['model']['layers'][1]['filters']           = filters
                temporary_job_struct['model']['layers'][4]['units']          = dense_units
                temporary_job_struct['analysis']['backend_options']['batch_size'] = batch_size
                
                fname_append = '_kernel_size' + str(kernel_size[0]) + '-' + str(kernel_size[1]) + '_filters' + str(filters) + \
                               '_dense_units' + str(dense_units) + '_batchsize' + str(batch_size)
                temporary_job_struct['output']['output_filename_stem'] += fname_append
                
                temp_job = DTJob.DTJob()
                temp_job.job_structure = temporary_job_struct
                temp_job.run()
