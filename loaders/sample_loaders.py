import numpy as np
import scipy.io as sio


def fsw_sample_flat_loader( loader_params ):
    data_filename = loader_params[0]
    label_filename = loader_params[1]
    my_samples = np.load(data_filename)
    my_labels = np.load(label_filename)
    my_samples = np.reshape(my_samples, (my_samples.shape[0], my_samples.shape[1]*my_samples.shape[2]) )
    my_sa = {}
    my_sa['class'] = my_labels[:,1]
    my_sa['subject'] = my_labels[:,0]
    return my_samples, my_sa, None


def fsw_sample_nonflat_loader( loader_params ):
    data_filename = loader_params[0]
    label_filename = loader_params[1]
    my_samples = np.load(data_filename)
    my_labels = np.load(label_filename)
    my_sa = {}
    my_sa['class'] = my_labels[:,1]
    my_sa['subject'] = my_labels[:,0]
    return my_samples, my_sa, None


def fso_sample_flat_loader( loader_params ):
    data_filename = loader_params[0]
    mat_data = sio.loadmat( data_filename )
    my_samples = mat_data['erp_data']
    my_samples = np.reshape(my_samples,(np.shape(my_samples)[0],np.shape(my_samples)[1]*np.shape(my_samples)[2]))
    my_sa = {}
    my_sa['subj'] = mat_data['subjects'].flatten()
    my_sa['category'] = mat_data['categories'].flatten()
    return my_samples, my_sa, None


def fso_sample_nonflat_loader( loader_params ):
    data_filename = loader_params[0]
    mat_data = sio.loadmat( data_filename )
    my_samples = mat_data['erp_data']
    my_sa = {}
    my_sa['subj'] = mat_data['subjects'].flatten()
    my_sa['category'] = mat_data['categories'].flatten()
    return my_samples, my_sa, None


def sample_generic_matfile_loader( loader_params ):
    # this will work for mat-files containing one variable of samples (already properly shaped)
    #   and one variable of SAs (sample attributes) e.g. class labels
    # could expand the functionality of this loader later, but for right now we'll assume 
    #   that the first item in the list is the filename, second is the variable name for the 
    #   samples data, and third item in the list is the variable name for the SAs -- which for 
    #   now we will just stick into a generic SA called "class". Items 4 onward we'll assume 
    #   to be names of additional SAs, which will get named whatever they were as variables 
    #   in the mat-file
    # MRJ notes: I've now added a way to try to make this work with either old-format Matlab 
    #   .mat files (>2GB) or newer format ones (saved with the "-v7.3" flag; >2GB). It's still 
    #   relatively untested and might require manually installing the hdf5storage package, as 
    #   I don't know if that is a dependency of Keras/PyMVPA, and thus it might not be 
    #   already set up as a part of a typical DeLINEATE install
    
    data_filename = loader_params[0]
    samples_varname = loader_params[1]
    labels_varname = loader_params[2]
    try:
        mat_data = sio.loadmat( data_filename )
    except:
        import hdf5storage
        mat_data = hdf5storage.loadmat( data_filename )
    my_samples = mat_data[ samples_varname ]
    my_sa = {}
    my_sa['class'] = mat_data[ labels_varname ].flatten()
    
    if len(loader_params) > 3:
        other_sa_varnames = loader_params[3:]
        for this_other_sa in other_sa_varnames:
            my_sa[this_other_sa] = mat_data[ this_other_sa ].flatten()
    
    return my_samples, my_sa, None


def sample_generic_npy_loader( loader_params ):
    # this will work for an NPY file containing one array of samples (already properly shaped)
    #   and one or more additional NPY files of SAs (sample attributes) e.g. class labels
    # could expand the functionality of this loader later, but for right now we'll assume 
    #   that the first item in the list is the filename of the data samples/examples array, 
    #   second is the filename name for the first of the SAs -- which for now we will just 
    #   stick into a generic SA called "class". If there are any subsequent items in the list, 
    #   they should occur in pairs -- first a filename, then a regular string, then maybe 
    #   another filename, then another string, and so on. Each filename will be for an NPY 
    #   file with an additional SA array in it, and the plain string that follows each 
    #   filename will be the name of the SA

    data_filename = loader_params[0]
    label_filename = loader_params[1]
    other_sa_params = loader_params[2:]
    my_samples = np.load(data_filename)
    my_labels = np.load(label_filename)
    my_sa = {}
    my_sa['class'] = my_labels.flatten()
    for i in range( int(len(other_sa_params) / 2) ):
        i1 = i*2
        i2 = i1+1
        this_filename = other_sa_params[i1]
        this_other_sa = other_sa_params[i2]
        this_data     = np.load(this_filename)
        my_sa[this_other_sa] = this_data.flatten()
    
    return my_samples, my_sa, None


def null_loader( loader_params ):
    # for when you want to do something sneaky but DTData really wants you to specify a loader...
    #   just specify this one.
    # use with caution; it doesn't do anything but still, only advanced users should find 
    #   themselves in the position of using this option
    return [], [], None
