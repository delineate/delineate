<!DOCTYPE html>
<html lang="en">
<title>DeLINEATE Toolbox</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../w3_mod.css">
<link rel="stylesheet" href="../../w3-theme-black.css">
<style>
.w3-sidebar {
  z-index: 3;
  width: 250px;
  top: 43px;
  bottom: 0;
  height: inherit;
}
</style>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
    <a href="../../index.html" class="w3-bar-item w3-button w3-theme-l1">&nbsp;About the DeLINEATE Toolbox&nbsp;</a>
    <a href="../../download.html" class="w3-bar-item w3-button w3-hide-small w3-hover-white">&nbsp;Download&nbsp;</a>
    <a href="../../documentation.html" class="w3-bar-item w3-button w3-hide-small w3-hover-white">&nbsp;Documentation&nbsp;</a>
    <a href="../../contact_contrib.html" class="w3-bar-item w3-button w3-hide-small w3-hover-white">&nbsp;Contact/Contribute&nbsp;</a>
  </div>
</div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" >


  <!-- Just some padding to shift everything down a bit -->
  <div class="w3-row w3-padding-32">
    <p>&nbsp;</p>
  </div>


  <!-- Spacer div to the left, then a paragraph of info -->
  <div class="w3-row w3-padding-16">
    <div class="w3-nearsixth w3-container">
      <p>&nbsp;</p>
    </div>
  
    <div class="w3-twothird w3-container">
    
    
    
    <p><strong><em>PREFACE TO ALL DOCUMENTATION</em></strong>: We have tried to be as comprehensive, helpful, and accurate as we can in all of these documents, but providing good documentation is always an uphill climb. Our code, and the underlying backend code we rely on, is always changing, which means things can easily go out of date; and doing these kinds of analyses is intrinsically a complicated process, which makes it hard to write documentation that works well for people at all different levels of technical proficiency and familiarity with the underlying concepts and technologies. We really don't want the learning curve to be a barrier to people using this toolbox, so we <em>highly</em> recommend -- especially while the number of users is relatively small and manageable -- getting in touch with the developers if you're confused, don't know where to start, etc., etc. And, of course, if you find any errors, omissions, or inconsistencies! Seriously, don't be a stranger... we are happy to add features, flesh out documentation, walk through setup, and so on, to help this project serve as many users as possible, but that requires hearing from you to help let us know what areas need the most attention. Please see our website (<code>http://delineate.it/</code>) and click on the contact page to get a link to our Bitbucket repo and an email address for the project devs.</p>

<hr />

<h1>DTData (class)</h1>

<hr />

<p>This is the class that is primarily responsible for storing your dataset in memory and doing stuff to it. Your master <code>DTAnalysis</code> object will have one <code>DTData</code> object representing the data for that analysis.</p>

<p>Like the other main classes in this toolbox, a <code>DTData</code> object is typically created by <code>DTJob</code> from a specification contained in a JSON-format job file, although you can also create one manually in code if you know what you're doing.</p>

<p><br><br></p>

<h2>Attributes</h2>

<p><strong>samples</strong> <em>(NumPy array)</em>: The actual data that you want to feed into your analysis, as a NumPy array. If you're doing a PyMVPA analysis, this should be a 2-D array with dimensions <code>trials x features</code>. If you're doing a deep learning analysis via Keras, it can be any dimensionality -- but the first dimension should still be trials, and the rest of the dimensions should match the configuration of the input layer of your neural network.</p>

<p>Note that if you're creating your analysis from a JSON job file, you normally don't have to specify this attribute directly; rather, you specify a "loader" function (see below) and DTData calls that function to get the dataset when it initializes itself.</p>

<p><strong>loader</strong> <em>(passed in as a string when initializing, but gets converted into a function object during initialization and stored as such internally)</em>: What data-loading function (aka "loader") to use. When the <code>DTData</code> object is fully instantiated, this will contain a function object representing the function to call. When you pass this into the <code>DTData</code> initialization function or specify it in a JSON job file, it can just be a string with the name of the function. In conjunction with <code>loader_file</code> (see below), <code>DTData</code> automagically finds the loader function to use. If <code>loader_file</code> is not specified, <code>DTData</code> will search through all the files in the "loaders" directory (in the main level of the toolbox folder) and if any of them contain a function whose name is the string in <code>loader</code>, that's the function it will use. (If more than one file in the "loaders" directory has a function with that name in it, <code>DTData</code> will just use whichever one it finds first.)</p>

<p>In short, if you're setting up your analysis with a JSON job file, this is just the name of the loader function to use. If you are an advanced user writing your own code using the <code>DTData</code> class, you can still do the same thing -- but if you don't want to use a loader function at all (and instead want to just load and configure your dataset in your own code, and stick it into the <code>samples</code> attribute manually), you can pass in the string <code>"null_loader"</code> for this attribute during <code>DTData</code> initialization. (<code>null_loader</code> is a loader function we provide in the <code>sample_loaders.py</code> file that does nothing but keeps <code>DTData</code> from throwing an error during initialization, as it normally does throw an error if you try to get away without passing in a loader function. In the future we may implement a less clunky way to skip passing in a loader function, but in the meantime, <code>null_loader</code> should work fine for any adventurous souls using this class programmatically.)</p>

<p>If you are writing your own loader function, you will probably want to check out our <code>sample_loaders.py</code> file for some examples on how they work. In brief, every loader function should take exactly one input argument (see <code>loader_params</code> below for more details), although it is welcome to ignore that argument if it likes. After doing whatever it needs to do in order to load in your data, it should return exactly three outputs, which will become the <code>samples</code>, <code>sa</code>, and <code>fa</code> attributes of the <code>DTData</code> object, so see the docs above and below on those attributes for more details. Note that we don't currently do much with the <code>fa</code> attribute, so you are welcome to return <code>None</code> for that output if you want to, as we currently do in all of our sample loaders. But <code>samples</code> and <code>sa</code> are critical to any classification, so you probably want to return meaningful values for those unless you are doing something very weird and special.</p>

<p><strong>loader_file</strong> <em>(string)</em>: If your data-loading function is not in the "loaders" directory, or if you just don't want <code>DTData</code> snorfling through all your files looking for the one with the right loader in it, you can use this to specify the exact file the loader is in. It just has to be something Python can find, so that can mean an absolute path (starting at the root of the entire filesystem), a relative path that is resolvable relative to whatever current working directory you're running from, or a bare filename that is somewhere on Python's search path. This attribute is optional during initialization (and thus is optional in JSON job files) if the function name specified in the <code>loader</code> attribute can be found in any of the files in the "loaders" directory (see the notes on the <code>loader</code> attribute above for more details on how that works).</p>

<p><strong>files</strong> <em>(old and busted; see <code>loader_params</code>)</em>: Don't use this attribute; it has been replaced by <code>loader_params</code> (see below) but has not been taken out of the code yet, so as to avoid breaking old scripts and job files.</p>

<p><strong>loader_params</strong> <em>(new hotness; can be almost anything, but a string or list of strings is common)</em>: The <code>loader_params</code> attribute is the new name for what used to be called <code>files</code>. Regardless of what you call it, the contents of this attribute are what gets passed into your loader function when it runs. Often this would be a string naming a file of data to load (or a list of several such files), hence the old name -- but we realized there were lots of other kinds of arguments that could be useful in telling loader functions what their business should be, and so we generalized the name of this attribute to <code>loader_params</code>. You can still use <code>files</code> for now, but you might get a gentle warning about it (telling you deprecation of that name is somewhere in your future), so you may want to start changing over now.</p>

<p>You may want to check out the <code>sample_loaders.py</code> file (and/or our tutorial videos) to see how <code>loader_params</code> is used in practice. If you are using one of our loaders, those examples should show you what to pass in for the loader parameters (currently all of our loaders take either a single string or a list of strings, but what those strings are used for can vary across different loader functions). If you're writing your own loader function, you can do pretty much whatever you want as long as the parameters you give that loader match its expectations -- just note that currently, we pass in the value of <code>loader_params</code> to the loader function without doing any checking to see if the loader function takes any arguments. So, every loader function should be written to take a single argument, even if it ignores that argument. But if your loader function doesn't need any arguments/parameters, it is perfectly OK to specify <code>None</code> for <code>loader_params</code> (or equivalently, not to include it in your JSON job file), as <code>None</code> is the default value for <code>loader_params</code>.</p>

<p>Note that despite our saying that <code>loader_params</code> can be almost anything, your loader function should treat it like a list (unless it ignores the parameters entirely), since <code>DTData</code> initialization automatically list-fies any non-list passed in for the <code>loader_params</code> attribute. This is basically so loader functions can be written consistently to expect a list of parameters to be passed in, but users are allowed to get a little lazy in their JSON job files and not worry about list-ifying the loader parameters there if there is only one (e.g. a single filename string, which is a very common use case).</p>

<p><strong>sa</strong> <em>(dictionary)</em>: A dictionary of "sample attributes," using the same terminology of PyMVPA and CoSMoMVPA, so you could also check out the documentation for those packages if the concepts are unclear as they are expressed here. Basically, a sample attribute is any label that can be applied to a sample (aka trial, for most neuroscience datasets) of data. Any dataset should have at least one SA, namely the class over which you intend to perform your classification. But datasets can have as many other SAs as you want, which could get used or could go unused. Another common SA type is subject (participant) number/code, which would likely be used in conjunction with the <code>loop_over_sa</code> cross-validation scheme to do a separate classification analysis for each subject. (See the <code>DTAnalysis</code> documentation for more details on that.) You could certainly use the <code>loop_over_sa</code> scheme to loop over other SAs as well besides subject -- that's just the most obvious example.</p>

<p>At present there aren't many other uses for SAs besides those cases, but there is also always the option to produce output files of SAs, even if they aren't used for anything in the analysis. This is useful if you want to do any post-processing of the output. For example, maybe you want to keep track of which exact trials are randomly selected for the test dataset in each round of classification. To do that, you would include in your dataset an SA (let's call it <code>trial_id</code>) that gives each trial a unique ID number, and then enable <code>tags:trial_id</code> in your <code>DTOutput</code> settings. This will make it fairly easy to, for example, track which specific trials (over potentially many iterations of classification) are consistently classified well and which ones are consistently classified poorly. See the <code>DTOutput</code> documentation for more details.</p>

<p>In terms of actually implementing the <code>sa</code> attribute, most users will not need to provide it directly during <code>DTData</code> initialization; much like <code>samples</code> above, it is most typically loaded and returned by your loader function. If you're writing your own loader function, or using the <code>sa</code> attribute in your own code, note that it should be a dictionary, with keys that are typically strings (although we don't explicitly check for this, so if you can figure out some other type of key you want to use and make it work, knock yourself out). As noted in the preceding paragraphs, typical keys would be things like <code>subject</code> and <code>class</code> (or <code>category</code>, or <code>condition</code>, or whatever you want to call the main attribute you're classifying over). The values for each key should be a Python list or 1-D NumPy array whose length is equal to the number of samples/trials in the <code>samples</code> attribute. What the individual labels/SAs are <em>within</em> that list/array are up to you; you can use numeric labels like 1, 2, 3, or string labels like "face" and "scene"... or presumably you COULD use more esoteric data types if you really want to make life harder on yourself, but most people are going to use either numbers or strings. If you are writing your own loader or other code that uses the <code>sa</code> attribute directly, you might want to check out our <code>sample_loaders.py</code> file to see how we do it in there.</p>

<p><strong>fa</strong> <em>(dictionary)</em>: A dictionary of "feature attributes," which are similar to "sample attributes" as described directly above, but for the features of your dataset rather than the samples/trials. For example, in fMRI data, the most obvious example of an FA would be a voxel ID (or you could have three separate FAs for its x-, y-, and z-coordinates). In EEG data, reasonable FAs would be things like electrode labels or time codes or both. This is also terminology borrowed from PyMVPA and CoSMoMVPA, so you could also check out the documentation for those packages if the concepts are unclear as they are expressed here.</p>

<p>At the moment we don't actually do anything with FAs, so you don't have to, either -- but if you write your own loader, there isn't anything stopping you from loading them and returning them. Presumably some people might find them useful if they are using the toolbox programmatically and writing their own code around it, which is why we make it possible to store FAs conveniently in <code>DTData</code> alongside everything else. But if you are using JSON job files and/or you don't really care about any feature attributes, you can forget you ever heard of them. (At least for now -- we may implement more explicit functionality with FAs in the future.)</p>

<p><strong>auto_load</strong> <em>(True or False)</em>: Most people won't need to worry about this attribute and can just ignore it, in which case it defaults to <code>True</code>. Basically, if <code>False</code> is provided instead, then <code>loader</code> and <code>loader_params</code> are ignored during initialization of the <code>DTData</code> object. So, you would probably never specify <code>False</code> if you are using the toolbox with JSON job files. If you are writing your own code using the toolbox modules programmatically, you might specify <code>False</code> if you want to create a mostly empty <code>DTData</code> object and fill in the actual data later, or if you want to directly feed in <code>samples</code>, <code>sa</code>, and/or <code>fa</code> during <code>DTData</code> initialization and ignore the whole concept of loader functions. Note that if you don't want to entirely ignore the concept of loader functions but also don't want them getting called when <code>DTData</code> is initialized, you can specify <code>False</code> for <code>auto_load</code>, and then manually call <code>load_data()</code> (see below under <strong>Methods</strong>) whenever you're ready to set the loader function in action, since all <code>auto_load</code> really does is call <code>load_data()</code> at the end of a <code>DTData</code> object's initialization.</p>

<p><strong>others</strong>: Other attributes get created on-the-fly even if they aren't provided at initialization... for now, we don't document those extensively as they aren't necessary for creating your analysis and they aren't really meant to be user-accessible in most cases, so if you are using JSON job files to create and run your analysis, you don't really have to worry about them. We may document some of the more relevant ones more extensively in the future, for people who are using this module by writing their own code. For now, we will just mention that the only one you <em>might</em> be interested in is <code>mask</code>, which basically contains a representation of which samples/trials are/aren't getting used at the moment... for example, if using the <code>xval_over_sa</code> cross-validation scheme to loop over subjects and do a separate classification for each one, each subject is masked in (and all others masked out) in turn. But that is a fairly deep implementation detail, so you probably don't even care about that one (if you do, though, there's more info in the <code>apply_mask</code> and <code>mask_sa</code> methods below).</p>

<p><br><br></p>

<h2>Methods</h2>

<p>Note that most users won't need to invoke these directly if they are creating their analyses via JSON job files, but some brief descriptions are provided for the brave few considering writing their own scripts. As always, if you are considering writing your own scripts, you might want to contact the devs for inside details.</p>

<p><br></p>

<p><strong>__init__</strong>( <code>self</code>, <code>samples</code>=<em>None</em>, <code>loader</code>=<em>None</em>, <code>loader_file</code>=<em>None</em>, <code>files</code>=<em>None</em>, <code>loader_params</code>=<em>None</em>, <code>sa</code>=<em>None</em>, <code>fa</code>=<em>None</em>, <code>auto_load</code>=<em>True</em> )</p>

<p><em>(no return value)</em> Initializer function for creating a new <code>DTData</code> object. Pretty much just assigns all the object's attributes, plus a little basic checking of the loader function and loader parameters. Also, if <code>auto_load</code> is set to <code>True</code>, the <code>load_data()</code> method (see below) will get called at the end of initialization and presumably cause the <code>samples</code>, <code>sa</code>, and/or <code>fa</code> attributes to get loaded in. All of the arguments are optional at this point in time, but most of them will need to get assigned one way or another before you can actually use the object.</p>

<p><br></p>

<p><strong>load_data</strong>( <code>self</code> )</p>

<p><em>(no return value)</em> One-liner function that just calls the loader function (stored in <code>self.loader</code>), passing it any necessary parameters (stored in <code>self.loader_params</code>), and saves the return values of the loader function (of which there should be three) into <code>self.samples</code>, <code>self.sa</code>, and <code>self.fa</code>, respectively.</p>

<p><br></p>

<p><strong>apply_mask</strong>( <code>self</code>, <code>index_array</code> )</p>

<p><em>(returns a list of logical indices)</em> Most users, even if writing their own code, don't need to worry about this method; unlike lipstick or Preparation H, it is intended mainly for internal use. Basically, it just logically "ands" together any existing mask with the index array that is passed in. Why would someone want to do that? Well, unless you're actually developing the toolbox, you probably shouldn't worry your pretty little face over it.</p>

<p><br></p>

<p><strong>slice_train_val_test</strong>( <code>self</code>, <code>train_proportion</code>, <code>val_proportion</code>, <code>test_proportion</code>, <code>classify_over</code>, <code>return_test_indices</code>=<em>False</em> )</p>

<p><em>(returns a whole bunch of stuff)</em> Another method that would rarely need to be called directly by a user, even if writing their own code; usually it gets called by one of the <code>DTAnalysis.run_X()</code> functions. As such, we'll be brief; if you <em>really</em> want to call it manually, the code is fairly self-explanatory, or feel free to get in touch with the devs.</p>

<p>Basically, this chops up the dataset into training, validation, and test subsets according to the specified proportions, ensuring equal numbers of trials/samples from each class in each of those subsets, so that training will be balanced across classes. Returns all of those things as well as their category labels (converted to a numeric scheme starting at 0; see <code>map_labels()</code> below) and, optionally, numeric indices of the trials/samples in the test dataset, which is a potential output option (see <code>DTOutput</code> documentation for more on this).</p>

<p><br></p>

<p><strong>mask_sa</strong>( <code>self</code>, <code>sa_name</code>, <code>keep_values</code> )</p>

<p><em>(no return value)</em> Another method typically called by <code>DTAnalysis</code> that users will rarely need to call directly. Sets the <code>mask</code> attribute (see above in <strong>Attributes</strong>) to mask in the value(s) specified in <code>keep_values</code> for the sample attribute specified by <code>sa_name</code>. Used, for example, when <code>DTAnalysis</code> runs a separate classification for each subject (or any other sample attribute, but subject is the most obvious usage case); each time through the loop, the subject currently being analyzed is masked in using this method, and everyone else is masked out.</p>

<p><br></p>

<p><strong>dirty_loader_things</strong>( <code>self</code>, <code>loader</code> )</p>

<p><em>(returns a function object for a loader function)</em> One of our dirty little secrets; don't look too close! Actually not that bad, but this is the method that, when passed in the name of a loader function (such as that specified by the user in a JSON job file or during <code>DTData</code> initialization), snarfles through either the <code>loader_file</code> (see <strong>Attributes</strong> above), or through all the files in the "loaders" directory in the main level of the toolbox (if no loader file is specified) to find the right function. The way we do it is not <em>not</em> gross, but it gets the job done.</p>

<p><br><br>
<strong><em>CLASS METHODS</em></strong>
<br><br></p>

<p><strong>validate_arguments</strong>( <code>cls</code>, <code>args</code> )</p>

<p><em>(returns True or False)</em> Validates the various input arguments used to initialize a <code>DTData</code> object; returns <code>True</code> if they are all OK and <code>False</code> if something is wrong with them (e.g. missing required attributes, wrong values or data types). Typically used to check the format of a JSON job file, and as such would be called by <code>DTJob</code> when the job file is read in (rather than a user calling this method directly).</p>

<p>Note that currently this method works in the laziest way possible, namely it just tries to create a temporary <code>DTData</code> object with the arguments given. If that object is created successfully, then it returns <code>True</code>; if some kind of error occurs, it returns <code>False</code>. In the future, hopefully we will make this method a bit smarter so it can actually inspect the arguments and give more useful feedback on exactly what is wrong with them.</p>

<p>Note also that if a Python global variable named <code>dt_debug_mode</code> is defined and set to <code>True</code>, a failed validation will cause an error rather than just making this method return <code>False</code>. Right now <code>dt_debug_mode</code> does default to <code>True</code>, but in the future we intend to some day change this behavior to the more graceful validation failure behavior of simply giving an informative warning.</p>

<p><br></p>

<p><strong>pymvpa_wrap</strong>( <code>cls</code>, <code>in_data</code>, <code>in_labels</code> )</p>

<p><em>(returns a PyMVPA Dataset object)</em> PyMVPA packages up data differently than Keras does (and differently from the way we store it in <code>DTData</code>) so this little utility function takes a chunk of data and a list of labels and returns it in the format that PyMVPA is expecting. Users probably won't need to call this much; it is mainly used by <code>DTModel.train_pymvpa()</code> to pack up the training data in the correct way.</p>

<p><br></p>

<p><strong>to_categorical</strong>( <code>cls</code>, <code>y</code>, <code>num_classes</code>=<em>None</em> )</p>

<p><em>(returns a one-hot-encoded NumPy matrix)</em> This is a function copied straight from Keras (with appropriate license), so that we don't <em>have</em> to have Keras as a dependency, for folks who might want to use this toolbox with PyMVPA only (or other backends, when/if we add them). Plus a little error checking of our own. Takes in a vector of integers representing class codes, and returns a matrix representing the same classes as a set of one-hot-encoded binary values. Used mainly by <code>train_val_test_to_categorical</code> (see below).</p>

<p><br></p>

<p><strong>train_val_test_to_categorical</strong>( <code>cls</code>, <code>train</code>, <code>val</code>=<em>None</em>, <code>test</code>=<em>None</em> )</p>

<p><em>(returns a one-hot-encoded version of the training, validation, and test datasets passed in)</em> Pretty straightforward, just categorical-izes (changes to one-hot encoding) the training, validation, and/or test datasets that it is given as arguments (which are presumed to be encoded with integer class codes). If a validation set is passed in, double-checks to make sure it has the same number of categories/classes as the training set. (The same is not enforced for the test set, because there are occasions where you might not have all the categories in your test set and that is perfectly OK.) Most of the time the toolbox handles when to convert class labels from integer to one-hot, but could theoretically be a useful little function if you're rolling your own analyses.</p>

<p><br></p>

<p><strong>map_labels</strong>( <code>cls</code>, <code>class_labels</code> )</p>

<p><em>(returns a set of "zero-based" labels and a dictionary for converting back the other way)</em> Yet another little utility function for converting class labels. This one takes in a list/array of class labels that can be pretty much anything (though presumably would be either numbers or strings) and converts them to an integer representation, starting with zero (which is what PyMVPA expects; Keras wants a one-hot-encoded version, so for Keras this would be an intermediate step to be followed with the categorical conversions described above). Basically enables the convenience of users being able to label their data however they like (e.g., as 'face', 'scene', 'object' or something like that) and have this toolbox worry about making those make sense to our classification backends. Also returns a dictionary that maps these integer-ized class labels back to whatever they were originally, which is used by <code>DTOutput</code> (in conjunction with <code>unmap_labels()</code> below) to recreate the original labeling scheme so that most users never have to worry about how their labels were transmogrified during the analysis process. That said, if you are writing your own code, this could be a useful little utility for you as well for integer-izing your string (or whatever) labels.</p>

<p><br></p>

<p><strong>unmap_labels</strong>( <code>cls</code>, <code>zerobase_labels</code>, <code>inverse_label_dict</code> )</p>

<p><em>(returns a set of NON-"zero-based" labels in the user's original labeling scheme)</em> Basically does the exact inverse of the <code>map_labels()</code> function described above. Takes in a set of class labels in an integer coding scheme (that starts at zero) in conjunction with the dictionary returned by <code>map_labels()</code>, and returns a list of class labels in whatever coding scheme (strings, etc.) the user originally coded their classes with.</p>

<p><br><br></p>

<h2>JSON job file options</h2>

<p>Generally what you specify in a JSON job file will be some subset of the attributes listed in the <strong>Attributes</strong> section above; however, not all attributes will need to be included in a typical job file. So here is a quick recap of the attributes that <em>would</em> typically be a good idea to include in a job file, and what data type they should be. For details on how they behave, see the <strong>Attributes</strong> section. As always, we recommend that you check out the <code>sample_jobfiles</code> directory and/or our video tutorials for some usage hints.</p>

<p><strong>loader</strong> <em>(string)</em>: The name of your loader function.</p>

<p><strong>loader_file</strong> <em>(string)</em>: The name of the file that your loader function is in. This can be an absolute path, a relative path (relative to your current working directory when you started the <code>delineate.py</code> script), a bare filename (as long as the directory it's in is somewhere in Python's search path), or nothing at all (i.e., you can just skip specifying this option, if your loader function is in any of the files located within the "loaders" directory in the main level of the toolbox folder).</p>

<p><strong>files</strong> <em>(string or list of strings)</em>: Don't use this anymore, as it has been replaced by <code>loader_params</code> (see below), but basically this was the name of the file (or a list of filenames) to pass into your loader function.</p>

<p><strong>loader_params</strong> <em>(potentially anything, but probably a string or list of strings)</em>: Replacement for <code>files</code> above since that name implied too narrow a usage case. What you put in this parameter depends on what loader function you're using, since it basically just gets passed along to the loader. So if you didn't write the loader yourself, you might need to check out its code or documentation to know what to put in here. Most commonly this would probably be either a single filename or a list of filenames for your loader function to process, but if the loader allows something else (like numeric parameters or whatever), that works too.</p>
</div></div>

  <div class="w3-row w3-padding-16">
    <p>&nbsp;</p>
  </div>

  <div class="w3-row w3-padding-16">
    <p>&nbsp;</p>
  </div>

  <div class="w3-row w3-padding-16">
    <p>&nbsp;</p>
  </div>


  <footer id="myFooter" class="w3-bottom">
    <div class="w3-container w3-theme-l2 w3-padding-16">
      <h4>DeLINEATE Toolbox</h4>
    </div>

    <div class="w3-container w3-theme-l1">
      <p>http://delineate.it &nbsp; &nbsp; &#8226; &nbsp; &nbsp; Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
    </div>
  </footer>

<!-- END MAIN -->
</div>

</body>
</html>


