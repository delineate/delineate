<!DOCTYPE html>
<html lang="en">
<title>DeLINEATE Toolbox</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../../w3_mod.css">
<link rel="stylesheet" href="../../w3-theme-black.css">
<style>
.w3-sidebar {
  z-index: 3;
  width: 250px;
  top: 43px;
  bottom: 0;
  height: inherit;
}
</style>
<body>

<!-- Navbar -->
<div class="w3-top">
  <div class="w3-bar w3-theme w3-top w3-left-align w3-large">
    <a href="../../index.html" class="w3-bar-item w3-button w3-theme-l1">&nbsp;About the DeLINEATE Toolbox&nbsp;</a>
    <a href="../../download.html" class="w3-bar-item w3-button w3-hide-small w3-hover-white">&nbsp;Download&nbsp;</a>
    <a href="../../documentation.html" class="w3-bar-item w3-button w3-hide-small w3-hover-white">&nbsp;Documentation&nbsp;</a>
    <a href="../../contact_contrib.html" class="w3-bar-item w3-button w3-hide-small w3-hover-white">&nbsp;Contact/Contribute&nbsp;</a>
  </div>
</div>

<!-- Main content: shift it to the right by 250 pixels when the sidebar is visible -->
<div class="w3-main" >


  <!-- Just some padding to shift everything down a bit -->
  <div class="w3-row w3-padding-32">
    <p>&nbsp;</p>
  </div>


  <!-- Spacer div to the left, then a paragraph of info -->
  <div class="w3-row w3-padding-16">
    <div class="w3-nearsixth w3-container">
      <p>&nbsp;</p>
    </div>
  
    <div class="w3-twothird w3-container">
    
    
    
    <p><strong><em>PREFACE TO ALL DOCUMENTATION</em></strong>: We have tried to be as comprehensive, helpful, and accurate as we can in all of these documents, but providing good documentation is always an uphill climb. Our code, and the underlying backend code we rely on, is always changing, which means things can easily go out of date; and doing these kinds of analyses is intrinsically a complicated process, which makes it hard to write documentation that works well for people at all different levels of technical proficiency and familiarity with the underlying concepts and technologies. We really don't want the learning curve to be a barrier to people using this toolbox, so we <em>highly</em> recommend -- especially while the number of users is relatively small and manageable -- getting in touch with the developers if you're confused, don't know where to start, etc., etc. And, of course, if you find any errors, omissions, or inconsistencies! Seriously, don't be a stranger... we are happy to add features, flesh out documentation, walk through setup, and so on, to help this project serve as many users as possible, but that requires hearing from you to help let us know what areas need the most attention. Please see our website (<code>http://delineate.it/</code>) and click on the contact page to get a link to our Bitbucket repo and an email address for the project devs.</p>

<hr />

<h1>DTJob (class)</h1>

<hr />

<p>The primary class that maintains information about a job (or list of jobs) to run. The most common usage of this class would probably be indirectly, e.g., by passing a JSON-based job configuration file to <code>delineate.py</code>, which basically just creates <code>DTJob</code> objects out of each file it is given and runs them in sequence. So if you are using <code>delineate.py</code> and specifying your jobs in JSON config files, you don't need to know too much about the inner workings of this class. Just see the documentation for the JSON-format job config files and <code>delineate.py</code>, and you should be all set.</p>

<p>However, if you are importing these modules manually and writing your own Python code, you may find yourself creating <code>DTJob</code> objects. See <code>dt_design_looper_example.py</code> for a basic script that does this.</p>

<p>The main thing to know in either case is that <code>DTJob</code> essentially maintains a structure of parameters (or hyperparameters, in deep learning parlance) that define a given analysis using a given dataset. Eventually, when the <code>run()</code> method is called, it will use those (hyper-)parameters to create <code>DTAnalysis</code>, <code>DTData</code>, and <code>DTModel</code> objects. So you can think of <code>DTJob</code> as kind of an umbrella container for those objects, although in the current implementation it doesn't hang on to them for any length of time -- those objects are just created long enough to run the analysis, and then destroyed. Another way to think of <code>DTJob</code> objects is as the thawed, usable form of the information frozen in JSON-based job config files.</p>

<p><br><br></p>

<h2>Attributes</h2>

<p><strong>job_file</strong> <em>(string)</em>: A filename or path to a JSON-format job configuration file that will be used to create the rest of the <code>DTJob</code> object. This is the only attribute that can be specified when creating a new <code>DTJob</code> object via the <code>__init__()</code> method; if that's the way you are going to create your <code>DTJob</code> object (which is essentially what <code>delineate.py</code> does), then you won't have to fiddle with any of the other attributes. In this case, all you'd have to do is something like:</p>

<blockquote>
  <p><code>import DTJob</code></p>

<p><code>my_job = DTJob.DTJob( 'my_job_file.json' )</code></p>

<p><code>my_job.run()</code></p>
</blockquote>

<p>Although if that's <strong>all</strong> you want to do, you could probably just run <code>delineate.py</code> and pass it <code>my_job_file.json</code> in the first place.</p>

<p>If you are manually creating your <code>DTJob</code> object, then this attribute is not mandatory to specify; in <code>__init()__</code>, it defaults to <code>None</code>. So you can also just do:</p>

<blockquote>
  <p><code>import DTJob</code></p>

<p><code>my_job = DTJob.DTJob()</code></p>
</blockquote>

<p>... and then fill in the rest of the attributes later, before you run the job.</p>

<p>Another option would be to load a basic job configuration in via a JSON file, but then tweak the job structure manually before you actually run. This is fundamentally not too different from specifying the whole job structure yourself in code, but it might save you a few lines of code if you typically use a very similar configuration (which can be saved in the JSON file and loaded in to create a template <code>DTJob</code> object) and just want to tweak a few things in your Python code.</p>

<p>Note that <code>job_file</code> is just going to be passed along to Python's regular <code>open()</code> function eventually, so it can be a full path or a bare filename or whatever, just as long as it is accessible from wherever you're running.</p>

<p><strong>job_structure</strong> <em>(list of dictionaries)</em>: This is the actual data structure representing what is going to get run (and with what data), either specified in <code>job_file</code> or in code that you write.</p>

<p>It follows the same basic format as the JSON-format job config files, so for a breakdown of all the stuff that can go into this data structure, see the documentation for those files.</p>

<p>Essentially, though, <code>job_structure</code> is a list of Python dictionaries. (However, if you make it a single Python dictionary, <code>DTJob</code> should generally be nice and turn it into a single-item list as necessary... but you should probably think of it and use it as a list of dictionaries to avoid problems down the line.) Each dictionary contains the information needed to run one job. The JSON job config files can contain either a single job or a list of jobs, so loading the contents of one job file into one <code>job_structure</code> maintains a 1:1 correspondence between actual job files and <code>DTJob</code> objects.</p>

<p>Each dictionary contains three main keys: <code>data</code>, <code>analysis</code>, and <code>model</code>. The values corresponding to those keys will in turn be other dictionaries; each of these dictionaries contains the (hyper-)parameters necessary to instantiate a <code>DTData</code>, a <code>DTAnalysis</code>, and a <code>DTModel</code> object, respectively. Again, see the docs for the config files for more details on what should go into those.</p>

<p>As suggested above, see <code>dt_design_looper_example.py</code> for some ideas as to how you might go about creating this structure manually in Python code.</p>

<p>Also, as noted above in the description of the <code>job_file</code> attribute, if you wanted to initially create a <code>DTJob</code> object using an actual JSON job config file, then tweak the <code>job_structure</code> attribute manually before running, that would be perfectly allowable. The <code>job_file</code> parameter is automatically converted into a <code>job_structure</code> if you pass in a job file when creating a brand-new <code>DTJob</code> object, so you can assume if you create a <code>DTJob</code> object from a file that the <code>job_structure</code> will exist when the object is done being initialized. Alternately, if you add in a <code>job_file</code> attribute after creating the <code>DTJob</code> object for some reason, you will need to call the <code>reload()</code> method (see below) to turn that file into a <code>job_structure</code>. In any case, whatever you want to do to modify the <code>job_structure</code> before calling the <code>run()</code> method is totally up to you.</p>

<p><strong>last_loaded_job_file</strong> <em>(string)</em>: Normally this attribute would not need to be set or accessed by the user. It is set whenever the <code>reload()</code> method (see below) is run, so that we have a record of what job file was last loaded in?</p>

<p>"But, all-knowing documentation writers," you say. "Why do you even need this? Isn't it just the same as the <code>job_file</code> attribute then?" We chuckle knowingly. "Yes, my child," we condescendingly reply. "But suppose some user, less forward thinking than you or we, changes the <code>job_file</code> attribute on the fly and then calls the <code>run()</code> method. Presumably, they would be expecting the new <code>job_file</code> to get run, but instead the <code>job_structure</code> would be reflecting an earlier job file. So we save the name of the last job file that we know we loaded, so that <code>run()</code> can make sure that no one has changed out the <code>job_file</code> on us, and warn the user if they have."</p>

<p>"Yeah, yeah, that makes sense... but just one more thing," you say, suddenly sounding a lot more like Columbo. "Couldn't you do the same thing without needing another attribute? Either by using a setter function that updates <code>job_structure</code> every time <code>job_file</code> is changed, or by checking the actual contents of the <code>job_structure</code> attribute against the current <code>job_file</code> when <code>run()</code> is called?" We turn around, pause, and nod wearily. "Yes, we could have," we say. "But those sounded like more work than just doing this, and also we just thought of those other options now, while writing this documentation, so it's going to stay that way for a while. And, at least this way, the user gets a warning that changing <code>job_file</code> on the fly without explicitly then reloading the <code>job_structure</code> using the <code>reload()</code> method is kind of a weird thing to do."</p>

<p><strong>job_file_hash</strong> <em>(string)</em>: Normally, the user will not have to set or access this attribute either. It is currently used in two places: By <code>DTModel</code>, to determine the name of a tempfile that will be created during Keras analyses, and by <code>DTOutput</code>, to provide a default output filename if the user has not specified their own. If you really care, you can see the documentation for those classes for a bit more detail. But essentially, if the user loads a <code>job_file</code> (see above), this gets a hashed version of the contents of that file; if no job file is provided, it gets a generic default value.</p>

<p>The only occasion we can envision where you might want to think about this attribute are when two analyses are running concurrently in the same place in the filesystem. If they are using two different job files, then the <code>job_file_hash</code> will be different for them, and there is no danger of collision in either the Keras tempfiles or the output (assuming the user was foolish enough not to provide an output filename). But if they are both using the same job file (which would be a weird thing to do), or if the user is writing their own code rather than using JSON job files and has left the <code>job_file</code> attribute blank, there is the danger of files having the same name and thus getting overwritten. In the first case, the solution should probably be just to not be weird and use two different job files with different output filenames explicitly specified, which will ensure that the tempfiles get unique names also. In the second case, you may want to specify <code>job_file_hash</code> manually and give it either a random value or some meaningful value that is guaranteed to be unique to the instance of the analysis that is currently running.</p>

<p><br><br></p>

<h2>Methods</h2>

<p>Note that most users won't need to invoke these directly if they are creating their analyses via JSON job files, but some brief descriptions are provided for the brave few considering writing their own scripts. As always, if you are considering writing your own scripts, you might want to contact the devs for inside details.</p>

<p><br></p>

<p><strong>__init__</strong>( <code>self</code>, <code>job_file</code>=<em>None</em> )</p>

<p><em>(no return value)</em> Initializer function for creating a new <code>DTJob</code> object. Pretty much just assigns all the object's attributes, and at this time the only one you can explicitly specify during init time is <code>job_file</code>.</p>

<p>When the initializer runs, it mainly just calls the <code>reload()</code> method (see below); if a <code>job_file</code> was passed in, <code>reload()</code> should then populate the other attributes listed in the <strong>Attributes</strong> section. If no job file was passed in, then most of the attributes just stay <code>None</code> and <code>job_file_hash</code> gets a generic default string as detailed in its entry above.</p>

<p><br></p>

<p><strong>reload</strong>( <code>self</code> )</p>

<p><em>(no return value)</em> Function to load the contents of a JSON job file (namely, the one specified in the <code>job_file</code> attribute) into the <code>job_structure</code> attribute. Automatically called during initialization. If users are writing their own code using this module, they should also call <code>reload()</code> if they manually change the <code>job_file</code> attribute and want the file to actually get loaded in.</p>

<p>Along the way it does some (fairly basic) validation of the specified file... first in just making sure it exists and has valid JSON syntax. If that's all good, it reads in the data structure from the JSON file and then calls <code>validate_job_structure()</code> (see below) for additional checking.</p>

<p>After the job file is read in, this function also updates the <code>job_file_hash</code> attribute with an MD5 hash of the file's contents; see the corresponding entry in the <strong>Attributes</strong> section above for more info on what that hash is used for.</p>

<p><br></p>

<p><strong>run</strong>( <code>self</code> )</p>

<p><em>(no return value)</em> This is a pretty big function at the heart of the entire toolbox; it runs the analysis. If you have used the <code>delineate.py</code> script, you may have noticed that it is basically just a loop of creating <code>DTJob</code> objects and then calling <code>run()</code> on them. If you are writing your own code using this module, you probably will call <code>run()</code> at some point.</p>

<p>Despite its importance, it's a pretty simple function. It checks to make sure there is actually a job to run, calls <code>generate_analysis()</code> (see below) to use the info in the <code>job_structure</code> attribute to actually create a <code>DTAnalysis</code> object and its <code>DTData</code>, <code>DTModel</code>, and <code>DTOutput</code> sub-objects, and then passes control off to the <code>run()</code> method of the <code>DTAnalysis</code> object to do the heavy lifting.</p>

<p>Oh, and if there are multiple jobs in the <code>job_structure</code> attribute (which should be a list in general, but it could be a list of one item), <code>run()</code> loops through those in order.</p>

<p>This function also handles the <code>KeyboardInterrupt</code> that is generated when the user presses Ctrl-C by printing a message onscreen and proceeding to the next job in the list (if any). So if your analysis isn't going well and you want to terminate it early, feel free to do so without fear of also killing any additional jobs that might be after it in the queue. (Just don't hold down Ctrl-C too long... we're not sure if that generates multiple <code>KeyboardInterrupt</code>s because we're too scared to try, and it might be system-specific anyway? But if you do hold it down and it kills multiple jobs as a result, don't say we didn't warn you.)</p>

<p><br></p>

<p><strong>generate_analysis</strong>( <code>self</code>, <code>job_item</code>=<em>None</em> )</p>

<p><em>(returns a fully-populated DTAnalysis object, ready to be run)</em> Takes in a single dictionary (e.g., one element of the list in the <code>job_structure</code> attribute) describing the (hyper)parameters of an analysis and uses it to create a living, breathing <code>DTAnalysis</code> object, along with the <code>DTData</code>, <code>DTModel</code>, and <code>DTOutput</code> objects that live inside its little kangaroo pouch.</p>

<p>Mostly what it does is pretty simple; it breaks out the values for the <code>analysis</code>, <code>data</code>, <code>model</code>, and <code>output</code> keys in the job structure (for more details, see the documentation on the format of the JSON job files) and uses those to instantiate the <code>DTAnalysis</code>, <code>DTData</code>, <code>DTModel</code>, and <code>DTOutput</code> objects.</p>

<p>In the process, it also fills in a few attributes of those objects that the <code>DTJob</code> object knows about (e.g. it passes along the <code>job_file_hash</code> attribute to both the <code>DTModel</code> and <code>DTOutput</code> objects, for purposes of naming temp files and output files). And, it sets up associations among the <code>DTAnalysis</code>, <code>DTData</code>, <code>DTModel</code>, and <code>DTOutput</code> objects (i.e., it assigns the <code>DTData</code>, <code>DTModel</code>, and <code>DTOutput</code> objects to be attributes of the <code>DTAnalysis</code> object, but also sets up the reference that the <code>DTOutput</code> object retains back to its parent <code>DTAnalysis</code> object).</p>

<p>If you are writing your own code using this module, it is conceivable that you might use this function (e.g., if you want to generate a <code>DTAnalysis</code> from a job structure and then go off and do your own thing with it), but in typical usage, you'd probably just call <code>run()</code> (see above) and let that take care of everything.</p>

<p><br></p>

<p><strong>validate_job_structure</strong>( <code>self</code>, <code>struct_to_validate</code>=<em>None</em> )</p>

<p><em>(returns True or False)</em> Checks the format of a job structure (or list of job structures); returns <code>True</code> if all is well and <code>False</code> if not. Mainly does so by looping through <code>validate_job_structure_onedict()</code> (see below). It's worth noting that this is not an incredibly comprehensive check; it won't necessarily catch all errors that could crop up when you actually run an analysis, it just confirms that the job structure is not SO broken that it can't even create the basic <code>DTAnalysis</code>, <code>DTData</code>, <code>DTModel</code>, and <code>DTOutput</code> objects that comprise a job to run.</p>

<p>Note that if nothing is passed in for the <code>struct_to_validate</code> argument, the default is to use the <code>job_structure</code> attribute (see <strong>Attributes</strong> section for details). In practice this flexibility is a little pointless, since the one place where <code>validate_job_structure()</code> is used in the toolbox code is to check potential candidates for the <code>job_structure</code> attribute. However, it does enable to the user to use this method to validate arbitrary job structures if they really desire; why you might want to do this outside of the normal use cases of the <code>DTJob</code> functionality is unclear to us, but we aren't here to judge your bizarre life choices. The only catch is that because this is not a class method, you would have to create a blank <code>DTJob</code> object in order to be able to use the method.</p>

<p><br></p>

<p><strong>validate_job_structure_onedict</strong>( <code>self</code>, <code>dict_to_validate</code> )</p>

<p><em>(returns True or False)</em> Checks the format of a single dictionary specifying a job structure, returning <code>True</code> if all is well and <code>False</code> if not. A warning message is also printed giving a little bit of detail if the check fails. Users would typically not need to call this sub-function directly; they could just call <code>validate_job_structure()</code> and let it take care of the details.</p>

<p>This check is pretty basic and essentially proceeds in two steps. First, it extracts the values for the <code>analysis</code>, <code>data</code>, <code>model</code>, and <code>output</code> keys in the job structure. A valid job structure has to have SOMETHING in place for all four of those elements, so if any of them are missing, the validation check fails.</p>

<p>If all four of those elements exist, we go on to call the <code>validate_arguments()</code> method within each of the <code>DTAnalysis</code>, <code>DTData</code>, <code>DTModel</code>, and <code>DTOutput</code> classes, passing them the relevant arguments from the job structure. (See those modules for details, but in short: Right now validation is very basic for all of them and essentially consists of trying to instantiate an object for each of them with the arguments provided. If an error is encountered, it is presumed to be due to bad arguments and the check fails. More sophisticated/smart argument checking may come in the future, but this basic version works OK for now.) If any one of those <code>validate_arguments()</code> checks fails, the overall validation check fails. If all four of them pass, then we're all good and return <code>True</code>.</p>

<p><br></p>

<p><strong>convert_old_json_output_options_to_new</strong>( <code>self</code>, <code>job_struct</code> )</p>

<p><em>(returns a converted job structure)</em> There is almost no chance anyone will ever want/need to run this method, since it is almost entirely vestigial at this point; however, we will document it for posterity and for the sake of convincing anyone who comes across it that they can safely ignore it.</p>

<p>Basically, in very early versions of this toolbox, <code>DTOutput</code> did not exist yet and output options were all under the umbrella of the <code>DTAnalysis</code> object. Before long we realized we had enough output options and functionality to warrant a dedicated output class, and <code>DTOutput</code> was born.</p>

<p>This method just takes job structures from job files written in the old style and migrates the output options to the new style. All of our examples should be in the new style, so no one except the devs should ever encounter the old style, and even we have mostly updated all of our ancient-est job files to be in the newer format.</p>

<p>So... yeah. TL;DR, don't worry about this method. <em>[Jedi mind trick gesture]</em> You can go about your business, move along.</p>
</div></div>

  <div class="w3-row w3-padding-16">
    <p>&nbsp;</p>
  </div>

  <div class="w3-row w3-padding-16">
    <p>&nbsp;</p>
  </div>

  <div class="w3-row w3-padding-16">
    <p>&nbsp;</p>
  </div>


  <footer id="myFooter" class="w3-bottom">
    <div class="w3-container w3-theme-l2 w3-padding-16">
      <h4>DeLINEATE Toolbox</h4>
    </div>

    <div class="w3-container w3-theme-l1">
      <p>http://delineate.it &nbsp; &nbsp; &#8226; &nbsp; &nbsp; Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
    </div>
  </footer>

<!-- END MAIN -->
</div>

</body>
</html>


