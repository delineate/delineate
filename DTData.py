import collections, copy, importlib, math, os, pkgutil, sys
import numpy as np
from support_modules import DTError, DTUtils
import loaders
#https://youtu.be/wQHZL9ePMKk
from loaders import *
import slicers
#https://youtu.be/Ig6MXx1jArE
from slicers import *


class DTData:
    
    
    def __init__(self, samples=None, loader=None, loader_file = None, files=None, loader_params=None, sa=None, fa=None, auto_load=True):
        
        self.mask = None
        
        if loader==None:
            DTError.err("DTData: A loader function must be specified, but was not.")
        
        if files==None and loader_params==None:
            warn_message = ["DTData: Warning! No files (or 'loader params' in the updated nomenclature) were provided. This may be OK, if you are",
                            "        using a loader function that does not require any filename input or other input parameters to do its job, but",
                            "        it's a little unusual. Please double-check that this is what you meant to do, and that you did not simply",
                            "        forget to include the loader_params (or 'files' in the older, to-be-deprecated nomenclature) value."]
            DTError.warn(warn_message)
        elif files != None and loader_params != None:
            if files != loader_params:
                err_message  = ["DTData: Error! Both the 'files' argument and the 'loader_params' argument to the data loader function were given, but",
                                "        these two arguments mean the same thing and you gave different values for them! What gives? Also, Dude, 'files'",
                                "        is not the preferred nomenclature -- 'loader_params', please. (We are trying to move away from calling it 'files'",
                                "        because it could be used to specify lots of other things. Both terms will work for now as long as you only provide",
                                "        one of them, but 'files' may be deprecated in the future.)"]
                DTError.err(err_message)
            else:
                warn_message = ["DTData: Warning! Both the 'files' argument and the 'loader_params' argument to the data loader function were given,",
                                "        but these two arguments mean the same thing. You did give the same value for both of them so we'll proceed as"
                                "        usual... just wanted to give you the heads-up that it was kind of a weird thing to do. Also, Dude, 'files'",
                                "        is not the preferred nomenclature -- 'loader_params', please. (We are trying to move away from calling it 'files'",
                                "        because it could be used to specify lots of other things. Both terms will work for now as long as you only provide",
                                "        one of them, but 'files' may be deprecated in the future.)"]
                DTError.warn(warn_message)
        elif files != None:
            warn_message = ["DTData: Warning! You provided a 'files' argument for the data loader function, which is fine for now (and was normal",
                            "        in the past), but we are trying to move to 'loader_params' as the preferred nomenclature for this argument,",
                            "        because in actual usage, this argument could be used to specify lots of other things besides filenames. Please",
                            "        update your job files and/or code at your convenience -- at some point in the future, the 'files' nomenclature",
                            "        may be deprecated."]
            DTError.warn(warn_message)
            loader_params = files
            files = None
        
        #this should be coming in as a list, but just in case
        if type(loader_params) != list:
            loader_params = DTUtils.listify( loader_params )
        self.loader_params = loader_params
        self.loader_file = loader_file
        self.loader = self.dirty_loader_things(loader)
        self.samples = samples
        self.sa = sa
        self.fa = fa
        
        if auto_load:
            self.load_data()
    
    
    def load_data(self):
        [self.samples, self.sa, self.fa] = self.loader(self.loader_params)
    
    
    def apply_mask(self, index_array):
        if self.mask is None:
            return index_array
        else:
            return np.logical_and(self.mask, index_array)
    
    
    def slice_train_val_test(self, train_proportion, val_proportion, test_proportion, classify_over ):
        if (train_proportion + val_proportion + test_proportion) == 100: #must have specified percent, not proportions
            train_proportion /= 100.0
            val_proportion /= 100.0
            test_proportion /= 100.0
        
        if np.abs(train_proportion + val_proportion + test_proportion - 1.0) > .0001:
            DTError.err("DTData: Train proportion + validation proportion + test proportion needs to equal 1.0!")
        
        class_labels = self.sa[classify_over] # TK: insert appropriate error checking here
         # including changing categorical labels to 0, 1, 2, etc... format in here
        zerobase_labels, inverse_label_dict = DTData.map_labels(class_labels)
        
        training_samples = np.empty([0], dtype = int)
        validation_samples = np.empty([0], dtype = int)
        testing_samples = np.empty([0], dtype = int)
        
        n_training_samples = []
        n_validation_samples = []
        n_testing_samples = []
        # TK: make this optional if you DON'T want to equate all classes
        for current_label in np.unique(class_labels):
            # TK: make less gross
            current_samples = np.asarray(np.where(self.apply_mask(class_labels == current_label))).flatten()
            n_training_samples.append( math.floor(current_samples.shape[0] * train_proportion) )
            n_validation_samples.append( math.floor(current_samples.shape[0] * val_proportion) )
            n_testing_samples.append( math.floor(current_samples.shape[0] * test_proportion) )

        n_training_samples = int(min( n_training_samples ))
        n_validation_samples = int(min( n_validation_samples ))
        n_testing_samples = int(min( n_testing_samples ))
        
        for current_label in np.unique(class_labels):
            # TK: use np.setdiff1d rather than casting back and forth
            # TK: change variable names to be more representative
            current_samples = np.asarray(np.where(self.apply_mask(class_labels == current_label))).flatten()
            current_training_samples = np.random.choice(current_samples, n_training_samples, replace = False)
            current_samples = list( set(current_samples) - set(current_training_samples) )
            current_validation_samples = np.random.choice(current_samples, n_validation_samples, replace = False)
            current_samples = list( set(current_samples) - set(current_validation_samples) )
            if test_proportion > .000001:
                current_testing_samples = np.random.choice(current_samples, n_testing_samples, replace = False)
            else:
                current_testing_samples = np.empty([0], dtype = int)

            training_samples = np.concatenate((training_samples, current_training_samples))
            validation_samples = np.concatenate((validation_samples, current_validation_samples))
            testing_samples = np.concatenate((testing_samples, current_testing_samples))

        training_data = self.samples[training_samples,:]
        validation_data = self.samples[validation_samples, :]
        test_data = self.samples[testing_samples,:]
        
        zerobase_training_labels = zerobase_labels[training_samples]
        zerobase_validation_labels = zerobase_labels[validation_samples]
        zerobase_test_labels = zerobase_labels[testing_samples]
        
        return training_data, validation_data, test_data, zerobase_training_labels, zerobase_validation_labels, zerobase_test_labels, testing_samples, inverse_label_dict
    
    
    def handle_custom_slicer( self, train_val_test, classify_over, slicing ):
        # format of 'slicing' dict: basically mirror loaders
        # - 'slicer_file' key with potential path to .py file (or empty/None/False to go searching)
        # - 'slicer' key with name of function
        # - 'slicer_params' key with any additional params
        # - main difference: slicer will always get passed this DTData object, train_val_test, and classify_over as first three params
        #   - (it doesn't have to DO anything with them, and they could be empty or whatever, but they will get passed)
        #   - then the fourth param will be the list specified by slicer_params, which it is up to the slicer to unpack and interpret
        #     - n.b. we recommend that slicer_params be a list if you have multiple parameters to specify, but technically it can be 
        #       anything... a single value, a dict, whatever works with the slicer function in question... and we may not even end up 
        #       following the list recommendation with all of our own stuff, frankly
        #     - so, basically, it's between you and your slicer function what goes into slicer_params... only formal constraint is that 
        #       if you're trying to use this stuff with JSON format job files, that limits you to data structures/types that can be 
        #       correctly represented in JSON (numbers, strings, dicts, lists, etc., but not weirder Python-specific object types)
        
        try:
            slicer_file = slicing['slicer_file']
        except:
            slicer_file = None
        
        try:
            slicer = slicing['slicer']
        except:
            DTError.err("DTData: It appears you want to use a custom slicer, but no 'slicer' parameter was specified! You silly goose.")
        
        try:
            slicer_params = slicing['slicer_params']
        except:
            slicer_params = None
        
        if slicer_file:
            if os.path.isfile(slicer_file):
                slicer_dir, slicer_file_name = os.path.split(slicer_file)
                sys.path.append(slicer_dir)
                slicer_import_name = os.path.splitext(slicer_file_name)[0]
                slicer_module = importlib.import_module(slicer_import_name)
                if getattr(slicer_module,slicer,None):
                    slicer = getattr(slicer_module,slicer)
                else: 
                    DTError.err('DTData: The slicer file named {} does not appear to contain a function named {}.'.format(slicer_file,slicer))
            else: 
                DTError.err('DTData: Unable to locate a slicer file named {}. Double check paths, etc.'.format(slicer_file))
        else:
            slicer_found = False
            for slicer_file_name in pkgutil.iter_modules(slicers.__path__):
                if getattr(eval('slicers.'+slicer_file_name[1]),str(slicer),None):
                    slicer = getattr(eval('slicers.'+slicer_file_name[1]),slicer)
                    slicer_found = True
            if slicer_found == False:
                DTError.err("DTData: Unable to locate a slicer function named {}. ".format(str(slicer)))
        
        [training_data, validation_data, test_data, zerobase_training_labels, zerobase_validation_labels, zerobase_test_labels, test_indices, inverse_label_dict ] = slicer(self,train_val_test,classify_over,slicer_params)
        
        return training_data, validation_data, test_data, zerobase_training_labels, zerobase_validation_labels, zerobase_test_labels, test_indices, inverse_label_dict
    
    
    def mask_sa(self, sa_name, keep_values):
        # keep_values may be iterable or not
        if isinstance(keep_values, collections.Iterable):
            keep_indices = np.asarray([item in keep_values for item in self.sa[sa_name]])
        else: 
            keep_indices = np.asarray(self.sa[sa_name] == keep_values)
        self.mask = keep_indices
        
        
    def dirty_loader_things(self, loader):
        if self.loader_file:
            if os.path.isfile(self.loader_file):
                loader_dir, loader_file_name = os.path.split(self.loader_file)
                sys.path.append(loader_dir)
                loader_import_name = os.path.splitext(loader_file_name)[0]
                loader_module = importlib.import_module(loader_import_name)
                if getattr(loader_module,loader,None):
                    loader = getattr(loader_module,loader)
                else: 
                    DTError.err('DTData: The loader file named {} does not appear to contain a function named {}.'.format(self.loader_file,loader))
            else: 
                DTError.err('DTData: Unable to locate a loader file named {}. Double check paths, etc.'.format(self.loader_file))
        else:
            loader_found = False
            for loader_file_name in pkgutil.iter_modules(loaders.__path__):
                if getattr(eval('loaders.'+loader_file_name[1]),str(loader),None):
                    loader = getattr(eval('loaders.'+loader_file_name[1]),loader)
                    loader_found = True
            if loader_found == False:
                warn_message = ["DTData: Unable to locate a loader function named {}. ".format(str(loader)),
                                "        Unless you are writing your own code and intentionally not using a loader function, there is probably an ",
                                "        issue with the loader function specified in your job file?"]
                DTError.warn(warn_message)
        
        return loader
    
    
    @classmethod
    def validate_arguments( cls, args ):
        # this is the laziest and dumbest possible version of a method like this, which really should inspect the arguments and give some intelligent
        # warning messages and so forth. But it'll do for now to get us off the ground
        try:
            val_args = copy.deepcopy( args )
            test_obj = cls( **val_args )
        except:
            global dt_debug_mode
            if 'dt_debug_mode' not in globals():
                dt_debug_mode = True # TK: change to false when we go into public production
            
            if dt_debug_mode:
                DTError.err( 'DTData: validate_arguments() failed' )
            else:
                return False
        
        return True
    
    
    @classmethod
    def pymvpa_wrap( cls, in_data, in_labels):
        from mvpa2.datasets.base import Dataset
        #KK: I don't love this, but the MRJ mandate says we go as far as possible before bringing in PyMVPA functions
        
        out_data = Dataset(in_data)
        out_data.sa['targets'] = in_labels
        return out_data
    
    
    # this next method (to_categorical) is copied straight from Keras, but re-implementing it so we don't have to have it as a dependency
    # (update to above comment: have now made some changes to fix a bug, but the part we added is clearly marked and the rest is unchanged)
    # n.b. Keras is MIT License so here is the copyright/license information from it, to satisfy those terms:
    # 
    # COPYRIGHT
    # 
    # All contributions by Francois Chollet:
    # Copyright (c) 2015 - 2018, Francois Chollet.
    # All rights reserved.
    # 
    # All contributions by Google:
    # Copyright (c) 2015 - 2018, Google, Inc.
    # All rights reserved.
    # 
    # All contributions by Microsoft:
    # Copyright (c) 2017 - 2018, Microsoft, Inc.
    # All rights reserved.
    # 
    # All other contributions:
    # Copyright (c) 2015 - 2018, the respective contributors.
    # All rights reserved.
    # 
    # Each contributor holds copyright over their respective contributions.
    # The project versioning (Git) records all such contribution source information.
    # 
    # LICENSE
    # 
    # The MIT License (MIT)
    # 
    # Permission is hereby granted, free of charge, to any person obtaining a copy
    # of this software and associated documentation files (the "Software"), to deal
    # in the Software without restriction, including without limitation the rights
    # to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    # copies of the Software, and to permit persons to whom the Software is
    # furnished to do so, subject to the following conditions:
    # 
    # The above copyright notice and this permission notice shall be included in all
    # copies or substantial portions of the Software.
    # 
    # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    # FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    # AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    # LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    # OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    # SOFTWARE.
    # 
    @classmethod
    def to_categorical( cls, y, num_classes=None):
        """Converts a class vector (integers) to binary class matrix.
        E.g. for use with categorical_crossentropy.
        # Arguments
            y: class vector to be converted into a matrix
                (integers from 0 to num_classes).
            num_classes: total number of classes.
        # Returns
            A binary matrix representation of the input.
        """
        
        # this part by DeLINEATE team to ensure that labels aren't doubly categorical-ized by accident
        try:
            if sum(np.greater(y.shape,1)) > 1: #at least 2 non-singleton dims?
                my_message = [  "DTData: Warning! Values passed into to_categorical() appear to have already been ",
                                "        categorical-ized. Might be OK if you're doing something weird, but worth ",
                                "        looking into if you're not sure why you're getting this. Returning the ",
                                "        already categorical-ized data unchanged."]
                DTError.warn(my_message)
                return y
            else:
                pass #assume we are good to keep going
                # this is not a perfect check because there are many other things the input could be that 
                #   break to_categorical, but at least this should ensure that we don't re-categorical-ize
                #   anything by mistake
        except:
            pass # if y is not a numpy array, we could hit an exception, but that is OK because then we know 
                 #   it has not already been through this function
        # end part by DeLINEATE team, now back to the stuff we ripped out of Keras:
        
        y = np.array(y, dtype='int').ravel()
        if not num_classes:
            num_classes = np.max(y) + 1
        n = y.shape[0]
        categorical = np.zeros((n, num_classes))
        categorical[np.arange(n), y] = 1
        return categorical
    
    
    @classmethod
    def train_val_test_to_categorical( cls, train, val=None, test=None ):
        n_classes_train = (np.unique(train)).shape[0]
        train = DTData.to_categorical( train, n_classes_train )
        
        if not (val is None):
            n_classes_val = (np.unique(val)).shape[0]
            if n_classes_val != n_classes_train:
                my_message = [  "DTData: Number of training classes doesn't match number of validation classes ",
                                "        in DTData.train_val_test_to_categorical()" ]
                DTError.err( my_message )
            val = DTData.to_categorical( val, n_classes_val )
        
        if not (test is None):
            test = DTData.to_categorical( test, n_classes_train ) #test doesn't necessarily have to have all the classes
        
        return (train, val, test)
    
    
    @classmethod
    def map_labels(cls, class_labels):
        unique_labels = list(set(class_labels))
        unique_labels.sort()
        zerobase_label_vals = list(range(len(unique_labels)))
        
        current_zb_label = 0
        label_dict = {}
        for label in unique_labels:
            label_dict[label]=zerobase_label_vals[current_zb_label]
            current_zb_label += 1
        
        inverse_label_dict = dict([value,key] for key,value in label_dict.items())
        
        zerobase_labels = []
        for current_label in class_labels:
            zerobase_labels.append(label_dict[current_label]) 
            
        zerobase_labels = np.asarray(zerobase_labels)
        return zerobase_labels, inverse_label_dict
    
    
    @classmethod
    def unmap_labels(cls, zerobase_labels, inverse_label_dict):
        non_zb_labels = [inverse_label_dict[x] for x in zerobase_labels]
        # MRJ: I hate myself for writing this line of code (among other reasons)
        return non_zb_labels
