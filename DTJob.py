import hashlib, json, os, sys
import DTAnalysis, DTData, DTModel, DTOutput
from support_modules import DTError, DTUtils
        
class DTJob:
    
    
    def __init__(self, job_file = None):
        
        self.job_file = job_file
        self.job_structure = None
        self.last_loaded_job_file = None
        self.job_file_hash = 'default_output_filename_stem' #if job structure was specified directly there is no file to hash, but hash is used for output filename stem if that is not otherwise specified so the attribute needs to exist

        if not (job_file == None):
            self.reload()
    
    
    def reload(self):
        # make sure we even have a job file to try to load
        if self.job_file is None:
            DTError.err("DTJob: tried to call reload() but job_file attribute is None.")
        
        # die if the job_file attribute is not a string -- no other data type is currently valid
        # if we want Python 3, have to do 'str' instead of 'basestring'
        # see https://stackoverflow.com/questions/1303243/how-to-find-out-if-a-python-object-is-a-string
        if (sys.version_info > (3, 0)): #Py3
            string_type = str
        else: #Py2
            string_type = basestring
        if not isinstance( self.job_file, string_type ):
            DTError.err("DTJob: tried to call reload() but job_file attribute is not a string.")
        
        # die if the file specified in the job_file attribute does not exist (as a regular file or a symlink to one)
        if not os.path.isfile(self.job_file):
            my_message = [  "DTJob: tried to call reload() but file specified in the job_file attribute does not appear to exist.",
                            "       - the filename in question was: {0}".format( self.job_file ) ]
            DTError.err( my_message )
        
        # attempt to read in the entire job file; die on fail
        try:
            job_file_ptr = open( self.job_file, 'r' )
            job_file_text = job_file_ptr.read()
            job_file_text_encoded = job_file_text.encode('utf-8')
            self.job_file_hash = hashlib.md5(job_file_text_encoded).hexdigest()
            job_file_ptr.close()
        except:
            my_message = [  "DTJob: error encountered opening/reading file specified in the job_file attribute.",
                            "       - the filename in question was: {0}".format( self.job_file ) ]
            DTError.err( my_message )
        
        # die if the job file existed but was empty
        if len(job_file_text) < 1:
            my_message = [  "DTJob: file specified in the job_file attribute appears to be empty.",
                            "       - the filename in question was: {0}".format( self.job_file ) ]
            DTError.err( my_message )
        
        # if we have gotten this far -- the job file exists, is non-empty, and has been read in successfully
        # so now, try to load JSON data from it -- die if the JSON parser encounters any errors
        try:
            job_struct = json.loads( job_file_text )
        except:
            DTError.err("DTJob: error encountered while trying to translate JSON in the job file -- bad formatting?")
        
        job_struct = self.convert_old_json_output_options_to_new( job_struct ) # from 0.1 and earlier when 'output' was not an outermost member of the JSON dict
        
        if self.validate_job_structure( job_struct ):
            self.job_structure = job_struct
            self.job_structure = DTUtils.listify( self.job_structure ) #ensure it's a list if it was only a single dict before
            self.last_loaded_job_file = self.job_file
        else:
            DTError.err("DTJob: file specified in the job_file attribute was valid JSON but did not have proper DeLINEATE job format.")
    
    
    def run(self):
        # basic plan: loop through specified jobs, create a DTAnalysis object on each loop iteration
        #           - right now we will go with a very basic scheme wherein we pass the job dicts right along to the DTData, DTAnalysis,
        #             and DTModel class initializers
        
        if self.job_structure is None:
            if self.job_file is None:
                DTError.err("DTJob: run() was called with no job file specified; nothing to do, so bailing out!")
            else:
                self.reload()
        
        if self.job_file is None:
            pass #someone must have specified the structure directly?
        elif self.job_file != self.last_loaded_job_file:
            #someone must have loaded a job file, then changed the job_file attribute but not reloaded?
            warn_message = ["DTJob: Warning! It appears that the job_file attribute has been changed without calling reload() to re-generate",
                            "       the job structure. We are assuming you want us to load the job information in the file specified by the",
                            "       current value of job_file, so we are going to try to load that in now.",
                            "       Thus, this analysis will use the job info in the file: {0}".format(self.job_file) ]
            DTError.warn(warn_message)
            self.reload()
        
        self.job_structure = DTUtils.listify( self.job_structure ) #ensure it's a list if it was only a single dict before
        for job_item in self.job_structure:
            try:
                this_analysis = self.generate_analysis( job_item )
                this_analysis.run()
                
            except KeyboardInterrupt:
                print("\n\n")
                print("Job execution interrupted by user! Continuing on with next job in list (if any).")
                print("\n\n")
    
    
    def generate_analysis(self, job_item=None ):
        if job_item is None:
            js_temp = DTUtils.listify( self.job_structure )
            if len(js_temp) > 1:
                warn_message = ["DTJob: Warning! The method generate_analysis() was called without specifying a single job, and the job structure",
                                "       contains more than one job. Using the first one in the list, but this is kind of weird. You may want to",
                                "       reconsider some of the choices you have made in life, including the choice to run generate_analysis()",
                                "       in this manner."]
                DTError.warn(warn_message)
            job_item = js_temp[0]
        
        these_data_args = job_item['data']
        these_model_args = job_item['model']
        these_model_args['job_file_hash'] = self.job_file_hash
        these_analysis_args = job_item['analysis']
        these_output_args = job_item['output']
        these_output_args['job_file_hash'] = self.job_file_hash
        if these_output_args['output_filename_stem'] == 'json':
            junk, job_file_name = os.path.split(self.job_file)
            job_file_basename, junk = os.path.splitext(job_file_name)
            these_output_args['output_filename_stem'] = job_file_basename
        this_data = DTData.DTData( **these_data_args )
        this_model = DTModel.DTModel( **these_model_args )
        this_analysis = DTAnalysis.DTAnalysis( **these_analysis_args )
        this_output = DTOutput.DTOutput( **these_output_args )
        this_output.job_struct = job_item
        this_output.set_analysis( this_analysis )
        this_analysis.dataset = this_data
        this_analysis.model = this_model
        this_analysis.output_handler = this_output
        return this_analysis
        
    
    def validate_job_structure( self, struct_to_validate=None ):
        
        if struct_to_validate==None:
            struct_to_validate = self.job_structure
        
        if isinstance( struct_to_validate, dict ):
            return self.validate_job_structure_onedict( struct_to_validate )
        elif isinstance( struct_to_validate, list):
            for this_dict in struct_to_validate:
                if not self.validate_job_structure_onedict( this_dict ):
                    return False
            return True
        else:
            return False
    
    
    def validate_job_structure_onedict( self, dict_to_validate ):
        
        try:
            these_data_args = dict_to_validate['data']
            these_model_args = dict_to_validate['model']
            these_analysis_args = dict_to_validate['analysis']
            these_output_args = dict_to_validate['output']
        except:
            warn_message = ["DTJob: Warning! Job structure validation failed in validate_job_structure_onedict().",
                            "       Exact reasons are unclear but probably the overall structure of the dict is wrong; otherwise you'd get a more ",
                            "       informative error message. Check and make sure the dict for this job contains 'data', 'model', and 'analysis' ",
                            "       keys with appropriate values attached."]
            DTError.warn(warn_message)
            return False
        
        if not DTData.DTData.validate_arguments( these_data_args ):        #note we are passing dicts, not using ** to expand them
            warn_message = ["DTJob: Warning! Job structure validation failed in validate_job_structure_onedict(), while trying to validate ",
                            "       the parameters defining the DTData object."]
            DTError.warn(warn_message)
            return False
        
        if not DTModel.DTModel.validate_arguments( these_model_args ):
            warn_message = ["DTJob: Warning! Job structure validation failed in validate_job_structure_onedict(), while trying to validate ",
                            "       the parameters defining the DTModel object."]
            DTError.warn(warn_message)
            return False
        
        if not DTAnalysis.DTAnalysis.validate_arguments( these_analysis_args ):
            warn_message = ["DTJob: Warning! Job structure validation failed in validate_job_structure_onedict(), while trying to validate ",
                            "       the parameters defining the DTAnalysis object."]
            DTError.warn(warn_message)
            return False
        
        if not DTOutput.DTOutput.validate_arguments( these_output_args ):
            warn_message = ["DTJob: Warning! Job structure validation failed in validate_job_structure_onedict(), while trying to validate ",
                            "       the parameters defining the DTOutput object."]
            DTError.warn(warn_message)
            return False
        
        return True
    
    def convert_old_json_output_options_to_new(self, job_struct ): # from 0.1 and earlier when 'output' was not an outermost member of the JSON dict
        # n.b.: a side effect will be listification b/c it's annoying otherwise; caveat emptor
        
        job_struct = DTUtils.listify( job_struct )
        
        for job_item in job_struct:
            if 'output' in job_item.keys():
                #assume already in new format and we're good; if they have output stuff in both the old and new places, that's a paddlin'
                continue
            else:
                job_item['output'] = {}
                new_analysis = {}
                
            for this_key in job_item['analysis'].keys():
                if this_key in ['output_location', 'output_filename_stem', 'output_file_types', 'pre_existing_file_behavior']:
                    # this SHOULD change the original item in job_struct but Python object behavior is just weird
                    job_item['output'][this_key] = job_item['analysis'][this_key]
                else:
                    new_analysis[this_key] = job_item['analysis'][this_key]
            job_item['analysis'] = new_analysis
        
        return job_struct
