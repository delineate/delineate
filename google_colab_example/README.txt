Google Colab (https://colab.research.google.com) is a browser-based Python (IPython) environment akin to Jupyter Notebooks with some access to GPUs, and can be used to run the DeLINEATE toolbox. Its speed and stability won't compare to running on a hard drive, so we wouldn't recommend it as a long-term solution, but it can be a good starting point for beginners wishing to explore. We've provided an example jobfile and IPython notebook to run it.


HOW TO USE THIS FOLDER:

Download the toolbox from http://www.delineate.it/ or https://bitbucket.org/delineate/delineate and place the folder in Google Drive [drive.google.com]. (Make sure the folder is named "delineate", as Bitbucket may add some random numbers at the end of the name.) 

Go to http://colab.research.google.com > File > Open Notebook and click on the Google Drive tab. Click on delineate_colab_notebook.ipynb. This should open the notebook. You can then click on the [] / arrow symbols next to each piece of code to run them. If you haven't used Colab before, you might have to authorize Google File Stream to access Google Drive.

Some common issues you might run into:

* Make sure the DeLINEATE folder is in Google Drive *before* running the first section of code, which mounts the drive. If you intiially mount the drive without the folder, it won't find the jobfile even if you re-mount the drive. You'll have to restart the process by going to Runtime > Restart runtime. (You can check this by running the piece of code "sys.path"; if '/content/drive/My Drive/delineate' is listed more than once, restart runtime.)

* Run the code sections in the order they're listed!