function benchmark_data_generator

n_conditions =          3; %must be at least 2
n_features =            [200 2000 20000]; %can be a vector for multiple variants
n_branches =            10:10:100; %can also be a vector; determines # of "trials"; see code for more details
noise_coef1 =           5000; %can be any positive value; higher = more noise; may have to play around with this to find a good value
noise_coef2 =           .8; %how much of a randomly selected trial from the other condition(s) to mix in with each trial; also may entail playing around
output_fname_format =   'benchmark_data_%d_conds_%.5d_features_%.5d_trialspercond_%7.2f_noisecoef1_%.3f_noisecoef2.mat';
big_file_format =       1==0; 
% set big_file_format to false if all your files are going to end up <2GB and you want to use the old Matlab file format
% set it to true if your files are going to be >2GB and you want to use the newer format
% set it to something obnoxious like 1==0 or 1==1 if you want to suppress a "this statement cannot be reached" warning in the editor
% n.b. you may have to use different Python loader functions for old vs. newer Matlab file formats

rng('shuffle'); %comment this out and seed the RNG with a known value if you want the same predictable data each time

for i = 1:numel(n_features)
    this_n_feat = n_features(i);
    for j = 1:numel(n_branches)
        this_n_branch = n_branches(j);
        trials_per_cond = this_n_branch .^ 2;
        data = nan(n_conditions,trials_per_cond,this_n_feat);
        labels = nan(n_conditions,trials_per_cond,1);
        for k = 1:n_conditions
            data(k,:,:) = do_one_condition(this_n_branch, this_n_feat, noise_coef1);
            labels(k,:,1) = k;
        end
        
        for k = 1:n_conditions
            other_conds = setdiff(1:n_conditions,k);
            for m = 1:trials_per_cond %#ok<BDSCI> no idea why Matlab giving this warning
                other_cond_trial_ind = randi(trials_per_cond);
                data(k,m,:) = data(k,m,:) * (1-noise_coef2) + mean(data(other_conds,other_cond_trial_ind,:),1) * noise_coef2;
            end
        end
        
        data =   reshape(data,   [n_conditions*trials_per_cond this_n_feat] ); %#ok<NASGU>
        labels = reshape(labels, [n_conditions*trials_per_cond 1] ); %#ok<NASGU>

        out_fname = sprintf(output_fname_format, n_conditions, this_n_feat, trials_per_cond, noise_coef1, noise_coef2);
        if big_file_format
            save(out_fname,'data','labels','-v7.3');
        else
            save(out_fname,'data','labels');
        end
    end
end


function out_data = do_one_condition(n_branch, n_feat, noise_coef)
% out_data will be trials x features

base_signal = rand(1,n_feat) * 2 - 1; % -1 to 1 range, uniform distribution

level1_variants = nan(n_branch,n_feat);
level2_variants = nan(n_branch,n_branch,n_feat);
% The basic idea here, which seems plausible-ish if not actually biologically accurate, is that we start with a "canonical" signal 
% for each condition (base_signal). In EEG terms, this would be like the grand average ERP over an infinite number of trials. Then 
% we generate a certain number (n_branch) of "variants" of that signal by adding noise. The idea here is that in real life, you 
% might have a handful of common variations on that canonical signal -- like, what it would look like if the subject were zoning 
% out a bit, versus super-alert, versus sub-dimensions of non-interest in the stimulus set (e.g. male vs female faces if this were 
% a face ERP study), etc. These are not the actual "trial" data yet; those will come from doing a second level of noise-adding 
% variations on each of the first-level variants, to simulate random fluctuations in signal from trial to trial within each 
% variant on the canonical trial signal. For convenience, we "branch" the same number of variants at each level, so if n_branch=10, 
% you end up with 10 level-1 variants and 100 level-2 variants. Also for convenience, we add the same proportion of Gaussian noise 
% at each level. Again, this is not meant to simulate real data super-closely so much as it is meant to make it easy to make 
% datasets of differing sizes and levels of difficulty for classification, with a minimal number of free parameters.

% (By the way, I [MJ] am well aware that this code is not super Matlabbic or computationally efficient, but it also doesn't have to 
% be run very often, so I thought it would be nice to keep it simple and easy to read. Insert Donald Knuth quote about premature 
% optimization being the root of all evil here.)
for i = 1:n_branch
    level1_variants(i,:) = base_signal + randn(1,n_feat) * noise_coef;
    for j = 1:n_branch
        level2_variants(i,j,:) = level1_variants(i,:) + randn(1,n_feat) * noise_coef;
    end
end

out_data = reshape(level2_variants, [n_branch*n_branch n_feat] );
