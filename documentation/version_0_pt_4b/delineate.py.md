***PREFACE TO ALL DOCUMENTATION***: We have tried to be as comprehensive, helpful, and accurate as we can in all of these documents, but providing good documentation is always an uphill climb. Our code, and the underlying backend code we rely on, is always changing, which means things can easily go out of date; and doing these kinds of analyses is intrinsically a complicated process, which makes it hard to write documentation that works well for people at all different levels of technical proficiency and familiarity with the underlying concepts and technologies. We really don't want the learning curve to be a barrier to people using this toolbox, so we *highly* recommend -- especially while the number of users is relatively small and manageable -- getting in touch with the developers if you're confused, don't know where to start, etc., etc. And, of course, if you find any errors, omissions, or inconsistencies! Seriously, don't be a stranger... we are happy to add features, flesh out documentation, walk through setup, and so on, to help this project serve as many users as possible, but that requires hearing from you to help let us know what areas need the most attention. Please see our website (`http://delineate.it/`) and click on the contact page to get a link to our Bitbucket repo and an email address for the project devs.

---

# delineate.py #

---

The current main entry point into the toolbox if you are using a JSON-based job configuration file (as opposed to loading the modules manually and writing your own Python code).

This script is currently very simple. It takes in one or more JSON files with job configurations, instantiates a `DTJob` object based on each one, and runs each `DTJob` in turn. Everything else is handled by the rest of the toolbox.

See the main **README.md** file for some details on the JSON job file format. And you'll probably also want to check out the `sample_jobfiles` directory and/or our video tutorials for some usage hints.

<br><br>



## Usage ##

**python delineate.py my\_first\_job.json [my\_second\_job.json] [my\_third\_job.json] ...**

**python**: Can be either Python 2 or 3 (but see **README.md** in the root folder of the repository for compatibility details). You might replace `python` with whatever command you use to invoke the Python interpreter. On many systems it seems that Python 2.7 is still the default and that is what you get when you run `python`, but if you have other versions installed, or aliases or whatever, you might say `python3` or `py3` or something like that instead here.

**delineate.py**: This script. Note if you are not inside the main DeLINEATE directory when running the script, you would need to specify the full pathname, e.g.

> `python ~/delineate_toolbox_dir/delineate.py my_first_job.json my_second_job.json`

**my\_first\_job.json** (etc.): JSON-formatted job files; any number can be specified and the script will run them in sequence. If one of them encounters a fatal error (including incorrect JSON formatting), the current functionality is for the whole script to terminate without running subsequent jobs. However, if the user manually interrupts running (e.g. with Control-C), the script will continue with the next job in the sequence. This is useful if, for example, you have a whole bunch of models queued up and you see that one is not working very well -- in that case, you can just terminate the current analysis manually and have the script move on to the next one.

For those who care about the implementation details, note that what this script will do is create a `DTJob` object out of each file specified on the command line. The JSON-format job config files can contain either a single job specification or a list of such specifications, but either way, this script does not concatenate the files or anything like that. So if you specify three files with two jobs each in them, this script will create three `DTJob` objects, each containing a list of two jobs. This detail shouldn't really matter to the end user unless you're using this code as a template to write your own script.
