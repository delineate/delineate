# this script is gonna be a bit jankier than some of our other stuff, as it's mainly for internal use to generate 
#  documentation for the website. Makes lots of assumptions that would probably not be true for more generic purposes,
#  so edit any up-top parameters with care

# NOTE: because MJ is lazy, output directories must already exist, we ain't gonna create 'em for ya


import os, glob, subprocess


script_path = os.path.dirname(os.path.realpath(__file__))
doc_folder  = script_path + "/../../documentation" #do not include trailing slash
versions    = ["version_0_pt_4b"] #will have to update this for any new versions we want to generate
vers_eng    = ["version 0.4b"] #same as above but human-readable English for index.html page
out_folder  = script_path + "/../web_files/docs" #do not include trailing slash
header_file = "web_doc_header.html"
footer_file = "web_doc_footer.html"
markdown_pl = script_path + "/gruber_markdown/Markdown.pl"

header_file = open(header_file,"rt")
header_text = header_file.read()
header_file.close()

footer_file = open(footer_file,"rt")
footer_text = footer_file.read()
footer_file.close()


for version_ind in range(len(versions)):
    cur_dir = versions[ version_ind ]
    dirpath = doc_folder + "/" + cur_dir
    doc_files = glob.glob( dirpath + "/*.md" )
    doc_files.sort()
    index_file_html = header_text
    cur_version_english = vers_eng[ version_ind ]
    index_file_html = index_file_html + "<h2>Documentation pages for " + cur_version_english + ":</h2>\n<p>&nbsp;</p>\n"
    
    for cur_file in doc_files:
        fname_in = os.path.basename( cur_file )
        fname_out = fname_in[0:-3] + '.html'
        path_out  = out_folder + "/" + cur_dir + "/" + fname_out
        
        temp_index_html = '<p><a href="' + fname_out + '">' + fname_in[0:-3] + '</a></p>\n'
        index_file_html = index_file_html + temp_index_html
        
        md_text  = subprocess.check_output( [markdown_pl, cur_file] )
        if type(md_text) == type('string'): #python 2?
            out_text = header_text + md_text + footer_text
        else:
            out_text = header_text + md_text.decode('utf-8') + footer_text
        
        out_file = open(path_out, "wt")
        out_file.write( out_text )
        out_file.close()
    
    index_file_html = index_file_html + footer_text
    path_out = out_folder + "/" + cur_dir + "/" + "index.html"
    out_file = open(path_out, "wt")
    out_file.write( index_file_html )
    out_file.close()
