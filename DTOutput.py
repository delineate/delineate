import copy, glob, json, os, re, sys, weakref
import numpy as np
import DTData
from support_modules import DTError, DTUtils


class DTOutput:
    
    
    def __init__(self, output_location = 'delineate_output', output_filename_stem=None, output_file_types=None, pre_existing_file_behavior = 'silent_append', delimiter = "\t", file_extension='.tsv', job_file_hash = 'default_output_filename_stem', job_struct = None, check_existing=True):
        # new: now, like Batman, this function does not get any parents
        self.output_location = output_location
        self.output_filename_stem = output_filename_stem
        self.output_file_types = output_file_types
        self.pre_existing_file_behavior = pre_existing_file_behavior
        self.delimiter = delimiter
        self.file_extension = file_extension # by royal decree, if users want the dot, they must include the dot
        self.job_file_hash = job_file_hash
        self.job_struct = job_struct
        self._analysis = None
        
        if self.output_file_types is None:
            DTError.warn("No output file types specified; as configured now, no file output will be produced!")
        
        if self.output_filename_stem is None:
            self.output_filename_stem = os.path.join(self.output_location,self.job_file_hash)
            DTError.warn("No output filename stem was specified; using md5 hash of the job file.")
        
        if check_existing:
            self.check_existing() # and tweak output_filename_stem if necessary
    
    
    def write_output(self, iteration, fold=0, do_headers=False, do_config=False):
        if self.output_file_types is None:
            return
        
        self.output_file_types = DTUtils.listify( self.output_file_types )
        self.canonicalize_all_output_setting()
        
        if not os.path.isdir(self.output_location):
            os.makedirs(self.output_location)
        
        for this_output_file_type in self.output_file_types:
            self.write_a_file(this_output_file_type, iteration, fold=fold, do_headers=do_headers, do_config=do_config)
    
    
    def write_a_file(self, this_output_file_type, iteration, fold=0, do_headers=False, do_config=False):
        if this_output_file_type == "acc_summary" or this_output_file_type == "test_acc":
            # in the future, possibly deprecate old acc_summary name in favor of test_acc which would be more consistent with other options
            self.write_output_acc_summary(iteration, fold, do_headers)
            
        elif this_output_file_type == "scores":
            self.write_output_scores(iteration, fold, do_headers)
            
        elif this_output_file_type == "labels":
            self.write_output_labels( iteration, fold, do_headers )
            
        elif this_output_file_type == "training_acc":
            # for now, package accuracy and loss in a single file, though this could change in the future
            self.write_output_training_acc(iteration, fold, do_headers)
            
        elif this_output_file_type == "validation_acc":
            # for now, package accuracy and loss in a single file, though this could change in the future
            self.write_output_validation_acc(iteration, fold, do_headers)
            
        elif this_output_file_type == "madame_kerasana":
            self.write_output_kerasana( iteration, fold, do_headers )
            
        elif this_output_file_type == "timestamps":
            self.write_output_timestamps( iteration, fold, do_headers )
            
        elif this_output_file_type == "metadata":
            self.write_output_metadata( iteration, fold, do_headers )
        
        elif this_output_file_type == "trained_model":
            self.write_output_trained_model( iteration, fold ) # no do_headers b/c it's not a text file type!
        
        elif this_output_file_type[0:5] == 'tags:':
            self.write_output_tags( iteration, this_output_file_type, fold, do_headers )
            
        elif this_output_file_type == "job_config":
            if do_config:
                self.write_job_config()
        else:
            DTError.err("DTOutput.write_a_file(): Unsupported output type")
    
    
    def write_output_acc_summary(self, iteration, fold=0, do_headers=False):
        # maybe rename this function when/if we rename the option in question
        # - and the corresponding output file
        output_filename = self.build_a_filename_workshop("acc_summary")
                
        if do_headers:
            DTOutput.write_headers( output_filename, ['Iteration','Fold','MeanAcc','MeanLoss'], self.delimiter )
        
        DTOutput.write_one_row_values(  output_filename, 
                                        ['{0:05d}', '{0}',  '{0:.9f}',            '{0:.9f}'], 
                                        [iteration,  fold,  self._analysis.test_acc, self._analysis.test_loss],
                                        self.delimiter                                          )
    
    
    def write_output_scores(self, iteration, fold=0, do_headers=False):
        n_classes = len(np.unique(self._analysis.zb_test_labels))
        
        if (self._analysis.model.backend=='pymvpa'):
            if (n_classes > 2) and ('SVM' in self._analysis.model.model.summary()):
                self.write_output_scores_pymvpa_multiclass(iteration, fold, do_headers)
            else:
                self.write_output_scores_generic(iteration, fold, do_headers)
        else:
            self.write_output_scores_generic(iteration, fold, do_headers)
    
    
    def write_output_scores_generic(self, iteration, fold, do_headers):
        output_filename = self.build_a_filename_workshop("scores")
        n_scores = len(self._analysis.test_prediction_scores)
        
        if do_headers:
            DTOutput.write_headers( output_filename, ['Iteration','Fold','Class', {'base':'Score','length':n_scores}  ], self.delimiter )
        
        scores_to_write = list(map(list,zip(*self._analysis.test_prediction_scores)))
        # this is a super-gross line of code, but it should work for either Python 2 or 3, and also for either numpy arrays or a list of Python lists
        # see: https://stackoverflow.com/questions/6473679/transpose-list-of-lists for where it came from
        # it looks like self.test_prediction_scores can either end up being a numpy array or a list of lists? So we are stuck doing this unless we want 
        #   to enforce one format over the other when that variable gets assigned
        # anyway, we need to transpose the scores so we have them as class x score instead of score x class, and this is the only way I know how right now -- MRJ
        
        if all([type(value)==int for value in list(self._analysis.inverse_label_dict.values())]):
            for class_num in range(len(scores_to_write)):
                DTOutput.write_one_row_values(  output_filename, 
                                                ['{0:05d}', '{0}', '{0:03d}',                                         '{0:.9f}'], 
                                                [iteration,  fold, self._analysis.inverse_label_dict[class_num], scores_to_write[class_num]],
                                                self.delimiter                                      )
        else:
            for class_num in range(len(scores_to_write)):
                DTOutput.write_one_row_values(  output_filename, 
                                                ['{0:05d}', '{0}', '{}',                                         '{0:.9f}'], 
                                                [iteration,  fold, self._analysis.inverse_label_dict[class_num], scores_to_write[class_num]],
                                                self.delimiter                                      )
    
    
    def write_output_scores_pymvpa_multiclass(self, iteration, fold, do_headers):
        output_filename = self.build_a_filename_workshop("scores")
        n_scores = len(self._analysis.test_prediction_scores)
        
        if do_headers:
            DTOutput.write_headers( output_filename, ['Iteration','Fold','Class 1','Class 2', {'base':'Score','length':n_scores}  ], self.delimiter )
        
        # for reasons I don't want to think about right now we're getting a list of lists of dictionaries. use set operations to ignore the redundant keys
        # as a token kindness to matt I won't even use list comprehension to do it
        comparison_keys = []
        redundant_keys = set()
        for comparison in self._analysis.test_prediction_scores[0][0].keys():
            s = frozenset(comparison)
            if s not in redundant_keys:
                redundant_keys.add(s)
                comparison_keys.append(comparison)
        
        for comparison_num in comparison_keys:
            scores_to_write = [val[0][comparison_num] for val in self._analysis.test_prediction_scores]
            DTOutput.write_one_row_values(  output_filename,
                                            ['{0:05d}', '{0}',  '{0:03d}',          '{0:03d}',         '{0:.9f}'], 
                                            [iteration,  fold,   comparison_num[0], comparison_num[1], scores_to_write],
                                            self.delimiter                                      )
    
    
    def write_output_training_acc(self, iteration, fold, do_headers):
        output_filename = self.build_a_filename_workshop("training_acc")
        
        if self._analysis.model.backend != 'pymvpa':
            if do_headers:
                DTOutput.write_headers( output_filename, ['Iteration','Fold','Epoch','MeanAcc','MeanLoss'], self.delimiter )
            
            for epoch in range(len(self._analysis.train_acc)):
                DTOutput.write_one_row_values(  output_filename, 
                                                ['{0:05d}', '{0}',  '{0:05d}',            '{0:.9f}',                       '{0:.9f}'], 
                                                [iteration,  fold,  epoch, self._analysis.train_acc[epoch],  self._analysis.train_loss[epoch]],
                                                self.delimiter                                          )
        else:                
            if do_headers:
                DTOutput.write_headers( output_filename, ['Iteration','Fold','Epoch','MeanAcc','MeanLoss'], self.delimiter )
            
            DTOutput.write_one_row_values(  output_filename, 
                                            ['{0:05d}', '{0}',  '{0}',                '{0:.9f}',               '{0}'], 
                                            [iteration,  fold,  -1,   self._analysis.train_acc,  -1],
                                            self.delimiter                                          )
        
        
    def write_output_validation_acc(self, iteration, fold, do_headers):
        if self._analysis.model.backend != 'pymvpa':
            output_filename = self.build_a_filename_workshop("validation_acc")
                    
            if do_headers:
                DTOutput.write_headers( output_filename, ['Iteration','Fold','Epoch','MeanAcc','MeanLoss'], self.delimiter )
            
            for epoch in range(len(self._analysis.val_acc)):
                DTOutput.write_one_row_values(  output_filename, 
                                                ['{0:05d}', '{0}',  '{0:05d}',            '{0:.9f}',                           '{0:.9f}'], 
                                                [iteration,  fold,  epoch, self._analysis.val_acc[epoch],  self._analysis.val_loss[epoch]], 
                                                self.delimiter                                          )
        else:
            DTError.warn("Attempted to write validation accuracy with a backend other than Keras. Validation is not a thing of PyMVPA so that's not actually going to happen.")
        
        
    def write_output_labels(self, iteration, fold=0, do_headers=False):
        output_filename = self.build_a_filename_workshop("labels")
        n_labels = len(self._analysis.zb_test_labels)
        
        if isinstance(self._analysis.zb_test_labels[0], np.ndarray):  # one-hot coding, presumably from Keras
            if do_headers:
                DTOutput.write_headers( output_filename, ['Iteration','Fold','Class', {'base':'Label','length':n_labels}  ], self.delimiter )
                
            labels_to_write = list(map(list,zip(*self._analysis.zb_test_labels)))
            # this is a super-gross line of code, but it should work for either Python 2 or 3, and also for either numpy arrays or a list of Python lists
            # similar situation as above with the output scores... see comment up there
            
            for class_num in range(len(labels_to_write)):
                DTOutput.write_one_row_values(  output_filename, 
                                                ['{0:05d}', '{0}', '{0:03d}',                                         '{0:.9f}'], 
                                                [iteration,  fold, self._analysis.inverse_label_dict[class_num], labels_to_write[class_num]],
                                                self.delimiter                                  )
        
        else: # not one-hot coding, presumably from PyMVPA
            if do_headers:
                DTOutput.write_headers( output_filename, ['Iteration','Fold', {'base':'Label','length':n_labels}  ], self.delimiter ) #no need for 'Class' column
            
            DTOutput.write_one_row_values(  output_filename, 
                                            ['{0:05d}', '{0}', '{0:.9f}'], 
                                            [iteration,  fold, DTData.DTData.unmap_labels(self._analysis.zb_test_labels,self._analysis.inverse_label_dict) ],
                                            self.delimiter                                      )
    
    def write_output_metadata(self, iteration, fold, do_headers ):
        output_filename = self.build_a_filename_workshop("metadata")
        import getpass
        import socket
        
        #if the toolbox __init__ was not executed the version may need to be located by other means
        try:
            toolbox_version = self.__module__.version
        except: 
            delineate_toolbox_location = os.path.dirname(__file__)
            with open(os.path.join(delineate_toolbox_location,'VERSION.txt'),'r') as version_file:
                toolbox_version = version_file.read().strip()
                
        python_version = sys.version
        user = getpass.getuser()
        machine = socket.getfqdn()
        toolbox_location = os.path.realpath(sys.argv[0])
                
        if self._analysis.model.backend != 'pymvpa':
            import subprocess
            if do_headers:
                DTOutput.write_headers( output_filename, ['Iteration','Fold','Analysis_start_time','User_name', 'Host_name', 'Toolbox_location', 'Python_version','CUDA_version', 'MLL_name', 'MLL_version', 'Keras_version', 'Toolbox_version'], self.delimiter )
            #in theory it might be nice to return both driver and runtime versions for CUDA, but I'm not sure there's a reliable way for us to get at the driver so for now this is runtime only; it's also going to break if the formatting from nvcc ever changes
            #it's currently unclear how well this will translate across systems so we're going to try it and shrug if anything goes wrong
            try:
                system_check = subprocess.check_output("nvcc -V | grep release | awk '{print $NF}'", shell=True)
                cuda_version = str(system_check).strip("lnb'\\")
            except:
                cuda_version = 'undeterminable'
                
            #this is a lot of gross imports; might be better to see if we can harvest all of this when the model is actually built and store it in DTModel
            try:
                import keras
                mll_name = keras.backend.backend()
                backend_version = keras.__version__
                try:
                    if mll_name == 'theano':
                        import theano
                        mll_version = theano.__version__
                    elif mll_name == 'tensorflow':
                        import tensorflow
                        mll_version = tensorflow.__version__
                    elif mll_name == 'cntk':
                        import cntk
                        mll_version = cntk.__version__
                except:
                    mll_version = 'undeterminable'
            except:
                mll_name = 'undeterminable'
                mll_version = 'undeterminable'
                backend_version = 'undeterminable'
            
            # we might want to consider overriding delimiter here since it's going to get ugly with tabs, but it's also less critical for this file to be machine readable and it feels rude to preempt user settings so it's not happening now
            DTOutput.write_one_row_values(  output_filename, 
                                            ['{0:05d}', '{0}', '{0}', '{0}', '{0}', '{0}', '{0}', '{0}', '{0}', '{0}', '{0}', '{0}'], 
                                            [iteration,  fold, self._analysis.start_time, user, machine, toolbox_location, python_version, cuda_version, mll_name, mll_version, backend_version, toolbox_version], 
                                            self.delimiter                          )
        
        else:
            if do_headers:
                DTOutput.write_headers( output_filename, ['Iteration', 'Fold', 'Analysis_start_time', 'User_name', 'Host_name', 'Toolbox_location', 'Python_version', 'pymvpa_version', 'Toolbox_version'], self.delimiter )
            
            pymvpa_version = toolbox_version
            # we might want to consider overriding delimiter here since it's going to get ugly with tabs, but it's also less critical for this file to be machine readable and it feels rude to preempt user settings so it's not happening now
            DTOutput.write_one_row_values(  output_filename, 
                                            ['{0:05d}', '{0}', '{0}', '{0}', '{0}', '{0}', '{0}', '{0}', '{0}'], 
                                            [iteration,  fold, self._analysis.start_time, user, machine, toolbox_location, python_version, pymvpa_version, toolbox_version], 
                                            self.delimiter                          )
    
    
    def write_output_trained_model(self, iteration, fold):
        if self._analysis.model.backend != 'pymvpa':
            output_filename = self.build_a_filename_workshop("trained_model")
            iter_str = "iter{0:04d}".format( iteration ) #hard-coding for now, this is already kind of a weird situation so no need to make it weirder with lots of params and flags
            fold_str = "fold{0:04d}".format( fold )
            output_filename = output_filename.replace( 'xxxITERATIONxxx', iter_str )
            output_filename = output_filename.replace( 'xxxFOLDxxx', fold_str )
            self._analysis.model.model.save(output_filename)
        else:
            DTError.warn("Attempted to write trained model with a backend other than Keras. Currently we're only equipped to do this with Keras so, uh, SMOKE BOMB! [dashes away]   (In other words, ignoring that parameter.)")
    
    
    def write_output_tags(self, iteration, this_output_file_type, fold=0, do_headers=False):
        tag_name = this_output_file_type[5:]
        output_filename = self.build_a_filename_workshop(this_output_file_type)
        tags_to_write = self._analysis.dataset.sa[tag_name]
        tags_to_write = tags_to_write[self._analysis.test_indices]
        
        n_tags = len(tags_to_write)
        
        if do_headers:
            DTOutput.write_headers( output_filename, ['Iteration','Fold', {'base':'Tag','length':n_tags}  ], self.delimiter )
        
        DTOutput.write_one_row_values(  output_filename, 
                                        ['{0:05d}', '{0}', '{0}'], 
                                        [iteration,  fold, tags_to_write],
                                        self.delimiter                          )
    
    
    def write_output_kerasana(self, iteration, fold=0, do_headers=False):
        import random
        random.seed(int(self._analysis.test_acc*100))
        output_filename = self.build_a_filename_workshop("madame_kerasana")
        the_future                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          = ["It is certain","It is decidedly so","Without a doubt","Yes definitely","You may rely on it","As I see it, yes","Most likely","Outlook good","Yes","Signs point to yes","Reply hazy try again","Ask again later","Better not tell you now","Cannot predict now","Concentrate and ask again","Don't count on it","My reply is no","My sources say no","Outlook not so good","Very doubtful"]
        a_glimpse                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           = the_future[random.randrange(len(the_future))]
        
        if do_headers:
            DTOutput.write_headers( output_filename, ['Iteration','Fold','Prediction'], self.delimiter )
        
        DTOutput.write_one_row_values(  output_filename, 
                                        ['{0:05d}', '{0}', '{0}'], 
                                        [iteration,  fold, a_glimpse],
                                        self.delimiter                          )
    
    
    def write_output_timestamps(self, iteration, fold=0, do_headers=False ):
        output_filename = self.build_a_filename_workshop("timestamps")
        import datetime
        
        if do_headers:
            DTOutput.write_headers( output_filename, ['Iteration','Fold','Time'], self.delimiter )
            
        DTOutput.write_one_row_values(  output_filename, 
                                        ['{0:05d}', '{0}', '{0}'], 
                                        [iteration,  fold, datetime.datetime.now().isoformat('.')], #if anyone actually cared we would want to calculate this in DTModel, since this currently includes microsecond precision for the time of output, but it should do its actual job just fine
                                        self.delimiter                          )
        
        
    
    def write_job_config(self):
        filename = os.path.join(self.output_location, self.output_filename_stem + "_job_config.json")
        
        fid = open(filename,'at') #always append here -- overwriting is taken care of already when we get to this point
        out_str = json.dumps(self.job_struct,sort_keys=True,indent=3)
        fid.write(out_str)
        fid.close()
    
    
    def set_analysis(self, analysis):
        # Needs to be called before we can do anything good; _analysis should not be set directly as it requires a weak reference
        self._analysis = weakref.proxy(analysis)
    
    
    def canonicalize_all_output_setting(self, just_checking_filenames=False):
        if self.output_file_types[0] == "all":
            self.output_file_types = ["test_acc", "scores", "labels", "training_acc", "timestamps", "job_config", "metadata"]
            try:
                if self._analysis.model.backend != 'pymvpa':
                    self.output_file_types.append("validation_acc")
                    self.output_file_types.append("trained_model")
            except:
                # this is a little janky but one can only do so much to validate the "all" setting if no model is defined
                if just_checking_filenames:
                    self.output_file_types.append("validation_acc")
                    self.output_file_types.append("trained_model")
    
    
    def build_a_filename_workshop( self, this_output_file_type ):
        # not the BEST function name, but a beary good one
        
        if this_output_file_type == "job_config":
            output_filename = os.path.join(self.output_location, self.output_filename_stem + "_job_config.json")
            return output_filename #get rid of the weirdest case first; the rest are more similar to one another
        
        if this_output_file_type == "trained_model":
            output_filename = os.path.join(self.output_location, self.output_filename_stem + "_trained_model_xxxITERATIONxxx_xxxFOLDxxx.h5")
            return output_filename #another weird case, assume for now the calling function will know how to deal with the iter/fold tokens
        
        if this_output_file_type == "acc_summary" or this_output_file_type == "test_acc":
            output_type_code = "_accs"
        elif this_output_file_type == "scores":
            output_type_code = "_scores"
        elif this_output_file_type == "training_acc":
            output_type_code = "_training_acc"
#         elif this_output_file_type == "training_loss":
#             output_type_code = "_training_loss"       MRJ: TK?
        elif this_output_file_type == "validation_acc":
            output_type_code = "_validation_acc"
#         elif this_output_file_type == "validation_loss":
#             output_type_code = "_validation_loss"     MRJ: TK?
        elif this_output_file_type == "labels":
            output_type_code = "_labels"
        elif this_output_file_type == "madame_kerasana":
            output_type_code = "_madame_kerasana"
        elif this_output_file_type == "timestamps":
            output_type_code = "_timestamps"
        elif this_output_file_type == "metadata":
            output_type_code = "_metadata"
        elif this_output_file_type[0:5] == 'tags:':
            this_tag = this_output_file_type[5:]
            output_type_code = "_tags_" + this_tag
        else:
            DTError.err("DTOutput.build_a_filename_workshop(): Unsupported output type")
        
        output_filename = os.path.join(self.output_location, self.output_filename_stem + output_type_code + self.file_extension)
        return output_filename
    
    
    def check_existing(self):
        output_filenames = []
        any_exist = False
        self.output_file_types = DTUtils.listify( self.output_file_types )
        all_output_flag_set = False
        if self.output_file_types[0] == "all":
            all_output_flag_set = True
        self.canonicalize_all_output_setting( just_checking_filenames=True )
        for this_outfile in self.output_file_types:
            temp_filename = self.build_a_filename_workshop( this_outfile )
            output_filenames.append( temp_filename )
            if os.path.isfile(temp_filename):
                any_exist = True
        if all_output_flag_set: #a little weird, but only way I can see to do this without generating warnings
            self.output_file_types = ["all"]
                
        if not any_exist:
            return
        
        if self.pre_existing_file_behavior == 'prompt':
            new_filename_stem = None
            while new_filename_stem is None:
                print("One or more files beginning with '{}' already exist! Please enter a new filename stem.".format(self.output_filename_stem) )
                print("Instead of an actual filename stem, you can also enter:")
                print(" - 'overwrite' to replace the existing file(s)")
                print(" - 'append' to append to the existing file(s)")
                print(" - 'increment' to add a numeric increment to the filename(s)")
                print(" - 'quit' if you are a fraidy-cat and a quitter and just want to bail out here")
                new_filename_stem = DTUtils.dt_input("Your choice: ")
            
            if new_filename_stem.lower() == 'quit':
                print("Wow. You gave up that easily?")
                sys.exit()
            
            # these next few chunks: a little redundant but probably not worth getting fancy over, especially if we want to do anything
            #   more complicated for specific cases later on...?
            if new_filename_stem.lower() == 'overwrite':
                self.pre_existing_file_behavior = 'overwrite'
                self.check_existing()
                return
            
            if new_filename_stem.lower() == 'append':
                self.pre_existing_file_behavior = 'silent_append'
                self.check_existing()
                return
            
            if new_filename_stem.lower() == 'increment':
                self.pre_existing_file_behavior = 'increment'
                self.check_existing()
                return
            
            # all other options above either return or exit so no need for an 'else' here... must be a filename
            print("You have entered a new filename stem: {}".format( new_filename_stem ))
            print("Do you want to proceed with this?")
            print("(Type Y or yes to continue with this name, anything else to go back and pick again.)")
            zork = DTUtils.dt_input("Your choice: ")
            if (zork is not None) and ((zork.lower() == 'yes') or (zork.lower() == 'y')):
                self.output_filename_stem = new_filename_stem
            
            # either way, we should either check the NEW filename or just do it all over again
            self.check_existing()
            return        
        
        
        # all valid contents of pre_existing_file_behavior should be mutually exclusive, so no need for an 'else'
        #   (MRJ: my kingdom for a switch/case statement in Python!) 
        #   KK: switch/case is a vestigial line of code atop a pile of if statements, but I guess negative value is appropriate for a kingdom in academia
        if self.pre_existing_file_behavior == 'overwrite':
            for this_outfile in output_filenames:
                if os.path.isfile(this_outfile):
                    fid = open( this_outfile, 'wt' )
                    fid.close() #just create a blank file here so we can "append" later on regardless of pre_existing_file_behavior mode
        
        
        if self.pre_existing_file_behavior == 'silent_append':
            for this_outfile in output_filenames:
                if os.path.isfile(this_outfile):
                    fid = open( this_outfile, 'at' )
                    fid.write('\n\n\n\n')
                    fid.close() #just stick in a few blank lines to make the appendification clear
        
        
        if self.pre_existing_file_behavior == 'increment':
            increment_pattern = re.compile(self.output_filename_stem+"_\d{3}")
            all_existing_files = glob.glob(os.path.join(self.output_location,self.output_filename_stem)+'*'+self.file_extension)
            #KK: MRJ may wish to avert his eyes for a moment here, we're cracking the ark
            numbers_in_filenames = [int(re.search(increment_pattern,file).group(0)[-3:]) for file in all_existing_files if re.search(increment_pattern,file)]
            # official bet: if the above line ever breaks in normal usage, Karl owes Matt's zombie cookie jar $1
            #   (if we go like 10 years and it never breaks, Matt will concede that it isn't broken)
            #   disclaimer: bet invalid if we implement other code that breaks this line as a side effect
            
            current_increment = 1
            if numbers_in_filenames:
                current_increment += np.max(numbers_in_filenames)
                # MRJ: I resisted a very strong urge here to rename numbers_in_filenames to the much worse but much cleverer
                #      variable name 'increment_whether'
            
            current_increment_string = '{0:03d}'.format(current_increment)
            self.output_filename_stem = self.output_filename_stem + '_' + current_increment_string
            # MRJ: really tempted here to again recurse to double-check that the regexp stuff has ensured that we incremented correctly 
            #      and none of the files with incremented filenames exist... but if our regexp code is bug-free that should be pointless 
            #      and if it is buggy, I guess we go into an infinite recursion or something. So, no easy way to watch the watchmen here.
    
    
    @classmethod
    def write_headers(cls, filename, header_spec_list, delimiter="\t"):
        # each item in 'header_spec_list' can either be a string or a dict
        # if a string, the header just gets written out literally (with a delimiter)
        # if a dict, it should have two keys: 'base' and 'length'
        
        header_spec_list = DTUtils.listify( header_spec_list ) #turn single item into list if necessary
        fid = open(filename,'at') #always append here -- overwriting is taken care of already when we get to this point
        
        this_delim = "" #no delimiter for first item
        for header_spec in header_spec_list:
            if isinstance( header_spec, dict ):
                base = header_spec['base']
                n_items = header_spec['length']
                for i in range( n_items ):
                    header_string = base + '{0:07d}'.format( i ) # TK? maybe in future make number of digits optionally configurable; for now it's a pain
                    fid.write( this_delim + header_string )
                    this_delim = delimiter # a little inefficient if delimiter is big, but it's not likely to be
            else: #assume it's a string
                fid.write( this_delim + header_spec )
                this_delim = delimiter # a little inefficient if delimiter is big, but it's not likely to be
        
        fid.write("\n")
        fid.close()
    
    
    @classmethod
    def write_one_row_values(cls, filename, format_specs, values, delimiter="\t"):
        # 'format_specs' should be a list of format specification strings, e.g. ["{0:05d}", "{0:.9f}", "{0}"] or whatnot, one string for each item in 'values'
        # 'values' should be a list; each item in the list can either be a single value (string, number, whatever, as long as it matches corresponding format spec),
        #          or a list of such values, in which case each item in the list will be printed as a separate cell
        
        format_specs = DTUtils.listify( format_specs )
        values = DTUtils.listify( values )
        
        if len(format_specs) != len(values):
            DTError.err("DTOutput: Length of 'format_specs' does not match length of 'values' in input arguments!")
        
        fid = open( filename, 'at' ) # for now assume we always append
        this_delim = "" #no delimiter for first item
        for i in range(len(format_specs)):
            this_spec = format_specs[i]
            these_vals = values[i]
            these_vals = DTUtils.listify( these_vals )
            
            for this_val in these_vals:
                try:
                    string_to_write = this_delim + this_spec.format( this_val )
                    # MRJ: lazy way for us to write numbers with numeric formatting but fall back to string (or whatever)
                    #      if the class labels are non-numeric
                except ValueError:
                    string_to_write = this_delim + "{}".format( this_val )
                fid.write( string_to_write )
                this_delim = delimiter # a little inefficient if delimiter is big, but it's not likely to be
        
        fid.write("\n")
        fid.close()
    
    
    @classmethod
    def validate_arguments( cls, args ):
        # this is the laziest and dumbest possible version of a method like this, which really should inspect the arguments and give some intelligent
        # warning messages and so forth. But it'll do for now to get us off the ground
        try:
            val_args = copy.deepcopy( args )
            val_args['check_existing'] = False
            test_obj = cls( **val_args )
        except:
            global dt_debug_mode
            if 'dt_debug_mode' not in globals():
                dt_debug_mode = True # TK: change to false when we go into public production
            
            if dt_debug_mode:
                DTError.err( 'DTOutput: validate_arguments() failed' )
            else:
                return False
        return True
