import sys
import numpy

# note: does not yet differentiate between folds, because folds are not really supported in general yet
# TK, I guess?

n_args = len(sys.argv)
if n_args <= 1:
    print('No files specified! This script (dt_accs_summarizer.py) requires at least one command-line argument.')
    sys.exit()

print(' ')
print(' ')
for i in range(1, n_args):
    res_file = sys.argv[i]
    
    # TK: insert appropriate error checking here (e.g. better format verification)
    
    fid = open(res_file, 'rt')
    all_lines = fid.readlines()
    fid.close()
    
    print('Summary of: ' + res_file)
    accs = []
    losses = []
    for this_line in all_lines:
        this_line = this_line.strip( '\n' )
        this_line_cells = this_line.split('\t')
        try:
            first_cell_int = int(this_line_cells[0])
            this_mean_acc = float( this_line_cells[2] )
            this_mean_loss = float( this_line_cells[3] )
            
            accs.append(this_mean_acc)
            losses.append(this_mean_loss)
        except:
            continue
    
    overall_mean_acc = numpy.mean(accs)
    overall_mean_loss = numpy.mean(losses)
    
    overall_acc_std = numpy.std( accs, ddof=1 )
    overall_loss_std = numpy.std( losses, ddof=1 )
    
    nits = len(accs)
    overall_acc_stderr = overall_acc_std / (nits ** .5)
    overall_loss_stderr = overall_loss_std / (nits ** .5)
    
    print('- Nits: {0}'.format(nits))
    print('- Acc:  Mean = {0:10.6f},      Stdev = {1:10.6f},      StdErr = {2:10.6f}'.format(overall_mean_acc, overall_acc_std, overall_acc_stderr))
    print('- Loss: Mean = {0:10.6f},      Stdev = {1:10.6f},      StdErr = {2:10.6f}'.format(overall_mean_loss, overall_loss_std, overall_loss_stderr))
    
    print(' ')
