***PREFACE TO ALL DOCUMENTATION***: We have tried to be as comprehensive, helpful, and accurate as we can in all of these documents, but providing good documentation is always an uphill climb. Our code, and the underlying backend code we rely on, is always changing, which means things can easily go out of date; and doing these kinds of analyses is intrinsically a complicated process, which makes it hard to write documentation that works well for people at all different levels of technical proficiency and familiarity with the underlying concepts and technologies. We really don't want the learning curve to be a barrier to people using this toolbox, so we *highly* recommend -- especially while the number of users is relatively small and manageable -- getting in touch with the developers if you're confused, don't know where to start, etc., etc. And, of course, if you find any errors, omissions, or inconsistencies! Seriously, don't be a stranger... we are happy to add features, flesh out documentation, walk through setup, and so on, to help this project serve as many users as possible, but that requires hearing from you to help let us know what areas need the most attention. Please see our website (`http://delineate.it/`) and click on the contact page to get a link to our Bitbucket repo and an email address for the project devs.

---

# DTError (module) #

---

This is a simple utility module that gives us a little more control over how we output errors and warnings than Python's default exception handling. End users should not really have to interact with it (except to see the errors and warnings when they occur), but we are providing some brief notes here for the sake of completeness and to note a couple of oddities/future changes.

The main thing to note is that, in its current version, this toolbox is somewhere between in-development software and polished user-friendly software. You can certainly use it, but it is still in somewhat active development. This is mainly relevant, for present purposes, in terms of how we do error reporting in `DTError`.

Eventually, when/if the toolbox is all 100% nicely documented with a ton more features for validating user input and so on, we hope we can catch every error that might occur and print out a pretty, informative, human-readable error message.

However, while in development, it is usually better to print out a full "stack trace" (a list of what functions had been called and what line of code the program was executing when the fatal error occurred) so the programmer can figure out the issue.

Anyway, this behavior is controlled in DeLINEATE by a Python global variable called `dt_debug_mode`. If it is set to `True`, errors cause a stack trace to be printed, which is uglier for the end-user but more helpful to the devs. If it's `False`, no stack trace. Warnings are a little more complicated depending on what caused the warning, but in some cases they also are controlled by the `dt_debug_mode` setting.

If you care about this kind of thing at all, you can manually set `dt_debug_mode` on your own in a script somewhere and get stack traces or not, according to your whims. If you don't set it manually, you probably *will* get a stack trace for now, although note this behavior is subject to change in the future if we ever get things to a level of polish where our error messages are totally comprehensive and stack traces are rarely needed.

That is probably more than you ever wanted to know about a ~50-line module that just prints error messages.

<br><br>


## Functions ##


**err**( `messages` )

*(no return value)* Called when there is an error. Prints out the string or list of strings contained in the `messages` parameter, optionally displays a stack trace as described pleonastically above, and quits.


<br>

**warn**( `messages`, `trace`=*False* )

*(no return value)* Similar to `err()`, but doesn't quit the software, just prints out the error message and possibly a stack trace. In this function, whether the stack trace prints is determined by the `trace` parameter. If it's `True`, you get a stack trace; if it's `False`, you don't; and if it's set to the string `'defer_to_global'`, it does whatever the `dt_debug_mode` global variable indicates.

<br>

**stacktrace**( )

*(no return value)* Does the actual printing of the stack trace, by getting the relevant info from Python about the most recent exception and printing it out.


