import numpy as np
import scipy.io as sio

# A note to intrepid explorers: Unless you are one of the developers of this toolbox, you 
#   probably won't find much of interest in here. These are basically all loaders we used 
#   during development or for our own work that occurred in parallel, but are not explicitly
#   intended to be used by end-users. They are still here for historical purposes for now, 
#   but will be removed at some point. The only thing you might find them useful for, 
#   perhaps, would be a bit of inspiration about how you might write your own loader for 
#   your own weird-format dataset.


def ref_study_loader( loader_params ):
    # for now, needs to return samples, sa, fa
    data_filename = loader_params[0]
    label_filename = loader_params[1]
    my_samples = np.load(data_filename)
    my_labels = np.load(label_filename)
    my_samples = np.reshape(my_samples, (4555, 1147) )
    my_sa = {}
    my_sa['class'] = my_labels[:,1]
    return my_samples, my_sa, None


def jake_flat_loader( loader_params ) :
    data_filename = loader_params[0]
    label_filename = loader_params[1]
    my_samples = np.load(data_filename)
    my_labels = np.load(label_filename)
    my_samples = np.reshape(my_samples, (my_samples.shape[0], my_samples.shape[1]*my_samples.shape[2]) )
    my_sa = {}
    my_sa['class'] = my_labels[:,1]
    my_sa['subject'] = my_labels[:,0]
    return my_samples, my_sa, None


def jake_nonflat_loader( loader_params ) :
    data_filename = loader_params[0]
    label_filename = loader_params[1]
    my_samples = np.load(data_filename)
    my_labels = np.load(label_filename)
    my_sa = {}
    my_sa['class'] = my_labels[:,1]
    my_sa['subject'] = my_labels[:,0]
    return my_samples, my_sa, None


def jake_vslbin_nonflat_loader( loader_params ) :
    data_filename = loader_params[0]
    label_filename = loader_params[1]
    orig_samples = np.load(data_filename)
    newdims = list(np.shape(orig_samples)[0:2])
    newdims.append(np.int(np.shape(orig_samples)[2]/10))
    my_samples = np.zeros(newdims)
    my_labels = np.load(label_filename)
    
    np.mean(my_samples[1,1,:].reshape(-1,10),1)
    my_sa = {}
    my_sa['class'] = my_labels[:,1]
    my_sa['subject'] = my_labels[:,0]
    return my_samples, my_sa, None


def vsl_study_loader( loader_params ):
    # for now, needs to return samples, sa, fa
    data_filename = loader_params[0]
    mat_data = sio.loadmat( data_filename )
    my_samples = mat_data['all_subjs_data_of_interest']
    my_sa = {}
    my_sa['subj'] = mat_data['all_subjs_delineate_sa_subject'].flatten()
    my_sa['category'] = mat_data['all_subjs_delineate_sa_category'].flatten()
    return my_samples, my_sa, None


def vsl_study_loader_generic_multi_sa( loader_params ):
    # for now, needs to return samples, sa, fa
    data_filename = loader_params[0]
    mat_data = sio.loadmat( data_filename )
    my_samples = mat_data['all_subjs_data_of_interest']
    
    my_sa={}
    this_sa = loader_params[1]
    my_sa[this_sa] = mat_data[this_sa].flatten()
    
    i = 2
    samples_to_use = np.ones( my_samples.shape[0], dtype=bool )
    while i < len(loader_params):
        this_sa =  loader_params[i]
        this_val = loader_params[i+1]
        my_sa[this_sa] = mat_data[this_sa].flatten()
        samples_to_use = samples_to_use & (my_sa[this_sa] == this_val)
        i += 2
    
    my_samples = my_samples[samples_to_use,]
    
    for key in my_sa.keys():
        my_sa[key] = my_sa[key][samples_to_use,]
    
    return my_samples, my_sa, None


def vsl_study_loader_with_upcoming( loader_params ):
    # for now, needs to return samples, sa, fa
    data_filename = loader_params[0]
    mat_data = sio.loadmat( data_filename )
    my_samples = mat_data['all_subjs_data_of_interest']
    my_sa = {}
    my_sa['subj'] = mat_data['all_subjs_delineate_sa_subject'].flatten()
    my_sa['category'] = mat_data['all_subjs_delineate_sa_category'].flatten()
    my_sa['upcoming'] = mat_data['all_subjs_delineate_sa_upcoming'].flatten()
    return my_samples, my_sa, None


def vsl_study_loader_strength( loader_params ):
    # for now, needs to return samples, sa, fa
    data_filename = loader_params[0]
    mat_data = sio.loadmat( data_filename )
    my_samples = mat_data['all_subjs_data_of_interest']
    my_sa = {}
    my_sa['subj'] = mat_data['all_subjs_delineate_sa_subject'].flatten()
    my_sa['category'] = mat_data['all_subjs_delineate_sa_category'].flatten()
    my_sa['strongweak'] = mat_data['all_subjs_delineate_sa_strongweak'].flatten()
    return my_samples, my_sa, None


def vsl_study_loader_trialid( loader_params ):
    # for now, needs to return samples, sa, fa
    data_filename = loader_params[0]
    mat_data = sio.loadmat( data_filename )
    my_samples = mat_data['all_subjs_data_of_interest']
    my_sa = {}
    my_sa['subj'] = mat_data['all_subjs_delineate_sa_subject'].flatten()
    my_sa['category'] = mat_data['all_subjs_delineate_sa_category'].flatten()
    my_sa['trialid'] = mat_data['all_subjs_delineate_sa_trialid'].flatten()
    return my_samples, my_sa, None


def vsl_study_loader_strength_collapse_features( loader_params ):
    # for now, needs to return samples, sa, fa
    data_filename = loader_params[0]
    mat_data = sio.loadmat( data_filename )
    my_samples = mat_data['all_subjs_data_of_interest']
    my_samples = np.reshape(my_samples,(8209,4914))
#     my_inds = np.random.rand(4914) < 2
#     my_samples = my_samples[:,my_inds]
#     my_inds = np.random.rand(8209) < .40
#     my_samples = my_samples[my_inds,:]
    print(my_samples.shape)
    my_sa = {}
    my_sa['subj'] = mat_data['all_subjs_delineate_sa_subject'].flatten()
    my_sa['category'] = mat_data['all_subjs_delineate_sa_category'].flatten()
    my_sa['strongweak'] = mat_data['all_subjs_delineate_sa_strongweak'].flatten()
    return my_samples, my_sa, None


def HDF_eyetracking_loader( loader_params ):
    import hdf5storage
    import re
    mat = hdf5storage.loadmat(loader_params[0])
    for dict_key in list(mat.keys()):
        if not re.search('.*_labels$',dict_key):
            samples_key = dict_key
    my_samples = mat[samples_key]
    my_sa = {}
    my_sa['trial_type'] = mat['good_trial_data_trialtype_labels'].flatten()
    my_sa['subject'] = mat['good_trial_data_subject_labels'].flatten()
    return my_samples, my_sa, None


def HDF_eyetracking_loader_3chan( loader_params ):
    import hdf5storage
    import re
    mat = hdf5storage.loadmat(loader_params[0])
    for dict_key in list(mat.keys()):
        if not re.search('.*_labels$',dict_key):
            samples_key = dict_key
    my_samples = mat[samples_key]
    my_samples = np.reshape(my_samples,(np.shape(my_samples)[0],3,np.int(np.shape(my_samples)[1]/3)))
    my_sa = {}
    my_sa['trial_type'] = mat['good_trial_data_trialtype_labels'].flatten()
    my_sa['subject'] = mat['good_trial_data_subject_labels'].flatten()
    return my_samples, my_sa, None