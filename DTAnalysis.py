import copy
import numpy as np
import datetime
import DTData
from support_modules import DTError

class DTAnalysis:
        
    
    def __init__(self, nits=None, xval_type=None, nfolds=None, train_val_test=None, scaling_method=None, scaling=None, classify_over=None, backend_options=None, dataset=None, model=None, xval_sa=None, output_handler=None, slicing=True ):
        self.nits = nits
        self.xval_type = xval_type
        self.nfolds = nfolds
        self.train_val_test = train_val_test
        self.scaling_method = scaling_method
        self.scaling = scaling
        self.classify_over = classify_over
        self.backend_options = backend_options
        self.dataset = dataset
        self.model = model
        self.sync_backend_options_with_model(warn_if_none=False)
        self.xval_sa = xval_sa
        self.output_handler = output_handler
        self.slicing = slicing
        
        self.start_time = datetime.datetime.now().isoformat('.') # will be re-set in run(), but might as well
                                                                 #  go ahead and initialize, in case anyone is 
                                                                 #  planning on doing something weird
        # TK: insert appropriate error checking here
    
    
    def run(self):
        # TK: insert appropriate error checking here
        #  specifically, make sure all necessary attributes exist and are the right data type, etc.
        
        self.sync_backend_options_with_model(warn_if_none=False)
        
        if isinstance( self.nits, list ):
            nits = self.nits[0]
        else:
            nits = self.nits
        
        self.start_time = datetime.datetime.now().isoformat('.') # save for eventual metadata output
        
        for iteration_num in range(nits):
            print('DeLINEATE: Iteration {0}'.format(iteration_num+1))
            if self.xval_type == 'transfer_over_sa':
                self.run_transfer( iteration_num )
            elif self.xval_type == 'single':
                self.run_single( iteration_num )
            elif self.xval_type == 'loop_over_sa':
                self.run_loop_over_sa( iteration_num )
            elif self.xval_type == 'manualtest':
                self.run_manualtest( iteration_num )
            else:
                DTError.err("DTAnalysis: Unknown 'xval_type' parameter specified!")
    
    
    def run_transfer(self, iteration_num):
        from keras.models import load_model
        unique_sa_values = np.unique(self.dataset.sa[self.xval_sa])
        for sa_value in unique_sa_values:
            self.dataset.mask_sa(self.xval_sa, np.setdiff1d(unique_sa_values, sa_value))
            if self.slicing != None:
                [self.train_data, self.val_data, self.test_data, self.zb_train_labels, self.zb_val_labels, self.zb_test_labels, self.test_indices, self.inverse_label_dict] = self.dataset.slice_train_val_test( .7, .3, 0, self.classify_over )

            if self.scaling != None:
                self.rescale_data()

            [self.zb_train_labels, self.zb_val_labels, self.zb_test_labels] = DTData.DTData.train_val_test_to_categorical( self.zb_train_labels, self.zb_val_labels, self.zb_test_labels )
            self.model.build()
            self.model.train( self.train_data, self.val_data, self.zb_train_labels, self.zb_val_labels )
            self.model.model.save("./transfer_temp")

            self.dataset.mask_sa(self.xval_sa, sa_value)

            print("Trained for subject " + str(sa_value))
            #TK: Explicit check to see if nits is list
            for i in range(self.nits[1]):

                #Probably doesn't work with tensorflow. 
                #TK: Update to keras.models.clone_model(model, input_tensors) when we updated Keras to at least 2.0.7
                self.model.model = load_model("./transfer_temp")
                [self.train_data, self.val_data, self.test_data, self.zb_train_labels, self.zb_val_labels, self.zb_test_labels, self.inverse_label_dict] = self.dataset.slice_train_val_test( self.train_val_test[0], self.train_val_test[1], self.train_val_test[2], self.classify_over )
                if self.scaling != None:
                    self.rescale_data()

                [self.zb_train_labels, self.zb_val_labels, self.zb_test_labels] = DTData.DTData.train_val_test_to_categorical( self.zb_train_labels, self.zb_val_labels, self.zb_test_labels )
                self.model.train( self.train_data, self.val_data, self.zb_train_labels, self.zb_val_labels )
                # TK: fix to enable save_training_accuracy when we actually make transfer canonical and fix it up in other ways
                [self.test_acc, self.test_loss, self.test_prediction_scores] = self.model.test( self.test_data, self.zb_test_labels )
                print("RESULT FOR " + str(sa_value) + " ITERATION " + str(i) + " IS " + str(self.test_acc))
                
                fold_to_write = '{0},{1}'.format(sa_value, i)
                if iteration_num == 0 and sa_value == unique_sa_values[0] and i == 0:
                    self.output_handler.write_output( iteration_num, fold=fold_to_write, do_headers=True, do_config=True)
                else:
                    self.output_handler.write_output( iteration_num, fold=fold_to_write )
    
    
    def run_loop_over_sa(self, iteration_num):
        unique_sa_values = np.unique(self.dataset.sa[self.xval_sa])
        for sa_value in unique_sa_values:
            print('DeLINEATE: Looping over SA values; current fold value is {0}'.format(sa_value))
            self.dataset.mask_sa(self.xval_sa, sa_value)
            
            if self.slicing != None:
                if type(self.slicing) != dict:
                    [self.train_data, self.val_data, self.test_data, self.zb_train_labels, self.zb_val_labels, self.zb_test_labels, self.test_indices, self.inverse_label_dict] = self.dataset.slice_train_val_test( self.train_val_test[0], self.train_val_test[1], self.train_val_test[2], self.classify_over )
                else:
                    [self.train_data, self.val_data, self.test_data, self.zb_train_labels, self.zb_val_labels, self.zb_test_labels, self.test_indices, self.inverse_label_dict] = self.dataset.handle_custom_slicer( self.train_val_test, self.classify_over, self.slicing )
            
            if self.scaling != None:
                self.rescale_data()
            if self.model.backend == 'keras':
                [self.zb_train_labels, self.zb_val_labels, self.zb_test_labels] = DTData.DTData.train_val_test_to_categorical( self.zb_train_labels, self.zb_val_labels, self.zb_test_labels )
            
            self.model.build()
            self.model.train( self.train_data, self.val_data, self.zb_train_labels, self.zb_val_labels )
            self.train_acc  = self.model.training_history['acc']
            self.train_loss = self.model.training_history['loss']
            self.val_acc    = self.model.training_history['val_acc']
            self.val_loss   = self.model.training_history['val_loss']
            
            [self.test_acc, self.test_loss, self.test_prediction_scores] = self.model.test( self.test_data, self.zb_test_labels )
            
            if iteration_num == 0 and sa_value == unique_sa_values[0]:
                self.output_handler.write_output( iteration_num, fold=sa_value, do_headers=True, do_config=True)
            else:
                self.output_handler.write_output( iteration_num, fold=sa_value )
            
            print('\n')
    
    
    def run_single(self, iteration_num):
        if self.slicing != None:
            if type(self.slicing) != dict:
                [self.train_data, self.val_data, self.test_data, self.zb_train_labels, self.zb_val_labels, self.zb_test_labels, self.test_indices, self.inverse_label_dict] = self.dataset.slice_train_val_test( self.train_val_test[0], self.train_val_test[1], self.train_val_test[2], self.classify_over )
            else:
                [self.train_data, self.val_data, self.test_data, self.zb_train_labels, self.zb_val_labels, self.zb_test_labels, self.test_indices, self.inverse_label_dict] = self.dataset.handle_custom_slicer( self.train_val_test, self.classify_over, self.slicing )
        
        if self.scaling != None:
            self.rescale_data()
        if self.model.backend == 'keras':
            [self.zb_train_labels, self.zb_val_labels, self.zb_test_labels] = DTData.DTData.train_val_test_to_categorical( self.zb_train_labels, self.zb_val_labels, self.zb_test_labels )
        
        self.model.build()
        self.model.train( self.train_data, self.val_data, self.zb_train_labels, self.zb_val_labels )
        self.train_acc  = self.model.training_history['acc']
        self.train_loss = self.model.training_history['loss']
        self.val_acc    = self.model.training_history['val_acc']
        self.val_loss   = self.model.training_history['val_loss']
        
        [self.test_acc, self.test_loss, self.test_prediction_scores] = self.model.test( self.test_data, self.zb_test_labels )
        
        if iteration_num == 0:
            self.output_handler.write_output( iteration_num, do_headers=True, do_config=True)
        else:
            self.output_handler.write_output( iteration_num )
            
        print('\n')
    
    
    def run_manualtest(self, iteration_num):
    # NEW as of somewhere-between-version-0.4b-and-0.5b... manual test data selection!
    #   - in case you want to randomly slice up your training and validation subsets but use a specific held-out dataset
    #     - note that if you want to do everything fully manual, that can be done just by setting "slicing" to None (null in JSON)
    #     - (and making sure in code that train_data, val_data, test_data, zb_train_labels, zb_val_labels, zb_test_labels, and inverse_label_dict exist...
    #        and also test_indices if you want to write out 'tags' in DTOutput)
    #     - though full-manual probably doesn't need to be run for more than a single iteration, mostly
    #     - whereas manual-test might benefit from different random slicings of training/validation
    #   - for 'manualtest', just need to make sure in code that test_data and zb_test_labels exist
    #     - and test_indices if you wanted to write out tags, but it seems like a relatively unlikely scenario
    #     - note that inverse_label_dict will still be created while slicing training/validation data, so the user is responsible for making 
    #       zb_test_labels compatible with that... which is going to get messy if your class labels are not just 0, 1, 2, etc. So probably best 
    #       to only use 'manualtest' if all your labels are zero-based integers to start with, unless you want headaches
    #     - in the future, we might essentially replace this with a 'separate_test' option or something that works with JSON job files,
    #       which would entail an extension to the DTData spec but would allow a bunch of the messiness to be handled automatically (TK!)
    #   - also we're going to require that the 'test' part of train_val_test is 0, because nothing else makes sense
    #   - this function is somewhat experimental, but a fairly straightforward modification of run_single so should be pretty safe
    #   - will still do scaling (if requested) and to_categorical conversion for Keras stuff
    #   - xval_type should be set to 'manualtest' to run this
    
        if self.slicing != None:
            if self.train_val_test[2] != 0:
                DTError.err("DTAnalysis: If using 'manualtest' cross-validation, the 'test' portion of train_val_test should be set to 0.")
            [self.train_data, self.val_data, junk_ignore, self.zb_train_labels, self.zb_val_labels, junk_ignore, junk_ignore, self.inverse_label_dict] = self.dataset.slice_train_val_test( self.train_val_test[0], self.train_val_test[1], self.train_val_test[2], self.classify_over )
        else:
            DTError.err("DTAnalysis: If using 'manualtest' cross-validation, 'slicing' should not be None. If you don't want any train/val/test slicing at all, just use the 'single' cross-validation method.")
        
        if self.scaling != None:
            self.rescale_data()
        if self.model.backend == 'keras':
            [self.zb_train_labels, self.zb_val_labels, self.zb_test_labels] = DTData.DTData.train_val_test_to_categorical( self.zb_train_labels, self.zb_val_labels, self.zb_test_labels )
        
        self.model.build()
        self.model.train( self.train_data, self.val_data, self.zb_train_labels, self.zb_val_labels )
        self.train_acc  = self.model.training_history['acc']
        self.train_loss = self.model.training_history['loss']
        self.val_acc    = self.model.training_history['val_acc']
        self.val_loss   = self.model.training_history['val_loss']
        
        [self.test_acc, self.test_loss, self.test_prediction_scores] = self.model.test( self.test_data, self.zb_test_labels )
        
        if iteration_num == 0:
            self.output_handler.write_output( iteration_num, do_headers=True, do_config=True)
        else:
            self.output_handler.write_output( iteration_num )
            
        print('\n')
    
    
    def sync_backend_options_with_model(self, warn_if_none=True, warn_if_different=True):
        if self.model is None:
            if warn_if_none:
                DTError.warn("DTAnalysis: Warning! DTAnalysis.sync_backend_options_with_model() was called, but self.model is None! Not doing anything here.")
            return
        
        if self.model.backend_options is None:
            self.model.backend_options = self.backend_options
            return
        
        if self.model.backend_options != self.backend_options:
            self.model.backend_options = self.backend_options
            if warn_if_different:
                warn_message = ["DTAnalysis: Warning! DTAnalysis.sync_backend_options_with_model() was called, and the model's backend options were different!",
                                "            This is OK if you meant to update the model with new backend options from your DTAnalysis object, but you might",
                                "            need to look into it further if this message is unexpected to you."]
                DTError.warn(warn_message)
    
    
    #TK: maybe change into a DTData classmethod like PyMVPA wrapping
    def rescale_data(self):
        if (self.scaling is None) and (self.scaling_method is None):
            return #should have already been checked but you never know
        
        #for the sake of backwards compatibility, if scaling is specified with no method assume percentile
        if (self.scaling_method == "percentile") or (self.scaling_method is None):
            if self.scaling >= 100 or self.scaling <= 1:
                DTError.err("DTAnalysis: Scaling needs to be >1 and <100 if you are going to use it (representing a percentile)")
            
            scale_value = np.percentile(np.abs(self.train_data), self.scaling)
            self.train_data = self.train_data / scale_value
            self.val_data = self.val_data / scale_value
            self.test_data = self.test_data / scale_value
            
        elif self.scaling_method == "standardize":
            training_mean = np.mean(self.train_data)
            training_std = np.std(self.train_data)
            self.train_data = (self.train_data - training_mean)/training_std
            self.val_data = (self.val_data - training_mean)/training_std
            self.test_data = (self.test_data - training_mean)/training_std
            
        elif self.scaling_method == "map_range":
            if self.scaling is None:
                targ_min = 0
                targ_max = 1
            else:
                targ_min = self.scaling[0]
                targ_max = self.scaling[1]
            training_min = np.min(self.train_data)
            training_max = np.max(self.train_data)
            max_min_dist = training_max - training_min
            
            norm_train_data = (self.train_data - training_min)/max_min_dist
            self.train_data = norm_train_data * (targ_max-targ_min) + targ_min
            norm_val_data = (self.val_data - training_min)/max_min_dist
            self.val_data = norm_val_data * (targ_max-targ_min) + targ_min
            norm_test_data = (self.test_data - training_min)/max_min_dist
            self.test_data = norm_test_data * (targ_max-targ_min) + targ_min
            
        elif self.scaling_method == "mean_center":
            training_mean = np.mean(self.train_data)
            self.train_data = self.train_data - training_mean
            self.val_data = self.val_data - training_mean
            self.test_data = self.test_data - training_mean
            
        else:
            DTError.err("DTAnalysis: Scaling method '{0}' is not currently supported. Implemented options are: percentile, standardize, map_range, and mean_center.".format(str(self.scaling_type)))
    
    
    @classmethod
    def validate_arguments( cls, args ):
        # this is the laziest and dumbest possible version of a method like this, which really should inspect the arguments and give some intelligent
        # warning messages and so forth. But it'll do for now to get us off the ground
        try:
            val_args = copy.deepcopy( args )
            test_obj = cls( **val_args )
        except:
            global dt_debug_mode
            if 'dt_debug_mode' not in globals():
                dt_debug_mode = True # TK: change to false when we go into public production
            
            if dt_debug_mode:
                DTError.err( 'DTAnalysis: validate_arguments() failed' )
            else:
                return False
        return True