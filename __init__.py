import os

delineate_toolbox_location = os.path.dirname(__file__)

with open(os.path.join(delineate_toolbox_location,'VERSION.txt'),'r') as version_file:
    version = version_file.read().strip()
