## things this should do:
# drop-down menu selection for categorical parameters
# entry box + sanity checking for numerical parameters
# ideally, visualization of architecture when keras is selected
# buttons to auto-build some simple architectures
# create a json file of all parameters when a button is clicked
# read existing json files to auto-populate all choices
# crash less often than eeglab

# issues to keep in mind: there are some Python 2 vs. 3 differences in tkinter - coverng the startup and creation ones now, then writing for Python 3 with the intent of pretending I will do a full compatibility sweep once things function there

import sys, json

if sys.version_info.major == 3:
    import tkinter as tk
    from tkinter import ttk
    from tkinter import filedialog
    python_env = 3
elif sys.version_info.major == 2:
    import Tkinter as tk
    import ttk
    import tkFileDialog as filedialog
    python_env = 2
else:
    print("Per sys.version_info.major Python version does not appear to be 2 or 3, which we can't handle.")

class DTGui(tk.Frame):
    
    def __init__(self, master = None):
        if python_env == 3:
            super().__init__(master)
        elif python_env == 2:
            tk.Frame.__init__(self, master)
        self.master.wm_title("DTGui")
        self.canvas = tk.Canvas(root, borderwidth=0, height = 600, width = 1000)
        self.outer_frame = tk.Frame(self.canvas)
        self.vsb = tk.Scrollbar(root, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.vsb.set)
        self.vsb.pack(side="right", fill="y")
        self.canvas.pack(side="left", fill="both", expand=True)
        self.canvas.create_window((4,4), window=self.outer_frame, anchor="nw", 
                                  tags="self.outer_frame")

        self.outer_frame.bind("<Configure>", self.onFrameConfigure)
        self.job_struct = {'data':{'loader':None,'loader_params':None}, 'analysis':{'nits':None,'xval_type':None,'train_val_test':None,'scaling':None,'scaling_type':None,'classify_over':None,'backend_options':{}}, 'output':{'output_location':None,'output_filename_stem':None,'output_file_types':{}}, 'model':{'compile_options':{},'layers':None,'classifier':{'classifier_type':None,'svm_impl':None,'implementation':None,'params':{}},'backend':None}}
        self.create_base_widgets()
        self.pack()
        
    def onFrameConfigure(self, event):
        # Reset the scroll region to encompass the inner frame; scrollbar stuff shamelessly stolen from 
        # https://stackoverflow.com/questions/3085696/adding-a-scrollbar-to-a-group-of-widgets-in-tkinter/3092341#3092341
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))
            
    def create_base_widgets(self):
        # backend selection and one subframe per primary DT class        
        self.outer_frame.backend_label = ttk.Label(self.outer_frame,text = "Select backend: ")
        self.outer_frame.backend_val = tk.StringVar(name='model;backend')
        self.outer_frame.backend_val.trace_add("write", self.update_job_struct)
        self.outer_frame.backend = ttk.Combobox(self.outer_frame, state='readonly',values=['PyMVPA','Keras'],textvariable=self.outer_frame.backend_val)
        self.outer_frame.backend.bind("<<ComboboxSelected>>", self.create_model_frame)
        self.outer_frame.backend_label.pack({"side":"top"})
        self.outer_frame.backend.pack({"side":"top"})
        self.create_data_frame()
        self.create_analysis_frame()
        self.create_output_frame()
        self.outer_frame.load_json = ttk.Button(self.outer_frame, text='Load settings from JSON file', command = self.load_json)
        self.outer_frame.save_json = ttk.Button(self.outer_frame, text='Save settings to JSON file', command = self.save_json)
        self.outer_frame.close = ttk.Button(self.outer_frame, text='Close GUI', command = self.quit)
        self.outer_frame.close.pack({"side":"bottom"})
        self.outer_frame.save_json.pack({"side":"bottom"})
        self.outer_frame.load_json.pack({"side":"bottom"})
        
        
    def create_model_frame(self, event=None):
        if (self.outer_frame.backend_val.get() == 'PyMVPA') | (self.outer_frame.backend_val.get() == 'pymvpa'):  
            if hasattr(self.outer_frame,'model_frame_keras'):
                self.outer_frame.model_frame_keras.pack_forget()
            if not hasattr(self.outer_frame,'model_frame_pymvpa'):
                self.outer_frame.model_frame_pymvpa = ttk.LabelFrame(self.outer_frame, text="Model")
                self.outer_frame.model_frame_pymvpa.model_type_label = ttk.Label(self.outer_frame.model_frame_pymvpa, text="Select classifier type: ")
                self.outer_frame.model_frame_pymvpa.model_type_val = tk.StringVar(name='model;classifier;classifier_type')
                self.outer_frame.model_frame_pymvpa.model_type_val.trace_add("write", self.update_job_struct)
                self.outer_frame.model_frame_pymvpa.model_type = ttk.Combobox(self.outer_frame.model_frame_pymvpa, state='readonly',values=['SVM','SMLR'],textvariable=self.outer_frame.model_frame_pymvpa.model_type_val)
                self.outer_frame.model_frame_pymvpa.model_type.bind("<<ComboboxSelected>>", self.pymvpa_model_handler)
            self.outer_frame.model_frame_pymvpa.pack()
            self.outer_frame.model_frame_pymvpa.model_type_label.pack()
            self.outer_frame.model_frame_pymvpa.model_type.pack()
        elif (self.outer_frame.backend_val.get() == 'Keras') | (self.outer_frame.backend_val.get() == 'keras'):
            if hasattr(self.outer_frame,'model_frame_pymvpa'):
                self.outer_frame.model_frame_pymvpa.pack_forget()
            if not hasattr(self.outer_frame,'model_frame_keras'):
                self.outer_frame.model_frame_keras = ttk.LabelFrame(self.outer_frame, text="Model")
                self.outer_frame.model_frame_keras.previous_model_layercount = 0
                self.outer_frame.model_frame_keras.compile_options_frame = ttk.LabelFrame(self.outer_frame.model_frame_keras, text="Compile options")
                self.outer_frame.model_frame_keras.compile_options_frame.loss_label = ttk.Label(self.outer_frame.model_frame_keras.compile_options_frame, text="Select loss type: ")
                self.outer_frame.model_frame_keras.compile_options_frame.loss_val = tk.StringVar(name='model;compile_options;loss')
                self.outer_frame.model_frame_keras.compile_options_frame.loss_val.trace_add("write", self.update_job_struct)
                self.outer_frame.model_frame_keras.compile_options_frame.loss_val.set('categorical_crossentropy')
                self.outer_frame.model_frame_keras.compile_options_frame.loss = ttk.Combobox(self.outer_frame.model_frame_keras.compile_options_frame, state='readonly',values=['categorical_crossentropy', 'mean_squared_error', 'mean_absolute_error', 'mean_absolute_percentage_error', 'mean_squared_logarithmic_error', 'squared_hinge', 'hinge', 'categorical_hinge', 'logcosh', 'huber_loss', 'sparse_categorical_crossentropy', 'binay_crossentropy', 'kuulback_leibler_divergence', 'poisson', 'cosine_proximity', 'is_categorical_crossentropy'],textvariable=self.outer_frame.model_frame_keras.compile_options_frame.loss_val)
                self.outer_frame.model_frame_keras.compile_options_frame.optimizer_label = ttk.Label(self.outer_frame.model_frame_keras.compile_options_frame, text="Select optimizer type: ")
                self.outer_frame.model_frame_keras.compile_options_frame.optimizer_val = tk.StringVar(name='model;compile_options;optimizer')
                self.outer_frame.model_frame_keras.compile_options_frame.optimizer_val.trace_add("write", self.update_job_struct)
                self.outer_frame.model_frame_keras.compile_options_frame.optimizer_val.set('Adam')
                self.outer_frame.model_frame_keras.compile_options_frame.optimizer = ttk.Combobox(self.outer_frame.model_frame_keras.compile_options_frame, state='readonly',values=['Adam', 'RMSprop', 'SGD', 'Adagrad', 'Adadelta', 'Adamax', 'Nadam'],textvariable=self.outer_frame.model_frame_keras.compile_options_frame.optimizer_val)
                self.outer_frame.model_frame_keras.compile_options_frame.optimizer_opts_label = ttk.Label(self.outer_frame.model_frame_keras.compile_options_frame, text="Enter additional optimizer parameters (name,value format; one per line): ")
                self.outer_frame.model_frame_keras.compile_options_frame.optimizer_opts = tk.Text(self.outer_frame.model_frame_keras.compile_options_frame, height = 2)
                self.outer_frame.model_frame_keras.model_layercount_label = ttk.Label(self.outer_frame.model_frame_keras, text="Select number of layers: ")
                self.outer_frame.model_frame_keras.model_layercount_val = tk.IntVar(name='layercount')
                self.outer_frame.model_frame_keras.model_layercount = tk.Spinbox(self.outer_frame.model_frame_keras, from_=0, to=999999999, textvariable=self.outer_frame.model_frame_keras.model_layercount_val)
                self.outer_frame.model_frame_keras.model_layercount_val.trace_add("write", self.keras_model_handler)
                self.outer_frame.model_frame_keras.model_layercount_val.set(0)
            self.outer_frame.model_frame_keras.pack()
            self.outer_frame.model_frame_keras.compile_options_frame.pack()
            self.outer_frame.model_frame_keras.compile_options_frame.loss_label.pack()
            self.outer_frame.model_frame_keras.compile_options_frame.loss.pack()
            self.outer_frame.model_frame_keras.compile_options_frame.optimizer_label.pack()
            self.outer_frame.model_frame_keras.compile_options_frame.optimizer.pack()
            self.outer_frame.model_frame_keras.compile_options_frame.optimizer_opts_label.pack()
            self.outer_frame.model_frame_keras.compile_options_frame.optimizer_opts.pack()
            self.outer_frame.model_frame_keras.model_layercount_label.pack()
            self.outer_frame.model_frame_keras.model_layercount.pack()
        else:
            print(['Error checking backend_val: ' + self.outer_frame.backend_val.get()])
                
    def pymvpa_model_handler(self, event=None):
        if self.outer_frame.model_frame_pymvpa.model_type_val.get().lower() == 'svm':
            if hasattr(self.outer_frame.model_frame_pymvpa, 'smlr_model_implentation_label'):
                self.outer_frame.model_frame_pymvpa.smlr_model_implentation_label.pack_forget()
                self.outer_frame.model_frame_pymvpa.smlr_model_implentation.pack_forget()
                self.outer_frame.model_frame_pymvpa.smlr_lambda_val_label.pack_forget()
                self.outer_frame.model_frame_pymvpa.smlr_lambda.pack_forget()
                self.outer_frame.model_frame_pymvpa.smlr_classifier_params_label.pack_forget()
                self.outer_frame.model_frame_pymvpa.smlr_classifier_params.pack_forget()
            if not hasattr(self.outer_frame.model_frame_pymvpa, 'svm_model_implentation_label'):
                self.outer_frame.model_frame_pymvpa.svm_model_implentation_label = ttk.Label(self.outer_frame.model_frame_pymvpa, text="Select SVM implementation: ")
                self.outer_frame.model_frame_pymvpa.svm_model_implentation_val = tk.StringVar(name='model;classifier;svm_impl')
                self.outer_frame.model_frame_pymvpa.svm_model_implentation_val.trace_add("write", self.update_job_struct)
                self.outer_frame.model_frame_pymvpa.svm_model_implentation_val.set('C_SVC')
                self.outer_frame.model_frame_pymvpa.svm_model_implentation = ttk.Combobox(self.outer_frame.model_frame_pymvpa, state='readonly',values=['C_SVC', 'ONE_CLASS', 'NU_SVR', 'NU_SVC', 'EPSILON_SVR'],textvariable=self.outer_frame.model_frame_pymvpa.svm_model_implentation_val)
                self.outer_frame.model_frame_pymvpa.svm_c_val_label = ttk.Label(self.outer_frame.model_frame_pymvpa, text="Select value of C parameter: ")
                self.outer_frame.model_frame_pymvpa.svm_c_val = tk.DoubleVar(name='model;classifier;params;C')
                self.outer_frame.model_frame_pymvpa.svm_c_val.trace_add("write", self.update_job_struct)
                self.outer_frame.model_frame_pymvpa.svm_c_val.set(-1)
                self.outer_frame.model_frame_pymvpa.svm_c = tk.Spinbox(self.outer_frame.model_frame_pymvpa, from_=-999999999, to=999999999, textvariable=self.outer_frame.model_frame_pymvpa.svm_c_val)
                self.outer_frame.model_frame_pymvpa.svm_kernel_type_label = ttk.Label(self.outer_frame.model_frame_pymvpa, text="Select kernel type: ")
                self.outer_frame.model_frame_pymvpa.svm_kernel_type_val = tk.StringVar(name='model;classifier;params;kernel')
                self.outer_frame.model_frame_pymvpa.svm_kernel_type_val.trace_add("write", self.update_job_struct)
                self.outer_frame.model_frame_pymvpa.svm_kernel_type_val.set('linear')
                self.outer_frame.model_frame_pymvpa.svm_kernel_type = ttk.Combobox(self.outer_frame.model_frame_pymvpa, state='readonly',values=['linear', 'rbf', 'polynomial', 'sigmoid'],textvariable=self.outer_frame.model_frame_pymvpa.svm_kernel_type_val)
                self.outer_frame.model_frame_pymvpa.svm_classifier_params_label = ttk.Label(self.outer_frame.model_frame_pymvpa, text="Additional classifier parameters (name,value format; one per line): ")
                self.outer_frame.model_frame_pymvpa.svm_classifier_params = tk.Text(self.outer_frame.model_frame_pymvpa, height=1)
                self.outer_frame.model_frame_pymvpa.svm_kernel_params_label = ttk.Label(self.outer_frame.model_frame_pymvpa, text="Additional kernel parameters (name,value format; one per line): ")
                self.outer_frame.model_frame_pymvpa.svm_kernel_params = tk.Text(self.outer_frame.model_frame_pymvpa, height=1)                
            self.outer_frame.model_frame_pymvpa.svm_model_implentation_label.pack()
            self.outer_frame.model_frame_pymvpa.svm_model_implentation.pack()
            self.outer_frame.model_frame_pymvpa.svm_c_val_label.pack()
            self.outer_frame.model_frame_pymvpa.svm_c.pack()
            self.outer_frame.model_frame_pymvpa.svm_kernel_type_label.pack()
            self.outer_frame.model_frame_pymvpa.svm_kernel_type.pack()
            self.outer_frame.model_frame_pymvpa.svm_classifier_params_label.pack()
            self.outer_frame.model_frame_pymvpa.svm_classifier_params.pack()
            self.outer_frame.model_frame_pymvpa.svm_kernel_params_label.pack()
            self.outer_frame.model_frame_pymvpa.svm_kernel_params.pack()
        if self.outer_frame.model_frame_pymvpa.model_type_val.get().lower() == 'smlr':
            if hasattr(self.outer_frame.model_frame_pymvpa, 'svm_model_implentation_label'):
                self.outer_frame.model_frame_pymvpa.svm_model_implentation_label.pack_forget()
                self.outer_frame.model_frame_pymvpa.svm_model_implentation.pack_forget()
                self.outer_frame.model_frame_pymvpa.svm_c_val_label.pack_forget()
                self.outer_frame.model_frame_pymvpa.svm_c.pack_forget()
                self.outer_frame.model_frame_pymvpa.svm_kernel_type_label.pack_forget()
                self.outer_frame.model_frame_pymvpa.svm_kernel_type.pack_forget()
                self.outer_frame.model_frame_pymvpa.svm_classifier_params_label.pack_forget()
                self.outer_frame.model_frame_pymvpa.svm_classifier_params.pack_forget()
                self.outer_frame.model_frame_pymvpa.svm_kernel_params_label.pack_forget()
                self.outer_frame.model_frame_pymvpa.svm_kernel_params.pack_forget()
            if not hasattr(self.outer_frame.model_frame_pymvpa, 'smlr_model_implentation_label'):
                self.outer_frame.model_frame_pymvpa.smlr_model_implentation_label = ttk.Label(self.outer_frame.model_frame_pymvpa, text="Select SMLR implementation: ")                
                self.outer_frame.model_frame_pymvpa.smlr_model_implentation_val = tk.StringVar(name='model;classifier;implementation')
                self.outer_frame.model_frame_pymvpa.smlr_model_implentation_val.trace_add("write", self.update_job_struct)
                self.outer_frame.model_frame_pymvpa.smlr_model_implentation_val.set('C')
                self.outer_frame.model_frame_pymvpa.smlr_model_implentation = ttk.Combobox(self.outer_frame.model_frame_pymvpa, state='readonly',values=['C', 'Python'],textvariable=self.outer_frame.model_frame_pymvpa.smlr_model_implentation_val)
                self.outer_frame.model_frame_pymvpa.smlr_lambda_val_label = ttk.Label(self.outer_frame.model_frame_pymvpa, text="Select value of lambda parameter: ")                
                self.outer_frame.model_frame_pymvpa.smlr_lambda_val = tk.DoubleVar(name='model;classifier;params;lm')
                self.outer_frame.model_frame_pymvpa.smlr_lambda_val.trace_add("write", self.update_job_struct)
                self.outer_frame.model_frame_pymvpa.smlr_lambda_val.set(-1)
                self.outer_frame.model_frame_pymvpa.smlr_lambda = tk.Spinbox(self.outer_frame.model_frame_pymvpa, from_=-999999999, to=999999999, textvariable=self.outer_frame.model_frame_pymvpa.smlr_lambda_val)                
                self.outer_frame.model_frame_pymvpa.smlr_classifier_params_label = ttk.Label(self.outer_frame.model_frame_pymvpa, text="Additional classifier parameters (name,value format; one per line): ")                
                self.outer_frame.model_frame_pymvpa.smlr_classifier_params = tk.Text(self.outer_frame.model_frame_pymvpa, height=1)                
            self.outer_frame.model_frame_pymvpa.smlr_model_implentation_label.pack()
            self.outer_frame.model_frame_pymvpa.smlr_model_implentation.pack()
            self.outer_frame.model_frame_pymvpa.smlr_lambda_val_label.pack()
            self.outer_frame.model_frame_pymvpa.smlr_lambda.pack()
            self.outer_frame.model_frame_pymvpa.smlr_classifier_params_label.pack()
            self.outer_frame.model_frame_pymvpa.smlr_classifier_params.pack()
        
    def keras_model_handler(self, *args):
        #Because Tkinter is considerate of no actual use cases: while a user is using the keyboard to 
        #replace a spinbox value, tracing a linked variable on write will get you an empty string 
        #once in between numbers; this doesn't crash anything but it does make an ugly error 
        #so we're going to step on it when that happens. May be obsoleted if/when we do more
        #aggressive validation.
        try:
            self.outer_frame.model_frame_keras.model_layercount_val.get()
        except Exception:
            return
        
        if not hasattr(self.outer_frame.model_frame_keras, 'layers'):
            self.outer_frame.model_frame_keras.layers = dict()
        if self.outer_frame.model_frame_keras.previous_model_layercount < self.outer_frame.model_frame_keras.model_layercount_val.get():
            for this_layer in range(self.outer_frame.model_frame_keras.previous_model_layercount,self.outer_frame.model_frame_keras.model_layercount_val.get()):
                self.outer_frame.model_frame_keras.layers[this_layer] = ttk.LabelFrame(self.outer_frame.model_frame_keras, text="Layer " + str(this_layer) + " parameters", name='model;layers;layer_'+str(this_layer)+';layer_frame')
                self.outer_frame.model_frame_keras.layers[this_layer].layer_type_label = ttk.Label(self.outer_frame.model_frame_keras.layers[this_layer], text="Layer type: ", name='layer_type_label')
                self.outer_frame.model_frame_keras.layers[this_layer].layer_type_val = tk.StringVar(name='model;layers;layer_'+str(this_layer)+';layer_type')
                self.outer_frame.model_frame_keras.layers[this_layer].layer_type = ttk.Combobox(self.outer_frame.model_frame_keras.layers[this_layer], state='readonly', values=['Reshape', 'LeakyReLU', 'BatchNormalization', 'Flatten', 'Dense', 'Dropout', 'Activation', 'Permute', 'RepeatVector', 'Lambda', 'ActivityRegularization', 'Masking', 'SpatialDropout1D', 'SpatialDropout2D', 'SpatialDropout3D', 'Input', 'Conv1D', 'Conv2D', 'SeparableConv1D', 'SeparableConv2D', 'DepthwiseConv2D', 'Conv2DTranspose', 'Conv3D', 'Conv3DTranspose', 'Cropping1D', 'Cropping2D', 'Cropping3D', 'UpSampling1D', 'UpSampling2D', 'UpSampling3D', 'ZeroPadding1D', 'ZeroPadding2D', 'ZeroPadding3D'], textvariable=self.outer_frame.model_frame_keras.layers[this_layer].layer_type_val, name='layer_type_selector')
                self.outer_frame.model_frame_keras.layers[this_layer].layer_type.bind("<<ComboboxSelected>>", self.keras_layer_handler)
                if this_layer == 0:
                    self.outer_frame.model_frame_keras.layers[this_layer].input_shape_label = ttk.Label(self.outer_frame.model_frame_keras.layers[this_layer], text="Input data shape (comma-separated dimensions): ", name='input_shape_label')
                    self.outer_frame.model_frame_keras.layers[this_layer].input_shape_val = tk.StringVar(value='18,273', name='model;layers;input_shape')
                    self.outer_frame.model_frame_keras.layers[this_layer].input_shape = ttk.Entry(self.outer_frame.model_frame_keras.layers[this_layer], textvariable=self.outer_frame.model_frame_keras.layers[this_layer].input_shape_val, name='input_shape_selector')
                    self.outer_frame.model_frame_keras.layers[this_layer].input_shape_label.pack()
                    self.outer_frame.model_frame_keras.layers[this_layer].input_shape.pack()
                self.outer_frame.model_frame_keras.layers[this_layer].pack()
                self.outer_frame.model_frame_keras.layers[this_layer].layer_type_label.pack()
                self.outer_frame.model_frame_keras.layers[this_layer].layer_type.pack()
        elif self.outer_frame.model_frame_keras.previous_model_layercount > self.outer_frame.model_frame_keras.model_layercount_val.get():
            for this_layer in range(self.outer_frame.model_frame_keras.model_layercount_val.get(), self.outer_frame.model_frame_keras.previous_model_layercount):
                self.outer_frame.model_frame_keras.layers[this_layer].pack_forget()
        self.outer_frame.model_frame_keras.previous_model_layercount = self.outer_frame.model_frame_keras.model_layercount_val.get()
        
    def keras_layer_handler(self,event=None,parent_layer=None):
        if event:
            parent_layer_name = event.widget.winfo_parent()
            parent_layer_frame = self.nametowidget(parent_layer_name)
        else:
            parent_layer_name = parent_layer
            parent_layer_frame = self.outer_frame.model_frame_keras.nametowidget(parent_layer_name)
        varname_stem = parent_layer_name.rsplit(';',1)[0]
        for old_contents in parent_layer_frame.winfo_children():
            if old_contents.winfo_name() not in ['layer_type_label','layer_type_selector','input_shape_label','input_shape_selector']:
                old_contents.pack_forget()
        if parent_layer_frame.layer_type_val.get() == 'Dense':
            parent_layer_frame.dense_unit_count_label = ttk.Label(parent_layer_frame, text="Number of units: ")
            parent_layer_frame.dense_unit_count_val = tk.IntVar(value=10, name=varname_stem+';units')
            parent_layer_frame.units = tk.Spinbox(parent_layer_frame, from_ = 0, to = 999999999, textvariable = parent_layer_frame.dense_unit_count_val)
            parent_layer_frame.kernel_reg_label= ttk.Label(parent_layer_frame, text="Kernel regularizer: ")
            parent_layer_frame.kernel_reg_val = tk.StringVar(value="keras.regularizers.l2(0.01)", name=varname_stem+';kernel_regularizer')
            parent_layer_frame.kernel_regularizer = tk.Entry(parent_layer_frame, textvariable = parent_layer_frame.kernel_reg_val)
            parent_layer_frame.dense_unit_count_label.pack()
            parent_layer_frame.units.pack()
            parent_layer_frame.kernel_reg_label.pack()
            parent_layer_frame.kernel_regularizer.pack()
        if parent_layer_frame.layer_type_val.get() == 'Reshape':
            parent_layer_frame.target_shape_label = ttk.Label(parent_layer_frame, text="Input target data shape (comma-separated dimensions): ")
            parent_layer_frame.target_shape_val = tk.StringVar(value='1,18,273', name=varname_stem+';target_shape')
            parent_layer_frame.target_shape = ttk.Entry(parent_layer_frame, textvariable=parent_layer_frame.target_shape_val)
            parent_layer_frame.target_shape_label.pack()
            parent_layer_frame.target_shape.pack()
        if parent_layer_frame.layer_type_val.get() == 'Conv2D':
            parent_layer_frame.conv2d_filter_count_label = ttk.Label(parent_layer_frame, text="Number of convolutional filters: ")
            parent_layer_frame.conv2d_filter_count_val = tk.IntVar(value=12, name=varname_stem+';filters')
            parent_layer_frame.filters = tk.Spinbox(parent_layer_frame, from_ = 0, to = 999999999, textvariable = parent_layer_frame.conv2d_filter_count_val)
            parent_layer_frame.conv2d_size_label = ttk.Label(parent_layer_frame, text="Kernel size (comma-separated dimensions): ")
            parent_layer_frame.conv2d_size_val = tk.StringVar(value='3,6', name=varname_stem+';kernel_size')
            parent_layer_frame.kernel_size = ttk.Entry(parent_layer_frame, textvariable=parent_layer_frame.conv2d_size_val)
            parent_layer_frame.conv2d_reg_label = ttk.Label(parent_layer_frame, text="Kernel regularizer: ")
            parent_layer_frame.conv2d_reg_val = tk.StringVar(value="keras.regularizers.l2(0.01)", name=varname_stem+';kernel_regularizer')
            parent_layer_frame.kernel_regularizer = tk.Entry(parent_layer_frame, textvariable = parent_layer_frame.conv2d_reg_val)
            parent_layer_frame.conv2d_padding_type_label = ttk.Label(parent_layer_frame, text="Padding strategy: ")
            parent_layer_frame.conv2d_padding_type_val = tk.StringVar(value='same', name=varname_stem+';padding')
            parent_layer_frame.padding = ttk.Combobox(parent_layer_frame, state='readonly', values=['same', 'casual', 'valid'], textvariable=parent_layer_frame.conv2d_padding_type_val)
            parent_layer_frame.conv2d_filter_count_label.pack()
            parent_layer_frame.filters.pack()
            parent_layer_frame.conv2d_size_label.pack()
            parent_layer_frame.kernel_size.pack()
            parent_layer_frame.conv2d_reg_label.pack()
            parent_layer_frame.kernel_regularizer.pack()
            parent_layer_frame.conv2d_padding_type_label.pack()
            parent_layer_frame.padding.pack()
        if parent_layer_frame.layer_type_val.get() == 'LeakyReLU':
            parent_layer_frame.leakyrelu_alpha_label = ttk.Label(parent_layer_frame, text="Alpha: ")
            parent_layer_frame.leakyrelu_alpha_val = tk.DoubleVar(value=0.1, name=varname_stem+';alpha')
            parent_layer_frame.alpha = tk.Spinbox(parent_layer_frame, from_ = 0, to = 1, increment = 0.1, textvariable = parent_layer_frame.leakyrelu_alpha_val)
            parent_layer_frame.leakyrelu_alpha_label.pack()
            parent_layer_frame.alpha.pack()
        if parent_layer_frame.layer_type_val.get() == 'BatchNormalization':
            pass
        if parent_layer_frame.layer_type_val.get() == 'Flatten':
            pass
        if parent_layer_frame.layer_type_val.get() == 'Activation':
            parent_layer_frame.activation_type_label = ttk.Label(parent_layer_frame, text="Activation function: ")
            parent_layer_frame.activation_type_val = tk.StringVar(value='linear', name=varname_stem+';activation')
            parent_layer_frame.activation = ttk.Combobox(parent_layer_frame, state='readonly', values=['softmax', 'tanh', 'elu', 'selu', 'softplus', 'softsign', 'relu', 'sigmoid', 'hard_sigmoid', 'exponential', 'linear'], textvariable=parent_layer_frame.activation_type_val)
            parent_layer_frame.activation_type_label.pack()
            parent_layer_frame.activation.pack()
        if parent_layer_frame.layer_type_val.get() == 'Dropout':
            parent_layer_frame.dropout_rate_label = ttk.Label(parent_layer_frame, text="Rate: ")
            parent_layer_frame.dropout_rate_val = tk.DoubleVar(value=0.3, name=varname_stem+';rate')
            parent_layer_frame.rate = tk.Spinbox(parent_layer_frame, from_ = 0, to = 1, increment = 0.1, textvariable = parent_layer_frame.dropout_rate_val)
            parent_layer_frame.dropout_rate_label.pack()
            parent_layer_frame.rate.pack()
        parent_layer_frame.additional_layer_params_label = ttk.Label(parent_layer_frame, text="Additional layer parameters (name,value format; one per line): ")                
        parent_layer_frame.additional_layer_params = tk.Text(parent_layer_frame, height=1)
        parent_layer_frame.additional_layer_params_label.pack()
        parent_layer_frame.additional_layer_params.pack()
                
    def create_data_frame(self):
        self.outer_frame.data_frame = ttk.LabelFrame(self.outer_frame, text="Data")
        self.outer_frame.data_frame.loader_label = ttk.Label(self.outer_frame.data_frame, text="Loader function name: ")
        self.outer_frame.data_frame.loader_val = tk.StringVar(name='data;loader')
        self.outer_frame.data_frame.loader_val.trace_add("write", self.update_job_struct)
        self.outer_frame.data_frame.loader_val.set('sample_generic_matfile_loader')
        self.outer_frame.data_frame.loader = ttk.Entry(self.outer_frame.data_frame, textvariable=self.outer_frame.data_frame.loader_val)
        self.outer_frame.data_frame.loader_params_label = ttk.Label(self.outer_frame.data_frame, text="Loader parameters (one per line): ")
        self.outer_frame.data_frame.loader_params = tk.Text(self.outer_frame.data_frame,height=3)
        self.outer_frame.data_frame.pack({"side":"top"})
        self.outer_frame.data_frame.loader_label.pack({"side":"left"})
        self.outer_frame.data_frame.loader.pack({"side":"left"})
        self.outer_frame.data_frame.loader_params_label.pack({"side":"top"})
        self.outer_frame.data_frame.loader_params.pack({"side":"right"})
            
    def create_analysis_frame(self):
        self.outer_frame.analysis_frame = ttk.LabelFrame(self.outer_frame, text="Analysis")
        self.outer_frame.analysis_frame.iterations_label = ttk.Label(self.outer_frame.analysis_frame, text="Number of iterations: ")
        self.outer_frame.analysis_frame.iterations_val = tk.IntVar(name='analysis;nits')
        self.outer_frame.analysis_frame.iterations_val.trace_add("write", self.update_job_struct)
        self.outer_frame.analysis_frame.iterations_val.set(20)
        self.outer_frame.analysis_frame.iterations = tk.Spinbox(self.outer_frame.analysis_frame, from_=0, to=999999999, textvariable=self.outer_frame.analysis_frame.iterations_val)
        self.outer_frame.analysis_frame.xval_type_label = ttk.Label(self.outer_frame.analysis_frame, text="Cross-validation type: ")
        self.outer_frame.analysis_frame.xval_type_val = tk.StringVar(name='analysis;xval_type')
        self.outer_frame.analysis_frame.xval_type_val.trace_add("write", self.update_job_struct)
        self.outer_frame.analysis_frame.xval_type_val.set('single')
        self.outer_frame.analysis_frame.xval_type = ttk.Combobox(self.outer_frame.analysis_frame, state='readonly', values=['single', 'loop_over_sa', 'transfer_over_sa'], textvariable=self.outer_frame.analysis_frame.xval_type_val)
        self.outer_frame.analysis_frame.classify_over_label = ttk.Label(self.outer_frame.analysis_frame, text="Variable to classify: ")
        self.outer_frame.analysis_frame.classify_over_val = tk.StringVar(name='analysis;classify_over')
        self.outer_frame.analysis_frame.classify_over_val.trace_add("write", self.update_job_struct)
        self.outer_frame.analysis_frame.classify_over_val.set('my_category_variable')
        self.outer_frame.analysis_frame.classify_over = tk.Entry(self.outer_frame.analysis_frame, textvariable=self.outer_frame.analysis_frame.classify_over_val)
        self.outer_frame.analysis_frame.trainprop_label = ttk.Label(self.outer_frame.analysis_frame, text="Percent of data to use for training: ")
        self.outer_frame.analysis_frame.trainprop_val = tk.IntVar(value=70, name='trainprop')
        self.outer_frame.analysis_frame.trainprop = tk.Spinbox(self.outer_frame.analysis_frame, from_=0, to=100, textvariable=self.outer_frame.analysis_frame.trainprop_val)
        self.outer_frame.analysis_frame.valprop_label = ttk.Label(self.outer_frame.analysis_frame, text="Percent of data to use for validation: ")
        self.outer_frame.analysis_frame.valprop_val = tk.IntVar(value=15, name='valprop')
        self.outer_frame.analysis_frame.valprop = tk.Spinbox(self.outer_frame.analysis_frame, from_=0, to=100, textvariable=self.outer_frame.analysis_frame.valprop_val)
        self.outer_frame.analysis_frame.testprop_label = ttk.Label(self.outer_frame.analysis_frame, text="Percent of data to use for testing: ")
        self.outer_frame.analysis_frame.testprop_val = tk.IntVar(value=15, name='testprop')
        self.outer_frame.analysis_frame.testprop = tk.Spinbox(self.outer_frame.analysis_frame, from_=0, to=100, textvariable=self.outer_frame.analysis_frame.testprop_val)
        self.outer_frame.analysis_frame.scaling_type_label = ttk.Label(self.outer_frame.analysis_frame, text="Data scaling type: ")
        self.outer_frame.analysis_frame.scaling_type_val = tk.StringVar(name='analysis;scaling_method')
        self.outer_frame.analysis_frame.scaling_type_val.trace_add("write", self.update_job_struct)
        self.outer_frame.analysis_frame.scaling_type_val.set('standardize')
        self.outer_frame.analysis_frame.scaling_type = ttk.Combobox(self.outer_frame.analysis_frame, state='readonly', values=['None', 'standardize', 'percentile', 'map_range', 'mean_center'], textvariable=self.outer_frame.analysis_frame.scaling_type_val)
        self.outer_frame.analysis_frame.scaling_type.bind("<<ComboboxSelected>>", self.scaling_type_handler)
        self.outer_frame.analysis_frame.pack({"side":"left"})
        self.outer_frame.analysis_frame.iterations_label.pack()
        self.outer_frame.analysis_frame.iterations.pack()
        self.outer_frame.analysis_frame.xval_type_label.pack()
        self.outer_frame.analysis_frame.xval_type.pack()
        self.outer_frame.analysis_frame.classify_over_label.pack()
        self.outer_frame.analysis_frame.classify_over.pack()
        self.outer_frame.analysis_frame.trainprop_label.pack()
        self.outer_frame.analysis_frame.trainprop.pack()
        self.outer_frame.analysis_frame.valprop_label.pack()
        self.outer_frame.analysis_frame.valprop.pack()
        self.outer_frame.analysis_frame.testprop_label.pack()
        self.outer_frame.analysis_frame.testprop.pack()
        self.outer_frame.analysis_frame.scaling_type_label.pack()
        self.outer_frame.analysis_frame.scaling_type.pack()
        
    def scaling_type_handler(self, event=None):
        if self.outer_frame.analysis_frame.scaling_type_val.get() == 'percentile':
            if hasattr(self.outer_frame.analysis_frame, 'scaling_minrange_val'):
                self.outer_frame.analysis_frame.scaling_minrange_label.pack_forget()
                self.outer_frame.analysis_frame.scaling_minrange.pack_forget()
                self.outer_frame.analysis_frame.scaling_maxrange_label.pack_forget()
                self.outer_frame.analysis_frame.scaling_maxrange.pack_forget()
            if not hasattr(self.outer_frame.analysis_frame, 'scaling_percentile_val'):
                self.outer_frame.analysis_frame.scaling_percentile_label = ttk.Label(self.outer_frame.analysis_frame, text="Percentile to use for rescaling: ")
                self.outer_frame.analysis_frame.scaling_percentile_val = tk.IntVar(name='analysis;scaling')
                self.outer_frame.analysis_frame.scaling_percentile_val.trace_add("write", self.update_job_struct)
                self.outer_frame.analysis_frame.scaling_percentile_val.set(90)
                self.outer_frame.analysis_frame.scaling_percentile = tk.Spinbox(self.outer_frame.analysis_frame, from_=0, to=100, textvariable=self.outer_frame.analysis_frame.scaling_percentile_val)
            self.outer_frame.analysis_frame.scaling_percentile_label.pack()
            self.outer_frame.analysis_frame.scaling_percentile.pack()
        elif self.outer_frame.analysis_frame.scaling_type_val.get() == 'map_range':
            if hasattr(self.outer_frame.analysis_frame, 'scaling_percentile_val'):
                self.outer_frame.analysis_frame.scaling_percentile_label.pack_forget()
                self.outer_frame.analysis_frame.scaling_percentile.pack_forget()
            if not hasattr(self.outer_frame.analysis_frame, 'scaling_minrange_val'):
                self.outer_frame.analysis_frame.scaling_minrange_label = ttk.Label(self.outer_frame.analysis_frame, text="Minimum value to use for rescaling: ")
                self.outer_frame.analysis_frame.scaling_minrange_val = tk.IntVar(value=0, name='scaling_min')
                self.outer_frame.analysis_frame.scaling_minrange = tk.Spinbox(self.outer_frame.analysis_frame, from_=-999999999, to=999999999, textvariable=self.outer_frame.analysis_frame.scaling_minrange_val)
                self.outer_frame.analysis_frame.scaling_maxrange_label = ttk.Label(self.outer_frame.analysis_frame, text="Maximum value to use for rescaling: ")
                self.outer_frame.analysis_frame.scaling_maxrange_val = tk.IntVar(value=1, name='scaling_max')
                self.outer_frame.analysis_frame.scaling_maxrange = tk.Spinbox(self.outer_frame.analysis_frame, from_=-999999999, to=999999999, textvariable=self.outer_frame.analysis_frame.scaling_maxrange_val)
            self.outer_frame.analysis_frame.scaling_minrange_label.pack()
            self.outer_frame.analysis_frame.scaling_minrange.pack()
            self.outer_frame.analysis_frame.scaling_maxrange_label.pack()
            self.outer_frame.analysis_frame.scaling_maxrange.pack()
        else:
            if hasattr(self.outer_frame.analysis_frame, 'scaling_minrange_val'):
                self.outer_frame.analysis_frame.scaling_minrange_label.pack_forget()
                self.outer_frame.analysis_frame.scaling_minrange.pack_forget()
                self.outer_frame.analysis_frame.scaling_maxrange_label.pack_forget()
                self.outer_frame.analysis_frame.scaling_maxrange.pack_forget()
            if hasattr(self.outer_frame.analysis_frame, 'scaling_percentile_val'):
                self.outer_frame.analysis_frame.scaling_percentile_label.pack_forget()
                self.outer_frame.analysis_frame.scaling_percentile.pack_forget()
        
    def create_output_frame(self):
        self.outer_frame.output_frame = ttk.LabelFrame(self.outer_frame, text="Output")
        self.outer_frame.output_frame.output_location_label = ttk.Label(self.outer_frame.output_frame, text="Path to output location: ")
        self.outer_frame.output_frame.output_location_val = tk.StringVar(name='output;output_location')
        self.outer_frame.output_frame.output_location_val.trace_add("write", self.update_job_struct)
        self.outer_frame.output_frame.output_location_val.set('output_files')
        self.outer_frame.output_frame.output_location = ttk.Entry(self.outer_frame.output_frame,textvariable=self.outer_frame.output_frame.output_location_val)
        self.outer_frame.output_frame.output_stem_label = ttk.Label(self.outer_frame.output_frame, text="Stem for output filenames: ")
        self.outer_frame.output_frame.output_stem_val = tk.StringVar(name='output;output_filename_stem')
        self.outer_frame.output_frame.output_stem_val.trace_add("write", self.update_job_struct)
        self.outer_frame.output_frame.output_stem_val.set('my_filename_stem')
        self.outer_frame.output_frame.output_stem = ttk.Entry(self.outer_frame.output_frame,textvariable=self.outer_frame.output_frame.output_stem_val)
        self.outer_frame.output_frame.output_types_selector_label = ttk.Label(self.outer_frame.output_frame, text="Output file types: ")
        self.outer_frame.output_frame.output_types_selector_val = tk.StringVar(name='output;output_file_types')
        self.outer_frame.output_frame.output_types_selector_val.trace_add("write", self.update_job_struct)
        self.outer_frame.output_frame.output_types_selector_val.set('All')
        self.outer_frame.output_frame.output_types_selector = ttk.Combobox(self.outer_frame.output_frame, state='readonly',values=['All','Select subset'],textvariable=self.outer_frame.output_frame.output_types_selector_val)
        self.outer_frame.output_frame.output_types_selector.bind("<<ComboboxSelected>>", self.create_output_types_frame)
        self.outer_frame.output_frame.pack({"side":"right"})
        self.outer_frame.output_frame.output_location_label.pack()
        self.outer_frame.output_frame.output_location.pack()
        self.outer_frame.output_frame.output_stem_label.pack()
        self.outer_frame.output_frame.output_stem.pack()
        self.outer_frame.output_frame.output_types_selector_label.pack()
        self.outer_frame.output_frame.output_types_selector.pack()
    
    def create_output_types_frame(self, event=None):
        if (not hasattr(self.outer_frame.output_frame,'output_types')) and (self.outer_frame.output_frame.output_types_selector_val.get() == 'Select subset'):
            self.outer_frame.output_frame.output_types = ttk.LabelFrame(self.outer_frame.output_frame, text="Select output file types: ")
            self.outer_frame.output_frame.output_types.acc_summary_val = tk.IntVar(name='output;output_file_types;acc_summary')
            self.outer_frame.output_frame.output_types.acc_summary = ttk.Checkbutton(self.outer_frame.output_frame.output_types,text="Test accuracy", variable=self.outer_frame.output_frame.output_types.acc_summary_val)
            self.outer_frame.output_frame.output_types.acc_summary_val.trace_add("write", self.update_job_struct)
            self.outer_frame.output_frame.output_types.acc_summary_val.set(0)
            self.outer_frame.output_frame.output_types.labels_val = tk.IntVar(name='output;output_file_types;labels')
            self.outer_frame.output_frame.output_types.labels = ttk.Checkbutton(self.outer_frame.output_frame.output_types,text="Labels", variable=self.outer_frame.output_frame.output_types.labels_val)
            self.outer_frame.output_frame.output_types.labels_val.trace_add("write", self.update_job_struct)
            self.outer_frame.output_frame.output_types.labels_val.set(0)
            self.outer_frame.output_frame.output_types.scores_val = tk.IntVar(name='output;output_file_types;scores')
            self.outer_frame.output_frame.output_types.scores = ttk.Checkbutton(self.outer_frame.output_frame.output_types,text="Test scores", variable=self.outer_frame.output_frame.output_types.scores_val)
            self.outer_frame.output_frame.output_types.scores_val.trace_add("write", self.update_job_struct)
            self.outer_frame.output_frame.output_types.scores_val.set(0)
            self.outer_frame.output_frame.output_types.job_config_val = tk.IntVar(name='output;output_file_types;job_config')
            self.outer_frame.output_frame.output_types.job_config = ttk.Checkbutton(self.outer_frame.output_frame.output_types,text="Job configuration", variable=self.outer_frame.output_frame.output_types.job_config_val)
            self.outer_frame.output_frame.output_types.job_config_val.trace_add("write", self.update_job_struct)
            self.outer_frame.output_frame.output_types.job_config_val.set(0)
            self.outer_frame.output_frame.output_types.timestamps_val = tk.IntVar(name='output;output_file_types;timestamps')
            self.outer_frame.output_frame.output_types.timestamps = ttk.Checkbutton(self.outer_frame.output_frame.output_types,text="Timestamps", variable=self.outer_frame.output_frame.output_types.timestamps_val)
            self.outer_frame.output_frame.output_types.timestamps_val.trace_add("write", self.update_job_struct)
            self.outer_frame.output_frame.output_types.timestamps_val.set(0)
            self.outer_frame.output_frame.output_types.metadata_val = tk.IntVar(name='output;output_file_types;metadata')
            self.outer_frame.output_frame.output_types.metadata = ttk.Checkbutton(self.outer_frame.output_frame.output_types,text="Metadata", variable=self.outer_frame.output_frame.output_types.metadata_val)
            self.outer_frame.output_frame.output_types.metadata_val.trace_add("write", self.update_job_struct)
            self.outer_frame.output_frame.output_types.metadata_val.set(0)
            self.outer_frame.output_frame.output_types.trained_model_val = tk.IntVar(name='output;output_file_types;trained_model')
            self.outer_frame.output_frame.output_types.trained_model = ttk.Checkbutton(self.outer_frame.output_frame.output_types,text="Trained model", variable=self.outer_frame.output_frame.output_types.trained_model_val)
            self.outer_frame.output_frame.output_types.trained_model_val.trace_add("write", self.update_job_struct)
            self.outer_frame.output_frame.output_types.trained_model_val.set(0)
            self.outer_frame.output_frame.output_types.training_acc_val = tk.IntVar(name='output;output_file_types;training_acc')
            self.outer_frame.output_frame.output_types.training_acc = ttk.Checkbutton(self.outer_frame.output_frame.output_types,text="Training accuracy", variable=self.outer_frame.output_frame.output_types.training_acc_val)
            self.outer_frame.output_frame.output_types.training_acc_val.trace_add("write", self.update_job_struct)
            self.outer_frame.output_frame.output_types.training_acc_val.set(0)
            self.outer_frame.output_frame.output_types.validation_acc_val = tk.IntVar(name='output;output_file_types;validation_acc')
            self.outer_frame.output_frame.output_types.validation_acc = ttk.Checkbutton(self.outer_frame.output_frame.output_types,text="Validation accuracy", variable=self.outer_frame.output_frame.output_types.validation_acc_val)
            self.outer_frame.output_frame.output_types.validation_acc_val.trace_add("write", self.update_job_struct)
            self.outer_frame.output_frame.output_types.validation_acc_val.set(0)
            self.outer_frame.output_frame.output_types.pack({"side":"bottom"})
            self.outer_frame.output_frame.output_types.acc_summary.pack()
            self.outer_frame.output_frame.output_types.labels.pack()
            self.outer_frame.output_frame.output_types.scores.pack()
            self.outer_frame.output_frame.output_types.job_config.pack()
            self.outer_frame.output_frame.output_types.timestamps.pack()
            self.outer_frame.output_frame.output_types.metadata.pack()
            self.outer_frame.output_frame.output_types.trained_model.pack()
            self.outer_frame.output_frame.output_types.training_acc.pack()
            self.outer_frame.output_frame.output_types.validation_acc.pack()
        elif (hasattr(self.outer_frame.output_frame,'output_types')) and (self.outer_frame.output_frame.output_types_selector_val.get() == 'All'):
            self.outer_frame.output_frame.output_types.pack_forget()
        elif (hasattr(self.outer_frame.output_frame,'output_types')) and (self.outer_frame.output_frame.output_types_selector_val.get() == 'Select subset'):
            self.outer_frame.output_frame.output_types.pack({"side":"bottom"})
            
    def load_json(self):
        json_file_name = filedialog.askopenfilename()
        if json_file_name == '':
            print('Load canceled')
            return
        json_file = open(json_file_name)
        new_job_struct_text = json_file.read()
        json_file.close()
        new_job_struct = json.loads( new_job_struct_text )[0]
        self.job_struct = {'data':{'loader':None,'loader_params':None}, 'analysis':{'nits':None,'xval_type':None,'train_val_test':None,'scaling':None,'scaling_type':None,'classify_over':None,'backend_options':{}}, 'output':{'output_location':None,'output_filename_stem':None,'output_file_types':{}}, 'model':{'compile_options':{},'layers':None,'backend':None, 'classifier':{'classifier_type':None,'svm_impl':None,'implementation':None,'params':{}}}}
        self.job_struct.update(new_job_struct)
        
        #propagate job_struct back to tkinter variables, create any missing widgets on the way
        #data
        self.outer_frame.data_frame.loader_val.set(self.job_struct['data']['loader'])
        self.outer_frame.data_frame.loader_params.delete(1.0,'end')
        loader_param_string = '\n'.join(self.job_struct['data']['loader_params'])
        self.outer_frame.data_frame.loader_params.insert(1.0,loader_param_string)
        
        #output
        self.outer_frame.output_frame.output_location_val.set(self.job_struct['output']['output_location'])
        self.outer_frame.output_frame.output_stem_val.set(self.job_struct['output']['output_filename_stem'])
        if self.job_struct['output']['output_file_types'] in ['All', 'all']:
            self.outer_frame.output_frame.output_types_selector_val.set('All')
        elif self.job_struct['output']['output_file_types']:
            output_type_list = self.job_struct['output']['output_file_types']
            self.outer_frame.output_frame.output_types_selector_val.set('Select subset')
            self.create_output_types_frame()
            for item in output_type_list:
                this_output_varname = 'output;output_file_types;'+item
                self.setvar(name=this_output_varname, value = 1)
                
        #analysis
        percentile_value = None
        for item in self.job_struct['analysis']:
            if item=='scaling':
                if 'scaling_method' in self.job_struct['analysis']:
                    if self.job_struct['analysis']['scaling_method'] == 'map_range':
                        map_minrange = self.job_struct['analysis'][item][0]
                        map_maxrange = self.job_struct['analysis'][item][1]
                else:
                    percentile_value = self.job_struct['analysis'][item]
            elif item=='train_val_test':
                trainprop = self.job_struct['analysis'][item][0]
                valprop = self.job_struct['analysis'][item][1]
                testprop = self.job_struct['analysis'][item][2]
            else:
                this_analysis_varname = 'analysis;'+item
                self.setvar(name=this_analysis_varname, value = self.job_struct['analysis'][item])
        self.scaling_type_handler()
        if 'train_val_test' in self.job_struct['analysis']:
            self.outer_frame.analysis_frame.trainprop_val.set(trainprop)
            self.outer_frame.analysis_frame.valprop_val.set(valprop)
            self.outer_frame.analysis_frame.testprop_val.set(testprop)
        if ('scaling_method' not in self.job_struct['analysis']) & percentile_value:
            self.outer_frame.analysis_frame.scaling_type_val.set('percentile')
        if self.outer_frame.analysis_frame.scaling_type_val.get() == 'percentile':
            self.outer_frame.analysis_frame.scaling_percentile_val.set(percentile_value)
        elif self.outer_frame.analysis_frame.scaling_type_val.get() == 'map_range':
            self.outer_frame.analysis_frame.scaling_minrange_val.set(map_minrange)
            self.outer_frame.analysis_frame.scaling_maxrange_val.set(map_maxrange)
            
        #model - should probably abstract this looping into another function at some point, but for now want to be prepared for special cases
        #for item in self.job_struct['model']:
        #    if type(self.job_struct['model'][item]) == 'dict':
        #        for inner_item_1 in self.job_struct['model'][item]:
        #            if type(self.job_struct['model'][item][inner_item_1]) == 'dict':
        #            
        #    else: 
        #        this_model_varname = 'model;'+item
        #        self.setvar(name=this_model_varname, value = self.job_struct['model'][item])
        if 'backend' not in self.job_struct['model']:
            self.outer_frame.backend_val.set('Keras')
        else:
            self.outer_frame.backend_val.set(self.job_struct['model']['backend'])
        self.create_model_frame()
        if self.outer_frame.backend_val.get().lower() == 'pymvpa':
            self.outer_frame.model_frame_pymvpa.model_type_val.set(self.job_struct['model']['classifier']['classifier_type'])
            loaded_c = None
            if ('C' in self.job_struct['model']['classifier']['params']):
                loaded_c = self.job_struct['model']['classifier']['params']['C']
            self.pymvpa_model_handler()
            if loaded_c:
                self.outer_frame.model_frame_pymvpa.svm_c_val.set(loaded_c)
        self.dictionary_loop(dictionary = self.job_struct['model'], varname_stem = 'model;')
    
    def dictionary_loop(self, dictionary, varname_stem): 
        for item in dictionary:
            if item == 'backend':
                pass
            elif item == 'classifier_type':
                pass
            elif item == 'C':
                pass
#            everything in here should have its own special case handling down to kernel_params, but maybe revisit a generalized version after reviewing pymvpa docs
#            elif item == 'params':
#                if self.outer_frame.model_frame_pymvpa.model_type_val.get().lower() == 'svm':
#                    classifier_param_list = ''
#                    for this_param in dictionary[item]:
#                        this_param_string = this_param+','+str(dictionary[item][this_param])+'\n'
#                        classifier_param_list = classifier_param_list+this_param_string
#                    self.outer_frame.model_frame_pymvpa.svm_kernel_params.insert("1.0",classifier_param_list)
#                elif self.outer_frame.model_frame_pymvpa.model_type_val.get().lower() == 'smlr':
#                    self.outer_frame.model_frame_pymvpa.smlr_kernel_params
            elif item == 'kernel_params':
                kernel_param_list = ''
                for this_param in dictionary[item]:
                    this_param_string = this_param+','+str(dictionary[item][this_param])+'\n'
                    kernel_param_list = kernel_param_list+this_param_string
                self.outer_frame.model_frame_pymvpa.svm_kernel_params.insert("1.0",kernel_param_list)
            elif item == 'optimizer_options':
                optimizer_param_list = ''
                for this_param in dictionary[item]:
                    this_param_string = this_param+','+str(dictionary[item][this_param])+'\n'
                    optimizer_param_list = optimizer_param_list+this_param_string
                self.outer_frame.model_frame_keras.compile_options_frame.optimizer_opts.insert("1.0",optimizer_param_list)
            elif item == 'layers':
                self.outer_frame.model_frame_keras.model_layercount_val.set(len(dictionary[item]))
                for this_layer in range(len(dictionary[item])):
                    self.setvar(name='model;layers;layer_'+str(this_layer)+';layer_type', value=dictionary[item][this_layer]['layer_type'])
                    parent_layer_name = 'model;layers;layer_'+str(this_layer)+';layer_frame'
                    self.keras_layer_handler(parent_layer = parent_layer_name)
                    additional_param_list = ''
                    for this_layer_param in dictionary[item][this_layer]:
                        if this_layer_param == 'layer_type':
                            pass
                        else:
                            if hasattr(self.outer_frame.model_frame_keras.nametowidget(parent_layer_name),this_layer_param):
                                self.setvar(name='model;layers;layer_'+str(this_layer)+';'+this_layer_param, value = dictionary[item][this_layer][this_layer_param])
                            elif hasattr(self.outer_frame.model_frame_keras.nametowidget(parent_layer_name),'additional_layer_params'):
                                this_param_string = this_layer_param+','+str(dictionary[item][this_layer][this_layer_param])+'\n'
                                additional_param_list = additional_param_list+this_param_string
                    if hasattr(self.outer_frame.model_frame_keras.nametowidget(parent_layer_name),'additional_layer_params'):
                        self.outer_frame.model_frame_keras.nametowidget(parent_layer_name).additional_layer_params.insert("1.0",additional_param_list)
            else:
                if isinstance(dictionary[item],dict):
                    self.dictionary_loop(dictionary[item], varname_stem+item+';')
                elif isinstance(dictionary[item],list):
                    for list_item in dictionary[item]:
                        if isinstance(dictionary[item][list_item],dict):
                            self.dictionary_loop(dictionary[item][list_item], varname_stem+item+';'+list_item+';')
                else:
                    this_varname = varname_stem+item
                    #debugging check; keping for future use with verbosity settings
                    #print('updating variable: '+this_varname+' to value: '+str(dictionary[item]))
                    self.setvar(name=this_varname, value = dictionary[item])
        
    def save_json(self):
        #update internal job_struct with untraced values
        if self.outer_frame.analysis_frame.scaling_type_val.get() == 'map_range':
            self.job_struct['analysis']['scaling'] = [self.outer_frame.analysis_frame.scaling_minrange_val.get(), self.outer_frame.analysis_frame.scaling_maxrange_val.get()]
        self.job_struct['analysis']['train_val_test'] = [self.outer_frame.analysis_frame.trainprop_val.get(), self.outer_frame.analysis_frame.valprop_val.get(), self.outer_frame.analysis_frame.testprop_val.get()]
        
        loader_param_list = self.outer_frame.data_frame.loader_params.get("1.0",'end-1c').split('\n')
        self.job_struct['data']['loader_params'] = []
        for this_param in loader_param_list:
            self.job_struct['data']['loader_params'].append(this_param)
            
        #may need to do additional float conversion for some cases
        if self.outer_frame.backend_val.get() == 'Keras':
            self.job_struct['model']['classifier'] = None
            
            optimizer_opt_list = self.outer_frame.model_frame_keras.compile_options_frame.optimizer_opts.get("1.0",'end-1c').split('\n')
            if optimizer_opt_list != ['']:
                for opt_pair in optimizer_opt_list:
                    this_key,this_val = opt_pair.split(',')
                    self.job_struct['model']['compile_options'][this_key] = this_val
                
            self.job_struct['model']['layers'] = []
            for this_layer_num in range(self.outer_frame.model_frame_keras.model_layercount_val.get()):
                this_layer_type = self.outer_frame.model_frame_keras.layers[this_layer_num].layer_type_val.get()
                if this_layer_type == 'Reshape':
                    target_shape_str = self.outer_frame.model_frame_keras.layers[this_layer_num].target_shape_val.get().split(',')
                    target_shape_list = []
                    for this_target_dim in range(len(target_shape_str)):
                        target_shape_list.append(int(target_shape_str[this_target_dim]))
                    self.job_struct['model']['layers'].append({'layer_type':this_layer_type, 'target_shape':target_shape_list})
                if this_layer_type == 'Dense':
                    self.job_struct['model']['layers'].append({'layer_type':this_layer_type, 'units':self.outer_frame.model_frame_keras.layers[this_layer_num].dense_unit_count_val.get(), 'kernel_regularizer':self.outer_frame.model_frame_keras.layers[this_layer_num].kernel_reg_val.get()})
                if this_layer_type == 'Conv2D':
                    kernel_size_str = self.outer_frame.model_frame_keras.layers[this_layer_num].conv2d_size_val.get().split(',')
                    kernel_size_list = []
                    for this_kernel_dim in range(len(kernel_size_str)):
                        kernel_size_list.append(int(kernel_size_str[this_kernel_dim]))
                    self.job_struct['model']['layers'].append({'layer_type':this_layer_type, 'padding':self.outer_frame.model_frame_keras.layers[this_layer_num].conv2d_padding_type_val.get(), 'filters':self.outer_frame.model_frame_keras.layers[this_layer_num].conv2d_filter_count_val.get(), 'kernel_size':kernel_size_list, 'kernel_regularizer':self.outer_frame.model_frame_keras.layers[this_layer_num].conv2d_reg_val.get()})
                if this_layer_type == 'Flatten':
                    self.job_struct['model']['layers'].append({'layer_type':this_layer_type})
                if this_layer_type == 'BatchNormalization':
                    self.job_struct['model']['layers'].append({'layer_type':this_layer_type})
                if this_layer_type == 'LeakyReLu':
                    self.job_struct['model']['layers'].append({'layer_type':this_layer_type, 'alpha':self.outer_frame.model_frame_keras.layers[this_layer_num].leakyrelu_alpha_val.get()})
                if this_layer_type == 'Dropout':
                    self.job_struct['model']['layers'].append({'layer_type':this_layer_type, 'rate':self.outer_frame.model_frame_keras.layers[this_layer_num].dropout_rate_val.get()})
                if this_layer_type == 'Activation':
                    self.job_struct['model']['layers'].append({'layer_type':this_layer_type, 'activation':self.outer_frame.model_frame_keras.layers[this_layer_num].activation_type_val.get()})
                if hasattr(self.outer_frame.model_frame_keras.layers[this_layer_num],'additional_layer_params'):
                    this_layer_dict = {'layer_type':this_layer_type}
                    layer_param_list = self.outer_frame.model_frame_keras.layers[this_layer_num].additional_layer_params.get("1.0",'end-1c').split('\n')
                    for this_layer_param in layer_param_list:
                        this_param_key, this_param_value = this_layer_param.split(',')
                        try:
                            this_param_value = float(this_param_value)
                        except ValueError:
                            pass
                        this_layer_dict[this_param_key] = this_param_value
                    self.job_struct['model']['layers'].append(this_layer_dict)
                if this_layer_num ==0:
                    input_shape_str = self.outer_frame.model_frame_keras.layers[this_layer_num].input_shape_val.get().split(',')
                    input_shape_list = []
                    for this_input_dim in range(len(input_shape_str)):
                        input_shape_list.append(int(input_shape_str[this_input_dim]))
                    self.job_struct['model']['layers'][this_layer_num]['input_shape'] = input_shape_list
                    
                
        elif self.outer_frame.backend_val.get() == 'pymvpa':
            self.job_struct['model']['compile_options'] = None
            self.job_struct['model']['layers'] = None
            
            if self.outer_frame.model_frame_pymvpa.model_type_val.get() == 'SVM':
                svm_param_list = self.outer_frame.model_frame_pymvpa.svm_classifier_params.get("1.0",'end-1c').split('\n')
                for param_pair in svm_param_list:
                    if len(param_pair):
                        this_key,this_val = param_pair.split(',')
                        try:
                            self.job_struct['model']['classifier']['params'][this_key] = float(this_val)
                        except ValueError:
                            self.job_struct['model']['classifier']['params'][this_key] = this_val
                svm_kernel_param_list = self.outer_frame.model_frame_pymvpa.svm_kernel_params.get("1.0",'end-1c').split('\n')
                for param_pair in svm_kernel_param_list:
                    if len(param_pair):
                        this_key,this_val = param_pair.split(',')
                        try:
                            self.job_struct['model']['classifier']['params']['kernel_params'][this_key] = float(this_val)
                        except ValueError:
                            self.job_struct['model']['classifier']['params']['kernel_params'][this_key] = this_val
            elif self.outer_frame.model_frame_pymvpa.model_type_val.get() == 'SMLR':
                smlr_param_list = self.outer_frame.model_frame_pymvpa.smlr_classifier_params.get("1.0",'end-1c').split('\n')
                for param_pair in smlr_param_list:
                    if len(param_pair):
                        this_key,this_val = param_pair.split(',')
                        try:
                            self.job_struct['model']['classifier']['params'][this_key] = float(this_val)
                        except ValueError:
                            self.job_struct['model']['classifier']['params'][this_key] = this_val
        
        
        out_str = json.dumps(self.job_struct,sort_keys=True,indent=3)
        out_fname = filedialog.asksaveasfilename()
        if out_fname == '':
            print('Save canceled')
            return
        out_file = open(out_fname,'w')
        out_file.write(out_str)
        out_file.close()
        
    def update_job_struct(self,*args):
        updated_var_name = args[0].split(';')
        try: 
            updated_var_val = float(self.tk.globalgetvar(args[0]))
        except ValueError:
            updated_var_val = self.tk.globalgetvar(args[0])
        #special-case handling first 
        #output file type subsets; initial checks prevents tkinter errors on initiialization
        if hasattr(self.outer_frame,'output_frame'):
            if hasattr(self.outer_frame.output_frame,'output_types_selector_val'):
                if (self.outer_frame.output_frame.output_types_selector_val.get()=='Select subset') & (updated_var_name[1]=='output_file_types') & (len(updated_var_name)==3):
                    if type(self.job_struct[updated_var_name[0]][updated_var_name[1]]) is not list:
                        self.job_struct[updated_var_name[0]][updated_var_name[1]] = []
                    if self.tk.globalgetvar(args[0]) == 1:
                        self.job_struct[updated_var_name[0]][updated_var_name[1]].append(updated_var_name[2])
                    elif (self.tk.globalgetvar(args[0]) == 0) & (updated_var_name[2] in self.job_struct[updated_var_name[0]][updated_var_name[1]]):
                        self.job_struct[updated_var_name[0]][updated_var_name[1]].remove(updated_var_name[2])
                    return
            
        #general case
        #KK note: I don't love this, but I don't believe anything gets deeper than 4 so it shouldn't be too too unwieldy - just have to be careful to create each nested dict at the right time so this never attempts to skip a level
        if len(updated_var_name) == 2:
            self.job_struct[updated_var_name[0]][updated_var_name[1]] = updated_var_val
        elif len(updated_var_name) == 3:
            self.job_struct[updated_var_name[0]][updated_var_name[1]][updated_var_name[2]] = updated_var_val
        elif len(updated_var_name) == 4:
            self.job_struct[updated_var_name[0]][updated_var_name[1]][updated_var_name[2]][updated_var_name[3]] = updated_var_val
        
        

#execution
root = tk.Tk()
gui = DTGui(master=root)
gui.mainloop()
root.destroy()