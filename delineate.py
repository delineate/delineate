import sys
import DTJob

sys.setrecursionlimit(10000)
# gets around a Theano issue with hitting the recursion limit
# - solution found online originally set it to 50000 but that is absurdly high
# - yet the default of 1000 is clearly too low
# - so, compromising for now on 10000
# - not an ideal solution and we may want to make this dynamic or user-adjustable in the future
# - just putting in delineate.py for now but may want to relocate elsewhere someday? TK

n_args = len(sys.argv)
if n_args <= 1:
    print("No job file specified! This script (delineate.py) requires at least one command-line argument.")
    sys.exit()

for i in range(1, n_args):
    job_file = sys.argv[i]

    # TK: insert appropriate error checking here
    print('delineate.py: running json file number {0} of {1}'.format(i, n_args-1))
    my_job = DTJob.DTJob(job_file)
    my_job.run()

