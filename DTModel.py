import copy, os, sys
import DTData
from support_modules import DTError

class DTModel:
    
    
    def __init__(self, model_func=None, model_args=None, layers=None, backend='keras', build_now=False, compile_options=None, classifier=None, backend_options=None, job_file_hash = None, remove_checkpoint_weights = True):
        if backend==None:
            backend = 'keras'
        
        if backend=='keras':
            global keras
            # We think we have to do it this way for Python 2 verus Python 3 but frankly we're not sure...
            #   It might be possible that the Python 2 solution works in Python 3 too? But we don't have 
            #   Keras set up in Python 2 on our systems anyway... so hard to test right now
            if (sys.version_info > (3, 0)): #Py3
                keras = __import__('keras', globals(), locals())
            else: #Py2
                import keras

            if model_func==None and layers==None:
                DTError.err("DTModel: For 'keras' backend, either 'model_func' or 'layers' argument must be provided, but neither was.")
                            
            if model_func!=None and layers!=None:
                DTError.err("DTModel: For 'keras' backend, one of 'model_func' or 'layers' argument must be provided, but not both!")
        
        elif backend=='pymvpa':
            if classifier==None:
                DTError.err("DTModel: For 'pymvpa' backend, a classifier must be specified.")
        
        else:
            DTError.err("DTModel: Invalid backend specified. At the moment the only supported options are 'keras' and 'pymvpa'.")
    
        self.model_func = model_func
        self.model_args = model_args
        self.layers = layers
        self.backend = backend
        self.model = None
        self.compile_options = compile_options
        self.classifier = classifier
        self.backend_options = backend_options
        self.job_file_hash = job_file_hash
        self.remove_checkpoint_weights = remove_checkpoint_weights
        
        if build_now:
            self.build()
    
    
    def build(self):
        if self.backend=='pymvpa':
            #everything is terrible
            temp_dict = copy.deepcopy( self.classifier )
            classifier_type = temp_dict.pop('classifier_type')
            
            #TK: since classifiers appear to have parameters that function similarly but are named differently
            #    (e.g. 'svm_impl' for SVM vs 'implementation' for SMLR), it might be nice to have more generalized
            #    toolbox parameters that can be intelligently interpreted into the right PyMVPA params
            if classifier_type == 'SVM':
                from mvpa2.clfs.svm import SVM
                svm_params = temp_dict.pop('params')

                if 'kernel' in svm_params:
                    svm_params['kernel']=self.get_SVM_kernel(svm_params)

                self.model = SVM( **temp_dict )
                for param_key in svm_params.keys():
                    self.model.params[param_key].value = svm_params[param_key]
                    
            elif classifier_type == 'SMLR':
                from mvpa2.clfs.smlr import SMLR
                smlr_params = temp_dict.pop('params')
                self.model = SMLR( **temp_dict )
                for param_key in smlr_params.keys():
                    self.model.params[param_key].value = smlr_params[param_key]
            else:
                DTError.err("DTModel: Invalid or not yet supported classifier specified for 'pymvpa' backend.")
            return

        if self.model_func==None and self.layers==None:
            my_message = ["DTModel: Trying to build() model with 'keras' backend, but neither 'model_func' nor 'layers' attributes ",
                          "         appear to exist."]
            DTError.err( my_message )
        
        if self.model_func!=None and self.layers!=None:
            my_message = ["DTModel: Trying to build() model with 'keras' backend, but both 'model_func' and 'layers' attributes ",
                          "         appear to exist. Unclear which model we are supposed to build!"]
            DTError.err( my_message )
        
        if self.layers!=None:
            # just a stubby implementation for now. Should really do something more sophisticated that tries the various 
            #     different Keras layers modules looking for a fit
            
            try:
                self.model = keras.models.Sequential()
                
                for this_layer in self.layers:
                    temp_dict = copy.deepcopy( this_layer )
                    layer_type = temp_dict.pop('layer_type')
                    
                    # MRJ: I think 'wreg' is Keras 1 specific (KK update: it is)
                    #   for now, marking this as "legacy Keras" but leaving it in
                    #   (it shouldn't break anything in Keras 2 if user doesn't try to mix/match Keras 1 and 2 options)
                    if 'wreg' in temp_dict:
                        reg_params = temp_dict.pop('wreg')
                        temp_dict['W_regularizer'] = keras.regularizers.WeightRegularizer( l1=reg_params[0], l2=reg_params[1] )
                    
                    # MRJ: I think kernel, bias, and activity regularizers might be Keras 2-only stuff
                    if 'kernel_regularizer' in temp_dict:
                        temp_dict['kernel_regularizer'] = eval(temp_dict.pop('kernel_regularizer'))

                    if 'bias_regularizer' in temp_dict:
                        temp_dict['bias_regularizer'] = eval(temp_dict.pop('bias_regularizer'))

                    if 'activity_regularizer' in temp_dict:
                         temp_dict['activity_regularizer'] = eval(temp_dict.pop('activity_regularizer'))
                    
                    #KK: This makes me nervous, but for now all keras layers are uniquely named so there is no present (3/21/2018) risk of false-positives
                    layer_func = None
                    for layer_module in (keras.layers.core, keras.layers.convolutional, keras.layers.recurrent, keras.layers.normalization, keras.layers.advanced_activations, keras.regularizers, keras.layers.local):
                        try:
                            layer_func = getattr( layer_module, layer_type )
                            break
                        except:
                            pass
                    
                    if layer_func is None:
                        raise
                    self.model.add( layer_func( **temp_dict ) )
                
                self.compile_options['metrics'] = ['acc']
                keras_opts = self.keras_options()
                if keras_opts['verbose'] > 0:
                    self.model.summary()

                if 'optimizer_options' in self.compile_options:
                    temp_compile_options = copy.deepcopy( self.compile_options )
                    optopts = temp_compile_options.pop('optimizer_options')
                    temp_compile_options['optimizer'] = getattr( keras.optimizers, self.compile_options['optimizer'])(**optopts)
                    self.model.compile(**temp_compile_options)

                else:
                    self.model.compile( **self.compile_options )
            except:
                my_message = [  "DTModel: Trying to build() model with 'keras' backend using 'layers' attribute, but the 'layers' ",
                                "         dictionary appears to be formatted improperly in some way, or perhaps includes a layer ",
                                "         type that is not currently supported."]
                DTError.err( my_message )
        else:
            self.model = self.model_func(**self.model_args)
    
    
    def train( self, train_data=None, val_data=None, train_labels=None, val_labels=None ):
        if train_data is None or train_labels is None:
            DTError.err("Need training data/labels to call DTModel train()!")
        
        if self.backend == 'keras':
            self.train_keras( train_data, val_data, train_labels, val_labels )
        elif self.backend == 'pymvpa':
            if val_data or val_labels:
                DTError.err("No support for validation data in PyMVPA backend!")
            self.train_pymvpa( train_data, train_labels )
        else:
            DTError.err("DTModel: Invalid backend specified. At the moment the only supported options are 'keras' and 'pymvpa'.")
    
    
    def train_keras(self, train_data=None, val_data=None, train_labels=None, val_labels=None ):
        if train_data is None or train_labels is None:
            DTError.err("Need training data/labels to call DTModel train_keras()!")
        
        # the following could get taken out someday if we ever implement analyses that don't need validation data
        if val_data is None or val_labels is None:
            DTError.err("Need validation data/labels to call DTModel train_keras()!")
        
        if self.model is None: #can check more conditions later e.g. needs_rebuilding or whatnot, but this should suffice for now
            self.build()
        
        keras_opts = self.keras_options()
        early_stopping = keras.callbacks.EarlyStopping( monitor=keras_opts['monitor'], patience=keras_opts['patience'] )
        mcp = keras.callbacks.ModelCheckpoint( keras_opts['checkpoint_filename'], monitor=keras_opts['monitor'], verbose=0, save_best_only=True, mode='auto' )
        
        if keras.__version__ < '2.0': # MRJ: This seems SUPER-gross but seems to work
            # marking this as "legacy Keras" for future taking-out
            self.training_history_raw = self.model.fit( train_data, train_labels, validation_data=(val_data, val_labels), batch_size=keras_opts['batch_size'], nb_epoch=keras_opts['epochs'], 
                            verbose=keras_opts['verbose'], callbacks=[early_stopping, mcp] )
            self.training_history = self.training_history_raw.history
        else:
            #Putting in here since this won't work with previous version of Keras
            lr_reduction = keras.callbacks.ReduceLROnPlateau(monitor=keras_opts['monitor'], factor=keras_opts['reduction_factor'], patience=keras_opts['reduction_patience'], verbose=keras_opts['verbose'])
            self.training_history_raw = self.model.fit( train_data, train_labels, validation_data=(val_data, val_labels), batch_size=keras_opts['batch_size'], epochs=keras_opts['epochs'], 
                            verbose=keras_opts['verbose'], callbacks=[early_stopping, mcp, lr_reduction] )
            self.training_history = self.training_history_raw.history
        self.model.load_weights(keras_opts['checkpoint_filename'])
        
        if self.remove_checkpoint_weights:
            try: 
                if os.path.isfile(keras_opts['checkpoint_filename']):
                    # in theory the file shouldn't go missing and we shouldn't have to check for it... but if somehow it DOES go missing, probably not worth 
                    #   throwing a warning/error over that
                    os.remove(keras_opts['checkpoint_filename'])
            except:
                DTError.warn("DTModel: Problem removing checkpoint weights file. You may have to clean it up by hand if its existence offends you.")
    
    
    def train_pymvpa(self, train_data=None, train_labels=None ):
        # MRJ note on below: copying these couple of lines from train_keras() for parity; not likely
        #   we'll run into this situation much, but just in case... couldn't hoit!
        if self.model is None: #can check more conditions later e.g. needs_rebuilding or whatnot, but this should suffice for now
            self.build()
        
        train_data = DTData.DTData.pymvpa_wrap( train_data, train_labels )
        self.model.train( train_data )
        binary_preds = self.model.predict(train_data)
        train_acc = (binary_preds==train_labels).astype(int)
        train_acc = sum(train_acc) / float(len(train_acc))
        temp_prediction_scores = self.model.ca['estimates']
        train_prediction_scores = []
        for x in temp_prediction_scores.value:
            if self.classifier['classifier_type'] == 'SMLR':
                train_prediction_scores.append( x )
            elif self.classifier['classifier_type'] == 'SVM':
                train_prediction_scores.append( [x] )
            else:
                DTError.err("DTModel: Unknown classifier type")
        self.training_history = {'acc':train_acc,'scores':train_prediction_scores, 'loss': None, 'val_acc': None, 'val_loss': None}
    
    def keras_options(self):
        if self.backend_options is None:
            self.backend_options = {}
        
        opts = {}
        opts['monitor'] = 'val_loss'
        opts['patience'] = 50
        opts['reduction_patience'] = 10
        opts['reduction_factor'] = .5
        if self.job_file_hash is None: # probably unlikely but weird API usage could potentially lead to this?
            opts['checkpoint_filename'] = 'delineate_checkpoint_weights_tempfile.h5'
        else:
            opts['checkpoint_filename'] = 'delineate_checkpoint_weights_tempfile_' + self.job_file_hash + '.h5'
        opts['batch_size'] = 500
        opts['epochs'] = 10000
        opts['verbose'] = 1
        
        for key in self.backend_options.keys():
            opts[key] = self.backend_options[key]
        
        return opts
    
    
    def test( self, test_data, test_labels ):
        if test_data is None or test_labels is None:
            DTError.err("Need test data/labels to call DTModel test()!")
        
        if self.backend == 'keras':
            test_acc, test_loss, test_prediction_scores = self.test_keras( test_data, test_labels )
            return test_acc, test_loss, test_prediction_scores
        elif self.backend == 'pymvpa':
            test_acc, test_prediction_scores = self.test_pymvpa( test_data, test_labels )
            return test_acc, -1, test_prediction_scores
        else:
            DTError.err("DTModel: Invalid backend specified. At the moment the only supported options are 'keras' and 'pymvpa'.")
    
    
    def test_keras( self, test_data, test_labels ):
        keras_opts = self.keras_options()
        
        test_loss, test_acc = self.model.evaluate(test_data, test_labels, verbose = keras_opts['verbose'])
        test_prediction_scores = self.model.predict(test_data) # n_samples list, each list item is n_categories long
        
        # MJ/TK: this chunk may be extraneous? Since we already remove during training
        #        should double-check before taking out, so won't do it now... have put a low-priority issue on Bitbucket though
        if self.remove_checkpoint_weights:
            try: 
                if os.path.isfile(keras_opts['checkpoint_filename']):
                    # in theory the file shouldn't go missing and we shouldn't have to check for it... but if somehow it DOES go missing, probably not worth 
                    #   throwing a warning/error over that
                    os.remove(keras_opts['checkpoint_filename'])
            except:
                DTError.warn("DTModel: Problem removing checkpoint weights file. You may have to clean it up by hand if its existence offends you.")
                
        return test_acc, test_loss, test_prediction_scores
    
    
    def test_pymvpa( self, test_data, test_labels ):
        binary_preds = self.model.predict( test_data )
        test_acc = (binary_preds==test_labels).astype(int)
        test_acc = sum(test_acc) / float(len(test_acc))
        temp_prediction_scores = self.model.ca['estimates']
        test_prediction_scores = []
        for x in temp_prediction_scores.value:
            if self.classifier['classifier_type'] == 'SMLR':
                test_prediction_scores.append( x )
            elif self.classifier['classifier_type'] == 'SVM':
                test_prediction_scores.append( [x] )
            else:
                DTError.err("DTModel: Unknown classifier type")

        return test_acc, test_prediction_scores
    

    def get_SVM_kernel(self, svm_params):
        kernel = svm_params.pop('kernel')

        if 'kernel_params' in svm_params:
            kernel_params = svm_params.pop('kernel_params')
        else:
            kernel_params = None

        if kernel == 'linear':
            from mvpa2.kernels.libsvm import LinearLSKernel
            returnkernel = LinearLSKernel()

        elif kernel == 'rbf':
            from mvpa2.kernels.libsvm import RbfLSKernel
            returnkernel = RbfLSKernel(**kernel_params)

        elif kernel == 'polynomial':
            from mvpa2.kernels.libsvm import PolyLSKernel
            returnkernel = PolyLSKernel(**kernel_params)

        elif kernel == 'sigmoid':
            from mvpa2.kernels.libsvm import SigmoidLSKernel
            returnkernel = SigmoidLSKernel(**kernel_params)
        else:
            DTError.err('Kernel type not found.')

        return(returnkernel)
    
    
    @classmethod
    def validate_arguments( cls, args ):
        # this is the laziest and dumbest possible version of a method like this, which really should inspect the arguments and give some intelligent
        # warning messages and so forth. But it'll do for now to get us off the ground
        try:
            val_args = copy.deepcopy( args )
            test_obj = cls( **val_args )
        except:
            global dt_debug_mode
            if 'dt_debug_mode' not in globals():
                dt_debug_mode = True # TK: change to false when we go into public production
            
            if dt_debug_mode:
                DTError.err( 'DTModel: validate_arguments() failed' )
            else:
                return False
        
        return True
