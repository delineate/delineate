# The DeLINEATE Toolbox -- basic info #

This file is just a quick overview -- for details on usage of various parts of the toolbox, see the Markdown files within the `documentation` folder of this repository or online at http://delineate.it

In theory these docs should be kept in sync with the documentation on the website, but you never know. If you discover discrepancies, please let the dev team know. Best way is probably to create an Issue using the Issues page of our Bitbucket repository -- this will require you to sign up for a (free) Bitbucket account.


<br><br>


## INSTALLATION / DEPENDENCIES ##

The installation of our toolbox is pretty straightforward, but fair warning: Installation of the toolboxes that we rely upon could be a minor-to-major pain. (We rely on PyMVPA for conventional MVPA, and Keras for deep learning -- and then Keras, in turn, depends on either a TensorFlow or Theano backend. And if you're doing GPU-accelerated learning, which hopefully you are if you'd like your analyses to complete while we're all still alive, you will also need Nvidia's CUDA and cuDNN tools.)

We hope to have a more comprehensive guide soon, but since we're working with open-source tools that are in active development, it's hard to provide a definitive set of steps that will work for everyone and won't go out of date nearly instantaneously. But basically, if you have a setup that is capable of running PyMVPA (http://www.pymvpa.org) and/or Keras (https://keras.io), then you should be able to run our stuff without any additional setup. Those toolboxes' documentation is not amazingly detailed but with a little tech-savviness, plus Google and perseverance, you should be able to get it all working. If you have real troubles, the dev team is happy to help people with their own setups -- all we ask in exchange is your help in fleshing out the documentation, so we can pay it forward to future users.

If it helps at all, we have some super-janky and probably somewhat out-of-date (possibly even straight-up incorrect) notes from our initial attempts to set up our own deep learning workstations. Feel free to crib from there as helpful, but take it all with a huge grain of salt. We hope to migrate the bits from that page that are actually useful and true to this documentation eventually, but for now... caveat emptor: http://matthewrjohnson.net/lab/rapwiki/DeepLearningSetup

**NEW** since the above paragraph was written -- we also now have a few YouTube video demos, including one of setting up a new system with all the bits and pieces, starting from booting up the hardware for the first time and installing the OS, all the way through running the toolbox (which takes about an hour once you get good at it). These videos are probably already out of date, but the basic steps should be the same regardless. Check them out on the Documentation page at http://delineate.it/documentation.html

See the next section for more specific deets on the kinds of hardware/software you're gonna need to get this stuff all working.


<br><br>


## COMPATIBILITY ##

### Hardware and OS ###

**Overview.** Minimally, just about any computer with a working Python installation will do if you want to try out this toolbox. PyMVPA does not do GPU acceleration at the moment, and Keras will run its analyses on the CPU(s) if it doesn't find a compatible GPU. However, most of the value of this toolbox probably comes from doing GPU-accelerated deep learning analyses. You'll probably want reasonable computing hardware to get decent speed out of whatever you're doing, but most of the other stuff doesn't matter nearly as much as the GPU. So if you're trying to allocate resources on a budget, sink more money into the GPU and less into maxing out the other stuff. A decent amount of system memory is a good idea (16 GB would be OK, 32 GB even better... basically you want RAM equivalent in size to the largest chunk of data you'd like to analyze at once, plus a few GB of headroom for the OS and applications). SSD vs conventional hard drive doesn't matter that much beyond the usual speedup to your system overall... most of the time you'll be loading the data once and then analyzing it for a long time thereafter, so disk reading/writing time will not be most people's speed bottleneck. CPU cores mainly only matter if you want to run Keras (deep learning) analyses without GPU acceleration (which even with many CPUs is going to be much slower than using a GPU, so not super-recommended), or if you want to run a bunch of PyMVPA processes simultaneously. So for most people, a run-of-the-mill CPU with 4 medium-fast cores is fine and will not be your main speed bottleneck either. Again, it all depends on your use case but as long as you have a medium-powerful computer in conventional terms, the main thing to think about will be the GPU.

**GPU.** At time of writing, the deep learning libraries that DeLINEATE depends upon only do GPU acceleration for Nvidia GPUs. So that's easy. For most people, you'll see a huge gain of using any semi-decent GPU versus relying on CPUs, and then relatively modest benefits with increasingly fancy GPUs. A pretty decent gaming GPU (e.g. a GeForce GTX 1060, currently retailing for ~300-400 USD) should be sufficient for many cases. You can go on up the line if you like (the GTX 1070 and 1080 line are also nice GPUs), but for the increase in price, you could be better off building more systems with cheaper GPUs if you want to get more analyses run faster, rather than maxing out each GPU. Again, your mileage may vary. If you're trying to decide, the dev team is happy to consult in exchange for your help fleshing out the documentation and benchmark data of this toolbox. **Note**: Since writing the first version of this paragraphs, Nvidia GPUs have had another couple of generations released... but the GPUs referenced above are still good for deep learning if you can get them. Or use the equivalents in whatever the current generation is!

One key note is that even if there are not huge speed bumps between GPU models, they are also differentiated by the amount of onboard GPU RAM. This makes a difference because some Keras models occupy a LOT of GPU RAM. So if you expect to run a complex neural network model and be able to push large chunks of data through it, you may need to shell out for a nicer GPU purely for RAM purposes, even if the speed boost is not very important to you.

### Code libraries and toolboxen we depend on ###

**Overview.** In general, we have tried to minimize dependencies as much as possible. As noted above, we currently have two back-end toolboxes that we support -- PyMVPA for conventional MVPA and Keras for deep learning (with Keras, in turn, relying on either a Theano or TensorFlow back-back-end). You do not need to have both of these installed, and we do not install either one of them for you -- but as long as the one you are intending to use is available on the Python path when you use our toolbox, you should be good to go.

**Architecture.** We use a pretty flexible and bare-bones wrapper architecture that we hope will allow us to maintain compatibility with a pretty wide range of Python/Keras/PyMVPA releases. At the moment, most pieces should work with both Python 2 and 3, and most recent-ish versions of Keras/PyMVPA (as far as we know, though of course we have not tested every possible permutation).

Our advice to you, however, is that you pick a Python version / environment / etc. that works for you, and stick to it as well as you can. Getting and running our toolbox itself is easy -- just download it (either a release version in .zip form or the ongoing development version via our Git repository), and run it from wherever you like (most typically via the delineate.py invocation script). However, our backends and their own dependencies may be a little more finicky.

You can install everything on top of your system default Python if you like, but one thing we have found helpful is to download the Anaconda environment (https://www.anaconda.com) and use that to set up a little sandboxed environment to keep your DeLINEATE stuff in. This is useful just in case things start to get all freaky-deaky and you don't want to disrupt the delicate balance of the rest of your Python ecosystem (assuming you use Python for other things, or intend to someday). Alternately or additionally, you could use Python's built-in virtual environment features. We'll leave the details of setting up Anaconda or other virtual environments to those projects' respective documentation for now, but if anyone is interested in seeing (or better yet, writing) a more complete walkthrough, feel free to get in touch.

**PyMVPA.** This project was not initially intended primarily as a means of providing an interface to PyMVPA or any other conventional MVPA package, but we found it useful to add PyMVPA support so that we could easily compare classifier performance between deep learning and conventional MVPA. (While ensuring that we keep every other aspect of the pre-processing, cross-validation, and so on the same, except for the actual classifier.) That said, you could totally use it just as a front-end way of running PyMVPA analyses if you find PyMVPA's design as baroque and confusing as we do. (No offense, PyMVPA devs, but your class hierarchy is so abstract we hear the ghost of Immanuel Kant is jealous.)

With that said, our PyMVPA support is *somewhat* limited at present. We currently support support vector machine (SVM) and sparse multinomial logistic regression (SMLR) classifiers within PyMVPA, as those have been the workhorses of our own conventional MVPA work. It is relatively easy to add support for more classifiers although not entirely non-trivial, as each classifier in PyMVPA is kind of unique in terms of how it is invoked and what parameters it takes. If you are interested in adding support for any other classifiers, get in touch with us -- it is on our to-do list but hasn't been a priority yet for the primary devs.

We should emphasize that mainly what we are using from PyMVPA is the classifiers themselves, and not any of PyMVPA's other functionality (e.g. cross-validation schemes and so forth). All of that stuff is done within DeLINEATE. This is a little redundant maybe, but we didn't want to have to rely on PyMVPA's architecture and its limitations because that architecture doesn't really work super-great for our deep learning analyses, and we wanted to keep all the infrastructure except for the actual classification identical between our backends.

**Keras.** This is where we started; Keras is our main backend for deep-learning analyses although it is always conceivable that we could add support for others in the future. We currently support most kinds of Sequential models in Keras -- which does not encompass every neural network architecture anyone might want to use, but it does encompass an awfully large number. Similarly, we don't necessarily support every possible parameter to each kind of layer/function/etc. within Keras because some of them are hard to describe with config files alone (without making the end user write any of their own Python code). But you can still do an awful lot, and if there is something you desperately want to accomplish in Keras but can't, get in touch with us -- it might not be too hard to add.

We should note that when we say something "isn't supported" for Keras, that usually means, "assuming you just want to make configuration files and don't want to write any of your own Python code." If, instead, you want to use DeLINEATE as a library of helper functions and are OK writing some of your own code to go along with it, you can do a lot more. (A hybrid approach would also be possible -- e.g. using a configuration file to create a basic neural network architecture and then tweaking it with your own custom code before actually running it.)

One complication with Keras: At the time of writing, there is currently some reconfiguration going on as Keras is now primarily being developed/packaged as part of TensorFlow. It looks like this has caused some issues with how we traditionally imported Keras. In the future, we'll try to make our toolbox work with both standalone Keras and Keras-within-TensorFlow, but for now, you may either want to (1) use an older version of Keras from when it was distributed separately, or (2) first install TensorFlow (including Keras), and then on top of that install the standalone Keras package (either from the Keras GitHub page, or via a package manager like PyPI/`pip`).


### Notes on Python/miscellaneous version compatibilities ###

**General caveat.** As anyone who has worked with Python libraries is probably aware, keeping track of all of your dependencies and their respective compatibilities can be a full-time job in itself. So take the points below with a grain of salt, but at the time of writing, we believe the following to still be true:

* Our own toolbox code is intended to be fully compatible with all versions of Python from 2.7 on up, including all versions of Python 3. (And, as far as we know, it actually *is* compatible with all those.)

* Current versions of Keras are only distributed/tested with Python 3 compatibility in mind. Older versions Keras should in theory work with both Python 2 and 3. We generally use Python 3 when working with Keras, but as far as we are aware, if you have a version of Keras that is playing nicely with your version of Python, our toolbox should play nicely with all of them.

* Officially, it seems like PyMVPA is Python 2.7 only, but so far we have had good luck with Python 3 in most use cases and have seemingly managed to avoid most of the stuff that would break under Python 3.

* We need to do more extensive testing to determine exactly which versions of everything we *officially* maintain compatibility with. Right now we are generally using whatever versions of Nvidia drivers / CUDA / cuDNN / Keras / PyMVPA are current, although we started out writing for Keras 1.x (it is at the time of writing up to 2.x) and if you, for some reason, want to keep running in older Keras 1.x, that will probably still work. Overall, we try to both keep current with compatibility and to not break backward compatibility when doing so... but if you come upon any incompatibilities, please let us know.

* In our Keras work, we previously mostly used the Theano backend but TensorFlow should work equally well. More recently, Theano (and multi-backend support in general) has been dropped from Keras, but we intend to continue supporting Theano (and the versions of Keras that supported Theano) for the foreseeable future. There are some issues with package paths that come along with the most recent versions of Keras actually being included as PART of TensorFlow (see some other notes on that above in the "Keras" subsection of the "Code libraries" section) but those versions should still work... you just might have to install a standalone Keras on top of the copy that comes with TensorFlow for now. (In the future, we will keep working on making this stuff all play together more transparently, but as noted above, keeping up with versions of everything is an uphill climb.)



### A personal note from the devs ###

Like many software toolboxen developed for/by scientists, this project will probably never be "complete" and documentation will probably always be in a continual state of needing additions/updating. But we're trying to keep it as complete and up-to-date as possible!

As of the time of this writing, we are still actively developing the code and docs, and plan to continue doing so for the foreseeable future, but adoption and feedback from the community will play a large part in where we go from here and how much time we devote to maintenance and adding new features.

This is currently an unfunded project that requires a fair bit of our "spare" time to maintain, so if you find some feature or documentation lacking, please bear with us! You are welcome to contact the dev team with questions and/or suggestions, and that will help us decide where to direct our energies. Contributions and pull requests are also welcomed. See our website for contact details.

