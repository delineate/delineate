***PREFACE TO ALL DOCUMENTATION***: We have tried to be as comprehensive, helpful, and accurate as we can in all of these documents, but providing good documentation is always an uphill climb. Our code, and the underlying backend code we rely on, is always changing, which means things can easily go out of date; and doing these kinds of analyses is intrinsically a complicated process, which makes it hard to write documentation that works well for people at all different levels of technical proficiency and familiarity with the underlying concepts and technologies. We really don't want the learning curve to be a barrier to people using this toolbox, so we *highly* recommend -- especially while the number of users is relatively small and manageable -- getting in touch with the developers if you're confused, don't know where to start, etc., etc. And, of course, if you find any errors, omissions, or inconsistencies! Seriously, don't be a stranger... we are happy to add features, flesh out documentation, walk through setup, and so on, to help this project serve as many users as possible, but that requires hearing from you to help let us know what areas need the most attention. Please see our website (`http://delineate.it/`) and click on the contact page to get a link to our Bitbucket repo and an email address for the project devs.

---

# DTGui (class) #

In this version of the toolbox, DTGui is still in a pretty early form, so it may not get comprehensive documentation until it is a little more mature. Also, since it is a GUI, no one should *really* be using it programmatically. So, we will eschew the usual documentation about methods and such, and focus on the primary elements of the interface. Overall, a video tutorial may be a more helpful way of learning `DTGui` than reading about it, anyway, so please check the website... if one hasn't been posted there yet by the time you read this, it should be soon.

The rest of this documentation pre-supposes that you have some general idea of the overall toolbox architecture, so if concepts like jobs and JSON job files, or the main object types that comprise an analysis (`DTAnalysis`,`DTData`,`DTModel`,`DTOutput`) are unfamiliar to you, you will probably want to check out the other docs/tutorials first.

Note that `DTGui` only provides some of the most common configurations/options that we think users will want. However, due to the flexible architecture of the toolbox, using a JSON job file (or writing your own code) gives much broader access to the many additional options that are theoretically possible to select in the Keras or PyMVPA backends, which are simply too numerous to squeeze into any reasonable GUI. So, if `DTGui` does not provide some advanced/obscure option that you want to use, you may still need to edit JSON job files (or write your own code).




<br><br>




## Main interface elements ##


**"Select backend" button**: This should be the first thing that your eye falls on, at the top center of the main `DTGui` window. This choice will affect the other interface elements, so probably best to make this one first. Current options are `PyMVPA` or `Keras`.


**"Data" section**: This section is centered near the top of the window, directly under the "Select backend" button described above. This is where you can select all the options that make up a `DTData` object or, equivalently, all the options that go in the `data` section of a JSON job file. The main things to provide here are a loader function name, and the parameters to pass into the loader function (assumed to be strings; one parameter per line). Typically the first parameter would be a filename, although that's really up to your loader function. See the `DTData` documentation for more explanation of these concepts.


**"Analysis" section**: This section is on the left side of the window, under the "Data" section. This is where you can select all the options that make up a `DTAnalysis` object or, equivalently, all the options that go in the `analysis` section of a JSON job file. Options provided here include things like how many iterations to run, what kind of cross-validation to use, how to scale the data, and more. See the `DTAnalysis` documentation for more explanation of these concepts.


**"Model" section**: This section is in the center of the window, under the "Data" section. This is where you can select all the options that make up a `DTModel` object or, equivalently, all the options that go in the `model` section of a JSON job file. Options provided here will determine what kind of classifier is used and the various hyperparameters of that classifier. Note that the interface options available in this section are *heavily* dependent on whether you're using the Keras or PyMVPA backend, so again, make sure you've selected your backend first at the top of the window before messing with anything in this section. See the `DTModel` documentation for more explanation of these concepts.


**"Output" section**: This section is on the right side of the window, under the "Data" section. This is where you can select all the options that make up a `DTOutput` object or, equivalently, all the options that go in the `output` section of a JSON job file. Options provided here include things where to save output files, what to call them, and what kinds of output to generate. See the `DTOutput` documentation for more explanation of these concepts.


**"Load settings from JSON file" button**: This button allows you to load in a previously configured job from a JSON file, so that you can modify it or just inspect its contents. Clicking this will open a standard "Open file" dialog box.


**"Save settings to JSON file" button**: This button lets you save the current configuration to a JSON job file that can then be run from the command line via the `delineate.py` command (currently, `DTGui` does not support running a job directly from the GUI). Clicking this will open a standard "Save file" dialog box.


**"Close GUI" button**: Those among you who did well in school might have already figured this one out, but this button closes the GUI window. But if you want your configuration saved, make sure you click the "Save settings" button first!

