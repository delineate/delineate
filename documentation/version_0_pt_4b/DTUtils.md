***PREFACE TO ALL DOCUMENTATION***: We have tried to be as comprehensive, helpful, and accurate as we can in all of these documents, but providing good documentation is always an uphill climb. Our code, and the underlying backend code we rely on, is always changing, which means things can easily go out of date; and doing these kinds of analyses is intrinsically a complicated process, which makes it hard to write documentation that works well for people at all different levels of technical proficiency and familiarity with the underlying concepts and technologies. We really don't want the learning curve to be a barrier to people using this toolbox, so we *highly* recommend -- especially while the number of users is relatively small and manageable -- getting in touch with the developers if you're confused, don't know where to start, etc., etc. And, of course, if you find any errors, omissions, or inconsistencies! Seriously, don't be a stranger... we are happy to add features, flesh out documentation, walk through setup, and so on, to help this project serve as many users as possible, but that requires hearing from you to help let us know what areas need the most attention. Please see our website (`http://delineate.it/`) and click on the contact page to get a link to our Bitbucket repo and an email address for the project devs.

---

# DTUtils (module) #

---

This is a very simple utility module that currently just contains a couple of general-purpose helper functions. End users should not really have to interact with it, but we will document those functions here anyway just in case anyone wants to use them in their own code. This module may be expanded with additional small helper functions in the future.

<br><br>


## Functions ##


**listify**( `item_or_list` )

*(returns a list or NumPy array)* Used as a convenience for use cases in which either a single object or a list can be supplied, but it's easier for the code in question just to be written to handle lists. If the argument passed into this function is already a Python list or NumPy array, it just returns that same argument without any changes. If the argument passed into this function is any other kind of singleton object, it returns a one-item Python list with that object in it.


<br>

**dt_input**( `msg` )

*(returns a string)* This is just providing a Python-version-independent way to request text input from the user on the command line, since Python3 uses `input()` and  Python2 uses `raw_input()`, and as far as we know there is no easy way to achieve this task in a single line without writing version-specific code. The `msg` argument should be a string prompt to be displayed when input is requested.

